<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sormat' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'w:GvJ<F+X ;tN>j_=@kqyU%0^6: dr5||}.]j9BIj-AS[kl{?S<;VERp=.UOWh6:' );
define( 'SECURE_AUTH_KEY',  'K4BvFW37j0u8WnBm~J2z%447V|VGh:zdo=iR)7!J#qfK+kU?RLDEZW3Vm-Bwd(we' );
define( 'LOGGED_IN_KEY',    'N$EU[{wW42Kz(Q<}4;*SL%=($]>Ei$A%Y.J,,++|n!J!x26f_uI>K3b{07xxLx57' );
define( 'NONCE_KEY',        'czp}T6F146HoE7um&EHHjZ$fNHM]ggRrVIk&hC3!zo;b{BR}:B>kB{(nV-Ll(Jue' );
define( 'AUTH_SALT',        'k<-2s!IEoo^s:dV(mT7>tHHs} 4a#dU[i|C~O|*!v2@RUd$|QLzkq[G==F>.v0@!' );
define( 'SECURE_AUTH_SALT', 'd]D~*_T_wot)uc{4@m?cQB*2frM:}p}_^;Rr7.w0{ly@w<dg4NVL-Y9<P~Q_ _+a' );
define( 'LOGGED_IN_SALT',   'UnMD;I.w(!;2HJ,*9a e~}t>5~cV39%4]4Bh<H,]dSjdWdNoix/|+7YY#%*` ;eH' );
define( 'NONCE_SALT',       '=mugjop}.SX5.KzgWbO%Vzq&1E7b5-<&q3YUX@EVL,xeQI/jq:1!2?>&b$yDkT,`' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

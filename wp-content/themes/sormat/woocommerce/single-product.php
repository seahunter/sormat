<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop'); ?>

    <section id="product" class="main-content-holder">

        <?php get_sidebar('breadcrumbs'); ?>
        <div class="woocommerce-notices-wrapper"><?php wc_print_notices(); ?></div>
        <div class="three-columns left first">
            <h2><?php echo get_field('public_product_name_ru') ?></h2>
        </div>
        <div class="clear"></div>
        <div class="sormat-code">
            <p>
                Артикул Sormat <span><?php the_title() ?></span>
            </p>
        </div>
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php
                $product_group = (int)get_field('product_group');
                $product_type = get_field('product_type', $product_group);
                $relative = new WP_Query([
                    'p' => $product_group,
                    'post_type' => 'productgroup',
                ]);
                $product_id = get_the_id();

                if ($relative->have_posts()) {
                    while ($relative->have_posts()) : $relative->the_post();
                        set_query_var('snro', get_field('snro', $product_id));
                        set_query_var('lvi', get_field('lvi', $product_id));
                        set_query_var('weight', get_field('weight', $product_id));
                        set_query_var('product_id', $product_id);
                        set_query_var('product_type', $product_type);
                        get_template_part('template-parts/product/template', 'main');
                    endwhile;
                    wp_reset_postdata();
                }

                get_template_part('template-parts/product/template', 'child');

                ?>
            <?php endwhile;
            wp_reset_postdata(); ?>
        <?php endif; ?>

    </section>
<?php
get_footer('shop');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

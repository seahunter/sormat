<div class="inside">
    <div>
        <?php $statuses = sormat_order_statuses(); ?>
        <?php if ('processing' === $order->get_status()) : ?>
            <a class="button button-success" href="?order-status=approve&order_id=<?php echo $order->get_id() ?>">Заказать</a>
            <a class="button button-cancel"
               href="?order-status=cancel&order_id=<?php echo $order->get_id() ?>">Отменить</a>
        <?php endif ?>
    </div>
    <div class="woocommerce_order_items_wrapper wc-order-items-editable">
        <table cellpadding="0" cellspacing="0" class="woocommerce_order_items">
            <thead>
            <tr>
                <th class="item sortable" colspan="1" data-sort="string-ins">Артикул</th>
                <th>Наименование</th>
                <th>Комментарии</th>
                <th class="item_cost custom-cost">Цена Sormat</th>
                <th class="quantity sortable" data-sort="int">Кол-во</th>
                <th class="line_cost sortable" data-sort="float">Итого</th>
            </tr>
            </thead>
            <?php foreach ($order_items as $item_id => $item) : ?>
                <?php
                $product = $item->get_product();
                $line_items_shipping = $order->get_items('shipping'); ?>
                <tbody id="order_line_items">
                <tr class="item " data-order_item_id="6">
                    <td class="name" data-sort-value="<?php echo $item->get_name(); ?>">
                        <a href="<?php echo get_permalink($product->get_id()) ?>"
                           target="_blank"><?php echo $item->get_name() ?></a>
                    </td>
                    <td>
                        <div class="view">
                            <?php echo get_field('public_product_name_ru', $product->get_id()) ?>
                        </div>
                    </td>
                    <td>
                        <div class="view">
                            <?php echo wc_get_order_item_meta($item_id, 'sormat_item_comment', true) ?>
                        </div>
                    </td>
                    <td class="quantity item_cost custom-cost">
                        <div class="view"><?php echo wc_price($order->get_item_total($item)) ?></div>
                    </td>
                    <td class="quantity" width="1%">
                        <div class="view">
                            <?php
                            $qty = $item->get_quantity();
                            $refunded_qty = $order->get_qty_refunded_for_item($item_id);
                            if ($refunded_qty) {
                                $qty_display = '<del>' . esc_html($qty) . '</del> <ins>' . esc_html($qty - ($refunded_qty * -1)) . '</ins>';
                            } else {
                                $qty_display = esc_html($qty);
                            }

                            echo $qty_display;
                            ?>
                        </div>
                    </td>
                    <td class="line_cost" width="1%" data-sort-value="108">
                        <div class="view"><?php echo wc_price($order->get_line_subtotal($item)) ?></div>
                    </td>
                </tr>
                </tbody>
            <?php endforeach; ?>
            <?php foreach ($line_items_shipping as $item_id => $item) : ?>
                <tbody id="order_shipping_line_items">
                <tr class="shipping " data-order_item_id="3">
                    <td class="name">
                        <div class="view">
                            Доставка
                        </div>
                    </td>
                    <td></td>
                    <td>
                        <div class="view">
                            <?php echo wc_get_order_item_meta($item_id, 'sormat_item_comment', true) ?>
                        </div>
                    </td>
                    <td class="item_cost" width="1%">&nbsp;</td>
                    <td class="quantity" width="1%">&nbsp;</td>

                    <td class="line_cost" width="1%">
                        <div class="view">
                            <?php echo wc_price($order->get_shipping_total()) ?>
                        </div>
                    </td>
                </tr>
                </tbody>
            <?php endforeach; ?>
        </table>
    </div>
    <div class="wc-order-data-row wc-order-totals-items wc-order-items-editable">
        <table class="wc-order-totals">
            <tbody>
            <tr>
                <td class="label">Итого по позициям:</td>

                <td class="total">
                    <?php echo wc_price($order->get_subtotal()) ?>
                </td>
            </tr>


            <tr>
                <td class="label">Доставка:</td>

                <td class="total">
                    <?php echo wc_price($order->get_shipping_total()) ?></td>
            </tr>


            <tr>
                <td class="label">Итог заказа:</td>

                <td class="total">
                    <?php echo wc_price($order->get_total()) ?></td>
            </tr>

            </tbody>
        </table>

        <div class="clear"></div>
    </div>

</div>
<?php get_header() ?>

<?php while (have_posts()) : the_post() ?>
    <section id="carousel-wrapper" style="padding-top: 160px;">
        <?php echo do_shortcode('[owl-carousel category="Main carousel" singleItem="true" autoPlay="true"]') ?>
    </section>
<?php endwhile ?>

    <section id="main-search-wrapper">
        <?php get_search_form() ?>
    </section>

    <section id="front-page" class="main-content-holder">
        <?php
        $articles_args = [
            'post_type' => 'article',
            'post_status' => 'status',
            'posts_per_page' => 3,
            'no_found_rows' => true,
            'tax_query' => [
                [
                    'taxonomy' => 'contentclass',
                    'field' => 'slug',
                    'terms' => 'training-article'
                ]
            ]
        ];

        $articles = new WP_Query($articles_args);
        ?>
        <?php if ($articles->have_posts()) : ?>
            <article id="articles-wrapper">
                <?php while ($articles->have_posts()) : $articles->the_post() ?>
                    <?php get_template_part('template-parts/article', 'preview') ?>
                <?php endwhile;
                wp_reset_postdata() ?>

                <div class="clear"></div>
            </article>
        <?php endif ?>

        <?php get_template_part('template-parts/product/filter'); ?>

        <form id="productgroup-search-form" action="<?php echo get_home_url() ?>"></form>
        <?php $last_news = new WP_Query(array(
            'post_type' => 'article',
            'post_status' => 'status',
            'posts_per_page' => 3,
            'no_found_rows' => true,
            'tax_query' => [
                [
                    'taxonomy' => 'contentclass',
                    'field' => 'slug',
                    'terms' => 'news'
                ]
            ]
        )) ?>
        <?php if ($last_news->have_posts()) : ?>
            <article id="recent-news-wrapper">
                <header id="recent-news-header">
                    <h3 class="left">Последние новости</h3>
                    <a href="<?php echo get_term_link( 'news', 'contentclass' ); ?>" class="right see-all"
                       title="All news">Все новости &gt; </a>
                </header>

                <?php while ($last_news->have_posts()) : $last_news->the_post() ?>
                    <?php get_template_part('template-parts/article', 'preview-date') ?>
                <?php endwhile ?>

                <div class="clear"></div>
            </article>
        <?php endif; wp_reset_postdata(); ?>

    </section>

<?php get_footer() ?>
<?php
get_header(); ?>

<section id="training-article" class="main-content-holder">

  <?php get_sidebar('breadcrumbs'); ?>
  
  <div class="row">
    <div class="one-column news-text left first">
    </div>

    <div class="two-columns news-texts right">
      <h1>Обучающих</h1>
          </div>
    <div class="clear"></div>
  </div>
  <?php if( have_posts() ) : ?>
    <?php while( have_posts() ) : the_post() ?>
      <?php get_template_part( 'template-parts/training', 'preview' ) ?>
    <?php endwhile ?>
    <div class="pagination">
      <?php the_posts_pagination() ?>
    </div>
    <div class="clear"></div>
  <?php endif ?>

</section>

<?php get_footer();
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sormat
 */

?>
<?php $footer = get_field('footer', 'option') ?>
<div class="slide-footer">
	<footer class="no-print open" style="position: relative; left: 0px; bottom: 0px;">
		<div id="upper-part">
			<!-- <span class="arrow-up-icon-left"></span>
			<p>
				Для связи с отделом обслуживания нажмите        </p>
				-->
			<p id="super-service-text">Обслуживание клиентов</p>
			<!-- <span class="arrow-up-icon-right"></span> -->
		</div>

		<div id="lower-part"></div><!--Orange line -->

		<div id="footer-content-holder">
			<?php if( $footer ) : ?>
			<div id="footer-content"><!--Actual Super service stuff -->
				<div class="footer-body"></div>

				
				<div class="footer-contact-sormat column first column-odd">
					<h3><span><?php echo $footer['left_column']['title'] ?></span><div class="open-close-accordion"></div></h3>

					<div class="accordion-holder">

						
						<div class="address">
							<?php $contact_page = $footer['contact_page']; ?>
							<?php if( $contact_page ) : ?>
								<p><a title="Связаться с “Sormat”" href="<?php echo get_permalink($contact_page->ID) ?>">Задайте любой вопрос</a></p>
							<?php endif ?>
							<p><?php echo $footer['left_column']['description'] ?></p>
						</div>

						<a class="button" href="<?php echo esc_url( $footer['left_column']['map_link'] ) ?>" target="_blank"><span>Посмотрите карту</span></a>
						<!-- <button><a style="display:inline;line-height:1;margin:0;padding:0;background:none;color:#fff;" href="https://www.google.com/maps/place/Sormat+Holding+Oy/@60.51942,22.22715,17z/data=!3m1!4b1!4m2!3m1!1s0x468b8a0a6ed5e4df:0xde9d44d8af601d6e" target="_blank">Посмотрите карту</a></button> -->

					</div>
				</div>

				
				<div class="footer-locate-reseller column">
					<h3><span><?php echo $footer['right_column']['title'] ?></span><div class="open-close-accordion"></div></h3>

					<div class="accordion-holder">

						
						<p><!--Продукция "Sormat" продается более чем в 40 странах по всему миру. Используйте поисковик, чтобы найти наших национальных или региональных партнеров по дистрибуции или местных дилеров.--><br>
						<script>// <![CDATA[
						// This is a temporary workaround script put into place when we needed to hide reseller search for a while. 
						jQuery(document).ready(function($){
								$('.footer-locate-reseller form:not(.keepthis)').hide();
						});
						// ]]&gt;</script><br>
						<?php echo $footer['right_column']['description'] ?></p>
						<form class="keepthis" action="<?php echo $footer['right_column']['link']['url'] ?>"><button type="submit"><?php echo $footer['right_column']['link']['title'] ?></button></form>

						

						
						<form method="get" action="https://ru.sormat.com/locate-reseller/" style="display: none;">
							<input type="text" name="search" id="country-zip-city" placeholder="Страна, город или почтовый индекс">
							<br>
							<button type="submit">Найти</button>
						</form>

						
					</div>
				</div>

				
				<div class="clear"></div>

			</div>
			<?php endif ?>

			<div class="full-column">
				<div class="footer-nav-container">
					<div class="menu-super-service-container"><ul id="menu-super-service" class="menu"><li id="menu-item-51" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-51"><a href="#">Left</a></li>
						<li id="menu-item-52" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-52"><a href="#">Right</a>
						<ul class="sub-menu">
							<li id="menu-item-281" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281"><a href="https://ru.sormat.com/description-of-file/">Description of file</a></li>
							<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53"><a href="https://ru.sormat.com/terms-conditions/">Terms &amp; Conditions</a></li>
						</ul>
						</li>
						</ul></div>            <div class="clear"></div>
				</div>
			</div>

		</div>

	</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>

��          �            x     y      �  !   �     �     �            *   /  (   Z  &   �  #   �     �  '   �  "        8  �  O     �          ,  	   D     N     [     b     h  	   t     ~     �  
   �     �     �     �               
                                          	                      Colleague&#39;s email name Share a link with your colleague You can personalise this message. package-typebox (bag) package-typeouter carton package-typepallet package-typepiece print-automation-productAlso suitable for print-automation-productFlange diameter print-automation-productHead diameter print-automation-productInner hole print-automation-productOther print-automation-productSuitable cable productAnchor and fixture details productScrew diameter Project-Id-Version: Sormat main theme
POT-Creation-Date: 2017-12-15 14:27+0200
PO-Revision-Date: 2017-12-15 14:27+0200
Last-Translator: Marco Martins <marco@h1.fi>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.5
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
X-Poedit-SearchPathExcluded-1: auto
 Recipient email Share this page with someone Type here your message. box (bag) outer carton pallet piece Suitability Flange ø Head ø Inner hole / eye ø Other info Suitable cable ø Installation details Screw ø 
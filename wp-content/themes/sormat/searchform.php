<form id="main-search-form" action="<?php echo get_home_url() ?>">
  <div id="main-search-holder">
    <div class="center-search-elements">
      <div class="icon-holder left"><span class="search-icon left"></span></div>
      <input type="text" id="main-search" class="text-input left" name="s" placeholder="Что вы ищете? Поиск по названию, артикулу, области применения…">
      <button type="submit" class="main search-submit right" name="search-submit">Поиск</button>
      <div class="clear"></div>
      <div id="search-result-container"></div>
    </div>
  </div>
</form>
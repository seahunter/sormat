<?php
/**
 * Registers the `feedback` post type.
 */
function geolocation_init() {
	register_post_type( 'geolocation', array(
		'labels'                => array(
			'name'                  => __( 'Геолокация', 'sormat' ),
			'singular_name'         => __( 'Города', 'sormat' ),
			'all_items'             => __( 'All Геолокация', 'sormat' ),
			'archives'              => __( 'Города Архивы', 'sormat' ),
			'attributes'            => __( 'Города Атрибуты', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Города', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Города', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'feedback', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'feedback', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'feedback', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'feedback', 'sormat' ),
			'filter_items_list'     => __( 'Filter Геолокация list', 'sormat' ),
			'items_list_navigation' => __( 'Геолокация list navigation', 'sormat' ),
			'items_list'            => __( 'Геолокация list', 'sormat' ),
			'new_item'              => __( 'New Города', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Города', 'sormat' ),
			'edit_item'             => __( 'Edit Города', 'sormat' ),
			'view_item'             => __( 'View Города', 'sormat' ),
			'view_items'            => __( 'View Геолокация', 'sormat' ),
			'search_items'          => __( 'Search Геолокация', 'sormat' ),
			'not_found'             => __( 'No Геолокация found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Геолокация found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Города:', 'sormat' ),
			'menu_name'             => __( 'Геолокация', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'feedback',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'geolocation_init' );

/**
 * Sets the post updated messages for the `feedback` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `feedback` post type.
 */
function geolocation_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['feedback'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Города updated. <a target="_blank" href="%s">View Города</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Города updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Города restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Города published. <a href="%s">View Города</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Города saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Города submitted. <a target="_blank" href="%s">Preview Города</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Города scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Города</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Города draft updated. <a target="_blank" href="%s">Preview Города</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'geolocation_updated_messages' );

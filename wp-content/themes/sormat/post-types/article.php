<?php

add_action('init', 'sormat_post_type_articles');
function sormat_post_type_articles()
{
    register_post_type('article', array(
        'labels' => array(
            'name' => 'Статьи', // Основное название типа записи
            'singular_name' => 'Статья', // отдельное название записи типа Book
            'add_new' => 'Добавить новую',
            'add_new_item' => 'Добавить новую статью',
            'edit_item' => 'Редактировать статью',
            'new_item' => 'Новая статья',
            'view_item' => 'Посмотреть статью',
            'search_items' => 'Найти статью',
            'not_found' => 'Статей не найдено',
            'not_found_in_trash' => 'В корзине статей не найдено',
            'parent_item_colon' => '',
            'menu_name' => 'Статьи'

        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail')
    ));
}
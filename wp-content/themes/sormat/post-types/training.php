<?php

/**
 * Registers the `training` post type.
 */
function training_init() {
	register_post_type( 'training', array(
		'labels'                => array(
			'name'                  => __( 'Trainings', 'sormat' ),
			'singular_name'         => __( 'Training', 'sormat' ),
			'all_items'             => __( 'All Trainings', 'sormat' ),
			'archives'              => __( 'Training Archives', 'sormat' ),
			'attributes'            => __( 'Training Attributes', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Training', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Training', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'training', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'training', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'training', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'training', 'sormat' ),
			'filter_items_list'     => __( 'Filter Trainings list', 'sormat' ),
			'items_list_navigation' => __( 'Trainings list navigation', 'sormat' ),
			'items_list'            => __( 'Trainings list', 'sormat' ),
			'new_item'              => __( 'New Training', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Training', 'sormat' ),
			'edit_item'             => __( 'Edit Training', 'sormat' ),
			'view_item'             => __( 'View Training', 'sormat' ),
			'view_items'            => __( 'View Trainings', 'sormat' ),
			'search_items'          => __( 'Search Trainings', 'sormat' ),
			'not_found'             => __( 'No Trainings found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Trainings found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Training:', 'sormat' ),
			'menu_name'             => __( 'Trainings', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'training',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'training_init' );

/**
 * Sets the post updated messages for the `training` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `training` post type.
 */
function training_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['training'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Training updated. <a target="_blank" href="%s">View Training</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Training updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Training restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Training published. <a href="%s">View Training</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Training saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Training submitted. <a target="_blank" href="%s">Preview Training</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Training scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Training</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Training draft updated. <a target="_blank" href="%s">Preview Training</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'training_updated_messages' );

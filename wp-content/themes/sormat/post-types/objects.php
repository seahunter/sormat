<?php

add_action('init', 'sormat_post_type_object');
function sormat_post_type_object()
{
    register_post_type('objects', array(
        'labels' => array(
            'name' => 'Наши объекты', // Основное название типа записи
            'singular_name' => 'Объекты', // отдельное название записи типа Book
            'add_new' => 'Добавить новый',
            'add_new_item' => 'Добавить новый объект',
            'edit_item' => 'Редактировать объект',
            'new_item' => 'Новая статья',
            'view_item' => 'Посмотреть объект',
            'search_items' => 'Найти объект',
            'not_found' => 'Объект не найдено',
            'not_found_in_trash' => 'В корзине статей не найдено',
            'parent_item_colon' => '',
            'menu_name' => 'Наши объекты'

        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail')
    ));
}
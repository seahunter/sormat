<?php
/**
 * Registers the `feedback` post type.
 */
function feedback_init() {
	register_post_type( 'feedback', array(
		'labels'                => array(
			'name'                  => __( 'Feedbacks', 'sormat' ),
			'singular_name'         => __( 'Feedback', 'sormat' ),
			'all_items'             => __( 'All Feedbacks', 'sormat' ),
			'archives'              => __( 'Feedback Archives', 'sormat' ),
			'attributes'            => __( 'Feedback Attributes', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Feedback', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Feedback', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'feedback', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'feedback', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'feedback', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'feedback', 'sormat' ),
			'filter_items_list'     => __( 'Filter Feedbacks list', 'sormat' ),
			'items_list_navigation' => __( 'Feedbacks list navigation', 'sormat' ),
			'items_list'            => __( 'Feedbacks list', 'sormat' ),
			'new_item'              => __( 'New Feedback', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Feedback', 'sormat' ),
			'edit_item'             => __( 'Edit Feedback', 'sormat' ),
			'view_item'             => __( 'View Feedback', 'sormat' ),
			'view_items'            => __( 'View Feedbacks', 'sormat' ),
			'search_items'          => __( 'Search Feedbacks', 'sormat' ),
			'not_found'             => __( 'No Feedbacks found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Feedbacks found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Feedback:', 'sormat' ),
			'menu_name'             => __( 'Feedbacks', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'feedback',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'feedback_init' );

/**
 * Sets the post updated messages for the `feedback` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `feedback` post type.
 */
function feedback_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['feedback'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Feedback updated. <a target="_blank" href="%s">View Feedback</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Feedback updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Feedback restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Feedback published. <a href="%s">View Feedback</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Feedback saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Feedback submitted. <a target="_blank" href="%s">Preview Feedback</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Feedback scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Feedback</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Feedback draft updated. <a target="_blank" href="%s">Preview Feedback</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'feedback_updated_messages' );

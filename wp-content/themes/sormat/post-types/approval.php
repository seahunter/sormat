<?php
/**
 * Registers the `feedback` post type.
 */
function approval_init() {
	register_post_type( 'approval', array(
		'labels'                => array(
			'name'                  => __( 'Одобрения', 'sormat' ),
			'singular_name'         => __( 'Одобрения', 'sormat' ),
			'all_items'             => __( 'All Одобрения', 'sormat' ),
			'archives'              => __( 'Одобрения Archives', 'sormat' ),
			'attributes'            => __( 'Одобрения Attributes', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Одобрения', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Одобрения', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'feedback', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'feedback', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'feedback', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'feedback', 'sormat' ),
			'filter_items_list'     => __( 'Filter Одобрения list', 'sormat' ),
			'items_list_navigation' => __( 'Одобрения list navigation', 'sormat' ),
			'items_list'            => __( 'Одобрения list', 'sormat' ),
			'new_item'              => __( 'New Одобрения', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Одобрения', 'sormat' ),
			'edit_item'             => __( 'Edit Одобрения', 'sormat' ),
			'view_item'             => __( 'View Одобрения', 'sormat' ),
			'view_items'            => __( 'View Одобрения', 'sormat' ),
			'search_items'          => __( 'Search Одобрения', 'sormat' ),
			'not_found'             => __( 'No Одобрения found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Одобрения found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Одобрения:', 'sormat' ),
			'menu_name'             => __( 'Одобрения', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'page-attributes' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'feedback',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'approval_init' );

/**
 * Sets the post updated messages for the `feedback` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `feedback` post type.
 */
function certificate_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['approval'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Одобрения updated. <a target="_blank" href="%s">View Одобрения</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Одобрения updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Одобрения restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Одобрения published. <a href="%s">View Одобрения</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Одобрения saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Одобрения submitted. <a target="_blank" href="%s">Preview Одобрения</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Одобрения scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Одобрения</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Одобрения draft updated. <a target="_blank" href="%s">Preview Одобрения</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'certificate_updated_messages' );

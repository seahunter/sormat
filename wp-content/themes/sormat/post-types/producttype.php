<?php
/**
 * Registers the `Product type` post type.
 */
function producttype_init() {
	register_post_type( 'producttype', array(
		'labels'                => array(
			'name'                  => __( 'Product type', 'sormat' ),
			'singular_name'         => __( 'Product type', 'sormat' ),
			'all_items'             => __( 'All Product type', 'sormat' ),
			'archives'              => __( 'Product type Archives', 'sormat' ),
			'attributes'            => __( 'Product type Attributes', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Product type', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Product type', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'Product type', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'Product type', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'Product type', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'Product type', 'sormat' ),
			'filter_items_list'     => __( 'Filter Product type list', 'sormat' ),
			'items_list_navigation' => __( 'Product type list navigation', 'sormat' ),
			'items_list'            => __( 'Product type list', 'sormat' ),
			'new_item'              => __( 'New Product type', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Product type', 'sormat' ),
			'edit_item'             => __( 'Edit Product type', 'sormat' ),
			'view_item'             => __( 'View Product type', 'sormat' ),
			'view_items'            => __( 'View Product type', 'sormat' ),
			'search_items'          => __( 'Search Product type', 'sormat' ),
			'not_found'             => __( 'No Product type found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Product type found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Product type:', 'sormat' ),
			'menu_name'             => __( 'Product type', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'Product type',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'producttype_init' );

/**
 * Sets the post updated messages for the `Product type` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `Product type` post type.
 */
function producttype_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['Product type'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Product type updated. <a target="_blank" href="%s">View Product type</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Product type updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Product type restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Product type published. <a href="%s">View Product type</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Product type saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Product type submitted. <a target="_blank" href="%s">Preview Product type</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Product type scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Product type</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Product type draft updated. <a target="_blank" href="%s">Preview Product type</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'producttype_updated_messages' );

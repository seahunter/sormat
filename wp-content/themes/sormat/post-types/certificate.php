<?php
/**
 * Registers the `feedback` post type.
 */
function certificate_init() {
	register_post_type( 'certificate', array(
		'labels'                => array(
			'name'                  => __( 'Сертификаты', 'sormat' ),
			'singular_name'         => __( 'Сертификаты', 'sormat' ),
			'all_items'             => __( 'All Сертификаты', 'sormat' ),
			'archives'              => __( 'Сертификаты Archives', 'sormat' ),
			'attributes'            => __( 'Сертификаты Attributes', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Сертификаты', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Сертификаты', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'feedback', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'feedback', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'feedback', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'feedback', 'sormat' ),
			'filter_items_list'     => __( 'Filter Сертификаты list', 'sormat' ),
			'items_list_navigation' => __( 'Сертификаты list navigation', 'sormat' ),
			'items_list'            => __( 'Сертификаты list', 'sormat' ),
			'new_item'              => __( 'New Сертификаты', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Сертификаты', 'sormat' ),
			'edit_item'             => __( 'Edit Сертификаты', 'sormat' ),
			'view_item'             => __( 'View Сертификаты', 'sormat' ),
			'view_items'            => __( 'View Сертификаты', 'sormat' ),
			'search_items'          => __( 'Search Сертификаты', 'sormat' ),
			'not_found'             => __( 'No Сертификаты found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Сертификаты found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Сертификаты:', 'sormat' ),
			'menu_name'             => __( 'Сертификаты', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'page-attributes' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'feedback',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'certificate_init' );

/**
 * Sets the post updated messages for the `feedback` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `feedback` post type.
 */
function ceritificate_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['certificate'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Сертификаты updated. <a target="_blank" href="%s">View Сертификаты</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Сертификаты updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Сертификаты restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Сертификаты published. <a href="%s">View Сертификаты</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Сертификаты saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Сертификаты submitted. <a target="_blank" href="%s">Preview Сертификаты</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Сертификаты scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Сертификаты</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Сертификаты draft updated. <a target="_blank" href="%s">Preview Сертификаты</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'ceritificate_updated_messages' );

<?php
/**
 * Registers the `Product group` post type.
 */
function productgroup_init() {
	register_post_type( 'productgroup', array(
		'labels'                => array(
			'name'                  => __( 'Product group', 'sormat' ),
			'singular_name'         => __( 'Product group', 'sormat' ),
			'all_items'             => __( 'All Product group', 'sormat' ),
			'archives'              => __( 'Product group Archives', 'sormat' ),
			'attributes'            => __( 'Product group Attributes', 'sormat' ),
			'insert_into_item'      => __( 'Insert into Product group', 'sormat' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Product group', 'sormat' ),
			'featured_image'        => _x( 'Featured Image', 'Product group', 'sormat' ),
			'set_featured_image'    => _x( 'Set featured image', 'Product group', 'sormat' ),
			'remove_featured_image' => _x( 'Remove featured image', 'Product group', 'sormat' ),
			'use_featured_image'    => _x( 'Use as featured image', 'Product group', 'sormat' ),
			'filter_items_list'     => __( 'Filter Product group list', 'sormat' ),
			'items_list_navigation' => __( 'Product group list navigation', 'sormat' ),
			'items_list'            => __( 'Product group list', 'sormat' ),
			'new_item'              => __( 'New Product group', 'sormat' ),
			'add_new'               => __( 'Add New', 'sormat' ),
			'add_new_item'          => __( 'Add New Product group', 'sormat' ),
			'edit_item'             => __( 'Edit Product group', 'sormat' ),
			'view_item'             => __( 'View Product group', 'sormat' ),
			'view_items'            => __( 'View Product group', 'sormat' ),
			'search_items'          => __( 'Search Product group', 'sormat' ),
			'not_found'             => __( 'No Product group found', 'sormat' ),
			'not_found_in_trash'    => __( 'No Product group found in trash', 'sormat' ),
			'parent_item_colon'     => __( 'Parent Product group:', 'sormat' ),
			'menu_name'             => __( 'Product group', 'sormat' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'Product group',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'productgroup_init' );

/**
 * Sets the post updated messages for the `Product group` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `Product group` post type.
 */
function productgroup_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['Product group'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Product group updated. <a target="_blank" href="%s">View Product group</a>', 'sormat' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'sormat' ),
		3  => __( 'Custom field deleted.', 'sormat' ),
		4  => __( 'Product group updated.', 'sormat' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Product group restored to revision from %s', 'sormat' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Product group published. <a href="%s">View Product group</a>', 'sormat' ), esc_url( $permalink ) ),
		7  => __( 'Product group saved.', 'sormat' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Product group submitted. <a target="_blank" href="%s">Preview Product group</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Product group scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Product group</a>', 'sormat' ),
		date_i18n( __( 'M j, Y @ G:i', 'sormat' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Product group draft updated. <a target="_blank" href="%s">Preview Product group</a>', 'sormat' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'productgroup_updated_messages' );

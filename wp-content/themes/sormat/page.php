<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sormat
 */

get_header();
?>

<section id="company" class="main-content-holder">
    <?php get_sidebar('breadcrumbs'); ?>
	<?php while( have_posts() ) : the_post() ?>
  <div class="two-columns single-texts first left">
		<article id="post-<?php the_id() ?>" <?php post_class() ?>>
			<h1><?php the_title() ?></h1>
			<?php the_content() ?>
		</article>
	</div>
	<?php endwhile ?>

  <div class="clear"></div>
</section>

<?php
get_footer();

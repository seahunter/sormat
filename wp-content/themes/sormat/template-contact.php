<?php 
/**
 * Template Name: Контакты
 */
get_header(); ?>
<section id="contact" class="main-content-holder">
<?php get_sidebar('breadcrumbs'); ?>
  <div class="two-columns single-texts left first">
    <?php while( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_id() ?>" <?php post_class() ?>>
      <h1><?php the_title() ?></h1>
      <?php the_content() ?>
    </article>
    <?php endwhile ?>
  </div>

  <div class="clear"></div>

  <div class="two-columns single-texts first" id="contact-form-container">

    
    <div class="blue-ruler"></div>

        
    <form action="<?php echo admin_url('/admin-ajax.php?action=feedback') ?>" method="post" id="ask-anything-form">

        <fieldset class="detail-fields">
            <input type="text" id="name" name="contact_name" placeholder="Имя" required="" value="">
            <input type="text" id="company" name="contact_company" placeholder="Компания" required="" value="">
            <input type="text" id="email-or-phone" name="contact_email" placeholder="Email или номер телефона" required="" value="">
            <input type="text" id="title" name="contact_subject" placeholder="Тема" required="" value="">
        </fieldset>

        <fieldset class="message-fields">
          <?php $questions = get_field('questions') ?>
          <?php if( ! empty( $questions ) ) : ?>
          <select name="contact_general-question" id="general-question-select" required="" class="hasCustomSelect">
            <option value="" disabled="disabled">Select enquiry type</option>
            <?php foreach( $questions as $question ) : ?>
              <option value="<?php echo $question['title'] ?>"><?php echo $question['title'] ?></option>
            <?php endforeach; ?>
          </select>
          <?php endif ?>
          <textarea name="contact_free_field" id="free-field" required="" placeholder="Please enter details"></textarea>
          <input type="submit" class="submit-contact-form" name="contact-submit" value="Отправить">

        </fieldset>
    </form>     
  </div>
  <div class="clear"></div>
</section>
<?php get_footer();
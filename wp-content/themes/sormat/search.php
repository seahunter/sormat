<?php

$products_args = [
    'post_type' => ['product'],
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'tax_query' => [
        [
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => ['ankery-shurupy', 'krepleniya-dlya-listovyh-i-pustotelnyh-materialov', 'raspornye-ankery'],
            'operator' => 'IN'
        ]
    ]
];

$products = new WP_Query($products_args);
echo $products->post_count;
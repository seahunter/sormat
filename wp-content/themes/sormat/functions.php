<?php
/**
 * sormat functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sormat
 */

require 'vendor/autoload.php';

if ( ! function_exists( 'sormat_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sormat_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on sormat, use a find and replace
		 * to change 'sormat' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sormat', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			[
				'menu-1' => esc_html__( 'Primary', 'sormat' ),
			]
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5', [
					   'search-form',
					   'comment-form',
					   'comment-list',
					   'gallery',
					   'caption',
				   ]
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background', apply_filters(
								   'sormat_custom_background_args', [
																	  'default-color' => 'ffffff',
																	  'default-image' => '',
																  ]
							   )
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo', [
							 'height'      => 250,
							 'width'       => 250,
							 'flex-width'  => true,
							 'flex-height' => true,
						 ]
		);
	}
endif;
add_action( 'after_setup_theme', 'sormat_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sormat_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'sormat_content_width', 640 );
}

add_action( 'after_setup_theme', 'sormat_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sormat_widgets_init() {
	register_sidebar(
		[
			'name'          => esc_html__( 'Sidebar', 'sormat' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'sormat' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		]
	);
}

add_action( 'widgets_init', 'sormat_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function sormat_scripts() {
	wp_enqueue_style( 'sormat-style', get_stylesheet_uri() );
	wp_enqueue_style( 'sormat-reset', get_theme_file_uri( '/app/css/reset.css' ) );
	wp_enqueue_style( 'sormat-owl-carousel', get_theme_file_uri( '/app/js/plugins/owl-carousel/owl.carousel.css' ) );
	wp_enqueue_style( 'sormat-owl-theme', get_theme_file_uri( '/app/js/plugins/owl-carousel/owl.theme.css' ) );
	wp_enqueue_style( 'sormat-qtip', get_theme_file_uri( '/app/css/jquery.qtip.min.css' ) );
	wp_enqueue_style( 'sormat-magnific-popup', get_theme_file_uri( '/app/css/magnific-popup.css' ) );
	wp_enqueue_style( 'sormat-styles', get_theme_file_uri( '/app/css/styles.css' ) );
	wp_enqueue_style( 'sormat-print', get_theme_file_uri( '/app/css/print.css' ), [], false, 'print' );
	wp_enqueue_style( 'sormat-lightbox', get_theme_file_uri( '/app/css/lightbox.css' ) );
	wp_enqueue_style( 'sormat-leaflet', get_theme_file_uri( '/app/js/leaflet/leaflet.css' ) );
	wp_enqueue_style( 'sormat-leaflet-marketcluster', get_theme_file_uri( '/app/js/MarkerCluster.css' ) );
	wp_enqueue_style( 'sormat-leaflet-marketcluster-theme', get_theme_file_uri( '/app/js/MarkerCluster.Default.css' ) );
	wp_enqueue_style( 'sormat-new', get_theme_file_uri( '/app/css/sormat.css' ) );


	wp_enqueue_script( 'sormat-respond', get_theme_file_uri( '/app/js/respond.js' ) );
	wp_enqueue_script( 'sormat-jquery-ui', get_theme_file_uri( '/app/js/jquery-ui-1.10.0.custom.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-modernize-js', get_theme_file_uri( '/app/js/modernizr.custom.js' ), [], false, true );
	wp_enqueue_script( 'sormat-customSelect', get_theme_file_uri( '/app/js/jquery.customSelect.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-tablesorter', get_theme_file_uri( '/app/js/jquery.tablesorter.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-lightbox', get_theme_file_uri( '/app/js/lightbox.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-magnific-popup', get_theme_file_uri( '/app/js/jquery.magnific-popup.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-qtip', get_theme_file_uri( '/app/js/jquery.qtip.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-main', get_theme_file_uri( '/app/js/main.js' ), [], false, true );
	wp_enqueue_script( 'sormat-hyphenate', get_theme_file_uri( '/app/js/hyphenate.min.js' ), [], false, true );
	wp_enqueue_script( 'sormat-leaflet', get_theme_file_uri( '/app/js/leaflet/leaflet.js' ), [], false, true );
	wp_enqueue_script( 'sormat-leaflet-google', get_theme_file_uri( '/app/js/leaflet-plugin/Google.js' ), [], false, true );
	wp_enqueue_script( 'sormat-leaflet-markercluster', get_theme_file_uri( '/app/js/leaflet/leaflet.markercluster.js' ), [], false, true );
	wp_enqueue_script( 'sormat-lazysizes', get_theme_file_uri( '/app/js/lazysizes.min.js' ), [], false, true );

	wp_localize_script( 'sormat-main', 'themeURL', get_home_url() );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'sormat_scripts' );

function sormat_admin_scripts() {
	wp_enqueue_style( 'sormat-select2', get_theme_file_uri( '/app/css/select2.css' ) );
	wp_enqueue_style( 'sormat-admin', get_theme_file_uri( '/app/css/admin.css' ) );

	wp_enqueue_script( 'sormat-select2', get_theme_file_uri( '/app/js/select2.js' ), [], false, true );
	wp_enqueue_script( 'sormat-admin', get_theme_file_uri( '/app/js/admin.js' ), [], false, true );
}

add_action( 'admin_enqueue_scripts', 'sormat_admin_scripts' );

if ( ! is_admin() ) {
	require get_template_directory() . '/inc/ajax-forms.php';
	require get_template_directory() . '/inc/filter-query.php';
}
require get_template_directory() . '/inc/acf.php';
require get_template_directory() . '/post-types/post-types.php';
require get_template_directory() . '/taxonomies/taxonomies.php';
require get_template_directory() . '/metaboxes/product-metabox.php';
require get_template_directory() . '/metaboxes/order.php';
require get_template_directory() . '/inc/class-geolocation.php';
require get_template_directory() . '/inc/class-sormat-geolocation.php';
require get_template_directory() . '/inc/woocommerce-functions.php';


function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_filter( 'upload_mimes', 'custom_upload_mimes' );
function custom_upload_mimes( $existing_mimes = [] ) {
	// add your extension to the mimes array as below
	$existing_mimes['zip'] = 'application/zip';
	$existing_mimes['gz']  = 'application/x-gzip';
	return $existing_mimes;
}

function product_cat_image_url( $term_id ) {
	$thumbnail_id = get_woocommerce_term_meta( $term_id, 'thumbnail_id', true );
	return wp_get_attachment_url( $thumbnail_id );
}

function build_sormat_meta_query( $data ) {
	$result = [];
	if ( ! empty( $data ) ) {
		foreach ( $data as $key => $item ) {
			$meta     = [
				'taxonomy' => $key,
				'field'    => 'slug',
				'terms'    => $item,
				'operator' => 'IN',
			];
			$result[] = $meta;
		}
	}

	return $result;
}

function sormat_ajax_product_query( $tax_query ) {
	$products_args = [
		'post_type'      => [ 'productgroup' ],
		'post_status'    => 'publish',
		'posts_per_page' => -1,
		'tax_query'      => $tax_query,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
	];

	$products = new WP_Query( $products_args );
	$results  = [];

	if ( $products->have_posts() ) {
		while ( $products->have_posts() ) : $products->the_post();
			$product_type = (int)get_field( 'product_type' );
			if ( 0 < $product_type ) {
				$product_type_title               = get_field( 'producttype_title_ru', $product_type );
				$results[ $product_type_title ][] = get_post();
			}
		endwhile;
		wp_reset_postdata();
	}

	return $results;
}

function sormat_get_products() {
	if ( ! empty( $_POST ) ) {
		$product_types = sormat_ajax_product_query( build_sormat_meta_query( $_POST ) );

		ob_start();

		include get_template_directory() . '/template-parts/search.php';

		$content = ob_get_contents();

		ob_end_clean();

		wp_send_json_success( $content );
		die();

	}
}


add_action( 'wp_ajax_sormat_get_products', 'sormat_get_products' );
add_action( 'wp_ajax_nopriv_sormat_get_products', 'sormat_get_products' );

/**
 * Add custom classes to body tag
 *
 * @param $classes
 * @param $class
 *
 * @return array
 */
function custom_body_class_filter( $classes, $class ) {
	if ( is_product() ) {
		$classes[] = 'page-template-product-page';
	}
	return $classes;
}

add_filter( 'body_class', 'custom_body_class_filter', 10, 2 );

function get_relative_products( int $product_id ) {
	$parent = get_field( 'product_group' );
	if ( empty( $parent ) ) {
		$relative_products_args = [
			'post_type'      => 'product',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			'meta_query'     => [
				[
					'key'     => 'product_group',
					'value'   => $product_id,
					'compare' => '=',
				],
			],
			'tax_query'      => [
				[
					'taxonomy' => 'publishingchannels',
					'field'    => 'slug',
					'terms'    => 'print',
				],
			],
			'orderby'        => 'title',
			'order'          => 'ASC',
		];

		$relative_products = new WP_Query( $relative_products_args );
		return 0 < $relative_products->post_count ? $relative_products : null;
	} else {
		return null;
	}
}

function get_main_image( string $title, string $size = 'thumbnail' ) {
	global $wpdb;
	$id         = wp_cache_get( 'main_image_' . $title );
	$src        = '';
	$attachment = [];
	if ( empty( $id ) ) {
		$query = "SELECT ID FROM {$wpdb->posts} WHERE post_title = '%s' AND post_type = 'attachment'";
		$id    = $wpdb->get_var( $wpdb->prepare( $query, $title ) );
		wp_cache_set( 'main_image_' . $title, $id );
	}

	return $id;
}

function correct_img_url( string $url ) {
	$image_path = 'https://www.sormat.com';
	return $image_path . parse_url( $url )['path'];
}

/**
 * Enter the key of meta fields and array of meta fields.
 *
 * @param array  $meta_fields
 * @param string $key
 *
 * @return string
 */
function get_product_meta( array $meta_fields, string $key ) : string {
	return isset( $meta_fields[ $key ][0] ) ? $meta_fields[ $key ][0] : '';
}

/**
 * Build column technical data
 *
 * @param array $data Meta data.
 *
 * @return array
 */
function build_setup_columns( array $data ) : array {
	$column1 = [
		'nominal_size'                => 'Размер',
		'total_length'                => 'Длина',
		'thread_length'               => 'Резьба',
		'nut_width_across_flats'      => 'Размер гайки под ключ',
		'width_across_flats_other'    => 'Размер под ключ, другой',
		'inner_thread_diameter'       => 'Внутренняя резьба',
		'drive'                       => 'Шлиц',
		'sheet_thickness_min'         => 'Мин.толщина листа (Tsheet, min)',
		'fixture_thickness'           => 'Макс.толщина прикрепляемого материала (Tfix )',
		'free_space_behind_the_sheet' => 'Свободное пространство за листом',
		'suitable_screws_diameter'    => 'Подходящие шурупы Диаметр',
		'suitable_screws_length'      => 'Подходящие шурупы Длина',
		'net_distance_to_structure'   => 'Чистое расстояние до конструкции',
		'for_pipe'                    => 'Для трубы',
		'suitable_diameter'           => 'Подходящий диаметр, Ø mm',
		'material_thickness'          => 'Толщина материала',
		'height'                      => 'Высота',
		'outer_thread_length'         => 'Длина внешней резьбы',
		'nail'                        => 'Гвоздь, mm (Ø x Длина)',
		'bundle_max'                  => 'Пучок max Ø',
		'strength_of_the_tie'         => 'Прочность сцепления',
		'hole_sizes_small'            => 'Размеры отверстий, mm (Ø маленький )',
		'hole_sizes_large'            => 'Размеры отверстий, mm (Ø большой)',
		'break_load'                  => 'Разрушающая нагрузка',
		'core_wire'                   => 'Сердечник провода',
		'outer_diameter'              => 'Внешний диаметр',
		'opening'                     => 'Зев',
		'number_of_holes'             => 'Количество отверстий',
		'hole_diameter_measurements'  => 'Отверстие Ø/размеры',
		'outer_diameter_hvac'         => 'Внешний Ø',
		'inner_diameter'              => 'Внутренний Ø',
	];

	$column2 = [
		'hole_in_fixture'           => 'Отверстие в прикрепляемом материале (Df)',
		'drill_hole_diameter'       => 'Диаметр сверла (d0)',
		'minimum_hole_depth'        => 'Мин.глубина отверстия',
		'total_length'              => 'Глубина отверстия (h1)',
		'nominal_setting_depth'     => 'Расчетная глубина анкеровки (Hnom)',
		'effective_anchorage_depth' => 'Эффективная глубина анкеровки (Hef)',
		'installation_torque'       => 'Момент затяжки (Tinst)',
		'effective_drilling_depth'  => 'Эффективная глубина сверления',
	];

	$column3 = [
		'head_diameter_anchor' => 'Диаметр головки ⌀',
		'opening_measurements' => 'Внутренний диаметр крюка',
		'head_diameter'        => 'Внутреннее отверстие / Диаметр петли ⌀',
		'head_height'          => 'Высота шляпики',
		'flange_diameter'      => 'Диаметр фланца ⌀',
		'diameter_other'       => 'Диаметр, другой',
	];

	$result1 = [];
	$result2 = [];
	$result3 = [];

	if ( ! empty( $data ) ) {
		foreach ( $data as $key => $item ) {
			$keys1 = array_keys( $column1 );
			$keys2 = array_keys( $column2 );
			$keys3 = array_keys( $column3 );
			if ( in_array( $key, $keys1 ) ) {
				$result1[ $column1[ $key ] ] = $item[0];
			} elseif ( in_array( $key, $keys2 ) ) {
				$result2[ $column2[ $key ] ] = $item[0];
			} elseif ( in_array( $key, $keys3 ) ) {
				$result3[ $column3[ $key ] ] = $item[0];
			}
		}
	}

	return [
		$result1,
		$result2,
		$result3,
	];
}

/**
 * Build product list table for category
 *
 * @param WP_Query $data WP_Query of products
 *
 * @return array
 */
function build_product_list_table( WP_Query $data ) {
	if ( $data->have_posts() ) {
		$fields  = [
			'type'                => 'Тип/Вид',
			'code'                => 'Код',
			'product_approvals'   => 'Одобрение ETA',
			'nominal_size'        => 'Размер',
			'total_length'        => 'Длина',
			'drill_hole_diameter' => 'Диаметр отверстия',
			'fixture_thickness'   => 'Толщина прикрепляемой детали',
			'suitable_diameter'   => 'Подходящий диаметр',
		];
		$keys    = array_keys( $fields );
		$columns = array_fill_keys( $keys, false );
		$posts   = [];

		while ( $data->have_posts() ) {
			$data->the_post();
			$type                = get_field( 'type' );
			$code                = get_the_title();
			$approvals           = ! empty( get_field( 'product_approvals' ) ) ? 'Да' : 'Нет';
			$size                = get_field( 'nominal_size' );
			$length              = get_field( 'total_length' );
			$drill_hole_diameter = get_field( 'drill_hole_diameter' );
			$fixture_thickness   = get_field( 'fixture_thickness' );
			$suitable_diameter   = get_field( 'suitable_diameter' );

			$current_product = [
				'type'                => $type,
				'code'                => $code,
				'product_approvals'   => $approvals,
				'nominal_size'        => $size,
				'total_length'        => $length,
				'drill_hole_diameter' => $drill_hole_diameter,
				'fixture_thickness'   => $fixture_thickness,
				'suitable_diameter'   => $suitable_diameter,
			];

			foreach ( $current_product as $key => $value ) {
				if ( false === $columns[ $key ] && ! empty( $value ) ) {
					$columns[ $key ] = true;
				}
			}

			$posts[ get_the_id() ] = $current_product;
		}
		wp_reset_postdata();

		foreach ( $columns as $key => $value ) {
			if ( true === $value ) {
				$columns[ $key ] = $fields[ $key ];
			} else {
				unset( $columns[ $key ] );
			}
		}

		return [
			'columns' => $columns,
			'posts'   => $posts,
		];
	} else {
		return [];
	}
}

/**
 * Build package info string from meta field
 *
 * @param int $product_id
 *
 * @return string
 */
function get_package_info( int $product_id ) {
	$names = [
		'PCE'  => 'шт.',
		'NMP'  => 'упак.',
		'NPL'  => 'опт.кор.',
		'PALL' => 'паллет.',
	];
	$keys  = array_keys( $names );

	$result  = '';
	$package = get_post_meta( $product_id, 'product_package' );
	if ( ! empty( $package ) ) {
		foreach ( $package as $item ) {
			wp_parse_str( $item, $info );
			if ( ! empty( $info ) ) {
				foreach ( $info as $key => $value ) {
					if ( 'type' === $key && in_array( $value, $keys ) ) {
						$result .= $names[ $value ] . ': ';
					}
					if ( 'size' === $key ) {
						$result .= (int)$value . ' /';
					}
				}
			}
		}
	}
	return trim( $result, ' /' );
}

function convertToHoursMins( $time, $format = '%02d:%02d' ) {
	if ( $time < 1 ) {
		return;
	}
	$hours   = floor( $time / 60 );
	$minutes = ($time % 60);
	return sprintf( $format, $hours, $minutes );
}

function filter_woocommerce_price() {
	return true;
}

add_filter( 'woocommerce_is_purchasable', 'filter_woocommerce_price', 10 );

function sort_images_by_type( array $images, int $parent = 0 ) {
	$result = [];
	if ( ! empty( $images ) ) {
		foreach ( $images as $image ) {
			$object = get_post( $image );
			if ( $object->post_parent === $parent ) {
				$terms = wp_get_post_terms( $image, 'media-category' );
				if ( ! empty( $terms ) ) {
					foreach ( $terms as $term ) {
						if ( $term instanceof WP_Term ) {
							$result[ $term->slug ][] = $image;
						}
					}
				}
			}
		}
	}


	return $result;
}

function get_sormat_image( int $id ) : int {
	$meta = get_post_meta( $id, 'image_order', true );
	if ( '' !== $meta ) {
		$list = explode( ',', $meta );
		$list = sort_images_by_type( $list, $id );
		if ( ! empty( $list['featured-image'][0] ) ) {
			return $list['featured-image'][0];
		}
	}

	return 0;
}


add_action( 'woocommerce_admin_order_item_headers', 'my_woocommerce_admin_order_item_headers' );
function my_woocommerce_admin_order_item_headers() {
	// set the column name
	$column_name         = 'Цена Sormat';
	$column_name_comment = 'Комментарии';

	// display the column name
	echo '<th>' . $column_name_comment . '</th>';
	echo '<th class="item_cost custom-cost">' . $column_name . '</th>';
}

// Add custom column values here
add_action( 'woocommerce_admin_order_item_values', 'my_woocommerce_admin_order_item_values', 10, 3 );
function my_woocommerce_admin_order_item_values( $_product, $item, $item_id = null ) {
	set_query_var( '_product', $_product );
	set_query_var( 'item', $item );
	set_query_var( 'item_id', $item_id );
	get_template_part( 'metaboxes/order-item-comment' );
	if ( null !== $_product ) {
		get_template_part( 'metaboxes/order-item' );
	}
}

add_filter( 'woocommerce_hidden_order_itemmeta', 'sormat_hidden_meta', 10, 1 );

function sormat_hidden_meta( $list ) {
	$list[] = '_sormat_price';
	$list[] = 'sormat_item_comment';
	return $list;
}

add_action( 'woocommerce_saved_order_items', 'save_sormat_price', 10, 2 );
function save_sormat_price( $order_id, $items ) {
	if ( ! empty( $items['sormat_item_comment'] ) ) {
		foreach ( $items['sormat_item_comment'] as $key => $item ) {
			wc_update_order_item_meta( $key, 'sormat_item_comment', $item );
		}
	}
	if ( ! empty( $items['sormat_price'] ) ) {
		foreach ( $items['sormat_price'] as $key => $item ) {
			$total_price = absint( $items['order_item_qty'][ $key ] ) * $item;
			wc_update_order_item_meta( $key, '_sormat_price', $item );
			wc_update_order_item_meta( $key, '_line_total', $total_price );
		}
	}

}

add_filter( 'woocommerce_checkout_fields', 'sormat_checkout_fields', 10, 1 );

function sormat_checkout_fields( $fields ) {
	$repeate_password                                 = [
		'type'        => 'password',
		'label'       => 'Повторите пароль',
		'required'    => true,
		'placeholder' => 'Повторите пароль',
	];
	$fields['billing']['billing_company']['required'] = true;
	$fields['order']['order_comments']['label']       = 'Примечание к запросу';
	$fields['order']['order_comments']['placeholder'] = 'Ваши пожелания';
	$fields['account']['repeate_password']            = $repeate_password;
	return $fields;
}

add_filter( 'woocommerce_thankyou_order_received_text', 'sormat_success_order_text', 10, 1 );

function sormat_success_order_text() {
	return 'Спасибо. Ваш запрос был принят.';
}

function sormat_title_order_received( $title, $id ) {
	if ( is_order_received_page() && get_the_ID() === $id ) {
		$title = "Запрос принят";
	}

	return $title;
}

add_filter( 'the_title', 'sormat_title_order_received', 10, 2 );

add_action( 'woocommerce_thankyou', 'woocommerce_thankyou_change_order_status', 10, 1 );
function woocommerce_thankyou_change_order_status( $order_id ) {
	if ( ! $order_id ) return;

	$order       = wc_get_order( $order_id );
	$user_region = mb_strtolower( sanitize_text_field( wp_unslash( $_COOKIE['sormat_user_location'] ) ) );
	update_field( 'customer_region', $user_region, $order_id );

	if ( $order->get_status() == 'processing' )
		$order->update_status( 'pending' );
}

add_filter( 'woocommerce_account_menu_items', 'sormat_remove_my_account_links' );
function sormat_remove_my_account_links( $menu_links ) {

	unset( $menu_links['dashboard'] ); // Dashboard

	//unset( $menu_links['payment-methods'] ); // Remove Payment Methods
	//unset( $menu_links['orders'] ); // Remove Orders
	//unset( $menu_links['downloads'] ); // Disable Downloads
	//unset( $menu_links['edit-account'] ); // Remove Account details tab
	//unset( $menu_links['customer-logout'] ); // Remove Logout link

	return $menu_links;

}

function sormat_order_statuses() {
	$statuses = [
		'directed' => 'Направлен',
		'expired'  => 'Просрочен',
		'success'  => 'Подтверждён',
		'cancel'   => 'Отменён',
	];
	return apply_filters( 'sormat_order_status', $statuses );
}

function sormat_update_order_status() {
	if ( empty( $_POST ) || ! wp_verify_nonce( $_POST['sormat_form']['update_order_status'], 'sormat_form' ) ) {
		wp_send_json_error( 'Произошла ошибка, повторите позже' );
	} else {
		$options = $_POST['sormat_options'];
		if ( ! empty( $options['order_status'] ) ) {
			$status = sanitize_text_field( $options['order_status'] );
			if ( '' !== $status ) {
				update_field( 'order_status', $status, $options['order_id'] );
			}
			switch ( $status ) {
				case 'success':
					wp_send_json_success( 'Заказ успешно подтвеждён' );
					break;
				case 'cancel':
					wp_send_json_success( 'Заказ успешно отменён' );
					break;
				default:
					wp_send_json_success( $status );
			}
		}
	}
	wp_die();
}

add_action( 'wp_ajax_update_sormat_order_status', 'sormat_update_order_status' );
function sormat_wc_register_post_statuses() {

	register_post_status(
		'wc-client-cancel', [
							  'label'                     => _x( 'Клиент отказался', 'WooCommerce Order status', 'sormat' ),
							  'public'                    => true,
							  'exclude_from_search'       => false,
							  'show_in_admin_all_list'    => true,
							  'show_in_admin_status_list' => false,
							  'label_count'               => _n_noop( 'Клиент отказался (%s)', 'Клиент отказался (%s)', 'sormat' ),
						  ]
	);

	register_post_status(
		'wc-manager-directed', [
								 'label'                     => _x( 'Отправлено менеджеру', 'WooCommerce Order status', 'sormat' ),
								 'public'                    => true,
								 'exclude_from_search'       => false,
								 'show_in_admin_all_list'    => true,
								 'show_in_admin_status_list' => false,
								 'label_count'               => _n_noop( 'Отправлено менеджеру (%s)', 'Отправлено менеджеру (%s)', 'sormat' ),
							 ]
	);

	register_post_status(
		'wc-client-call', [
							'label'                     => _x( 'Звонок клиенту', 'WooCommerce Order status', 'sormat' ),
							'public'                    => true,
							'exclude_from_search'       => false,
							'show_in_admin_all_list'    => true,
							'show_in_admin_status_list' => false,
							'label_count'               => _n_noop( 'Звонок клиенту (%s)', 'Звонок клиенту (%s)', 'sormat' ),
						]
	);

	register_post_status(
		'wc-invoiced', [
						 'label'                     => _x( 'Выставлен счёт', 'WooCommerce Order status', 'sormat' ),
						 'public'                    => true,
						 'exclude_from_search'       => false,
						 'show_in_admin_all_list'    => true,
						 'show_in_admin_status_list' => false,
						 'label_count'               => _n_noop( 'Выставлен счёт (%s)', 'Выставлен счёт (%s)', 'sormat' ),
					 ]
	);

	register_post_status(
		'wc-shipped', [
						'label'                     => _x( 'Отгружен товар', 'WooCommerce Order status', 'sormat' ),
						'public'                    => true,
						'exclude_from_search'       => false,
						'show_in_admin_all_list'    => true,
						'show_in_admin_status_list' => false,
						'label_count'               => _n_noop( 'Отгружен товар (%s)', 'Отгружен товар (%s)', 'sormat' ),
					]
	);


	register_post_status(
		'wc-expired',
		[
			'label'                     => _x( 'Просрочено', 'WooCommerce Order Status', 'sormat' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => false,
			'label_count'               => _n_noop( 'Просрочено (%s)', 'Просрочено (%s)', 'sormat' ),
		]
	);

	register_post_status(
		'wc-expired-operator',
		[
			'label'                     => _x( 'Просрочена отправка', 'WooCommerce Order Status', 'sormat' ),
			'public'                    => true,
			'exclude_from_search'       => false,
			'show_in_admin_all_list'    => true,
			'show_in_admin_status_list' => false,
			'label_count'               => _n_noop( 'Просрочена отправка (%s)', 'Просрочена отправка (%s)', 'sormat' ),
		]
	);
}

add_filter( 'init', 'sormat_wc_register_post_statuses' );

add_action( 'woocommerce_account_content', 'update_sormat_status' );


add_filter( 'wc_order_statuses', 'wc_renaming_order_status' );
function wc_renaming_order_status( $order_statuses ) {
	unset( $order_statuses['wc-on-hold'] );
	unset( $order_statuses['wc-refunded'] );
	//	unset( $order_statuses['wc-processing'] );
	unset( $order_statuses['wc-completed'] );
	unset( $order_statuses['wc-failed'] );
	unset( $order_statuses['wc-cancelled'] );

	foreach ( $order_statuses as $key => $status ) {
		if ( 'wc-pending' === $key ) {
			$order_statuses['wc-pending'] = _x( 'В обработке', 'В ожидании подтверждения', 'woocommerce' );
		}
	}

	$order_statuses['wc-manager-directed'] = _x( 'Отправлено менеджеру', 'WooCommerce Order status', 'sormat' );
	$order_statuses['wc-client-call']      = _x( 'Звонок клиенту', 'WooCommerce Order status', 'sormat' );
	$order_statuses['wc-invoiced']         = _x( 'Выставлен счёт', 'WooCommerce Order status', 'sormat' );
	$order_statuses['wc-shipped']          = _x( 'Отгружен товар', 'WooCommerce Order status', 'sormat' );
	$order_statuses['wc-client-cancel']    = _x( 'Клиент отказался', 'WooCommerce Order status', 'sormat' );
	$order_statuses['wc-expired-operator'] = _x( 'Просрочена отправка', 'WooCommerce Order status', 'sormat' );
	$order_statuses['wc-expired']          = _x( 'Просрочен', 'WooCommerce Order status', 'sormat' );

	return $order_statuses;
}

function sormat_order_user_cancel( int $order_id ) {
	$actual_link = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	return $actual_link . '?order-status=cancel&order_id=' . $order_id;
}


function update_sormat_status() {
	$actual_link = (isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if ( ! empty( $_GET['order-status'] ) && ! empty( $_GET['order_id'] ) ) {
		$order_id     = (int)$_GET['order_id'];
		$status       = sanitize_text_field( $_GET['order-status'] );
		$order        = wc_get_order( $order_id );
		$current_user = get_current_user_id();
		if ( $current_user === $order->get_user_id() ) {
			if ( 'pending' === $order->get_status() ) {
				if ( 'cancel' === $status ) {
					$order->update_status( 'client-cancel' );
				}
			}
			if ( 'processing' === $order->get_status() ) {
				if ( 'approve' === $status ) {
					$order->update_status( 'client-approved' );
				} elseif ( 'cancel' === $status ) {
					$order->update_status( 'client-cancel' );
				}
			}
		}
	}
}

add_action( 'init', 'update_sormat_status' );

function sormat_change_order_actions( $actions, $order ) {
	$status = $order->get_status();
	if ( 'pending' === $status || 'completed' === $status || 'cancelled' === $status || 'failed' === $status || 'client-cancel' === $status ) {
		unset( $actions['view'] );
	}
	if ( 'client-approved' === $status || 'client-cancel' === $status ) {
		unset( $actions['cancel'] );
	}
	if ( isset( $actions['cancel'] ) ) {
		$actions['cancel']['url'] = sormat_order_user_cancel( $order->get_id() );
	}
	return $actions;
}

add_filter( 'woocommerce_my_account_my_orders_actions', 'sormat_change_order_actions', 10, 2 );

function get_translated_term_name( int $id ) {
	$option = get_option( 'taxonomy_' . $id );

	return $option['term_name_ru'] ?: '';
}

add_action( 'woocommerce_register_form_start', 'sormat_register_form' );

function sormat_register_form() {
	get_template_part( 'template-parts/forms/registration' );
}

add_action( 'user_register', 'sormat_update_user_meta', 10, 1 );

function sormat_update_user_meta( int $user_id ) {

	if ( ! empty( $_POST['account_first_name'] ) ) {
		update_user_meta( $user_id, 'first_name', sanitize_text_field( $_POST['account_first_name'] ) );
	}
	if ( ! empty( $_POST['account_last_name'] ) ) {
		update_user_meta( $user_id, 'last_name', sanitize_text_field( $_POST['account_last_name'] ) );
	}
}

function get_geolocation_cities() {
	$cities_query = new WP_Query(
		[
			'post_type'      => 'geolocation',
			'posts_per_page' => -1,
			'post_status'    => 'publish',
		]
	);
	$cities_list  = [];
	if ( $cities_query->have_posts() ) {
		while ( $cities_query->have_posts() ) {
			$cities_query->the_post();
			$cities_list[] = mb_strtolower( get_the_title() );
		}
	}
	wp_reset_postdata();

	return $cities_list;
}

function get_user_location() {
	$cities_list = get_geolocation_cities();
	$cookie      = ! empty( $_COOKIE['sormat_user_location'] ) ? mb_strtolower( sanitize_text_field( wp_unslash( $_COOKIE['sormat_user_location'] ) ) ) : '';
	$param       = ! empty( $_GET['sl'] ) ? mb_strtolower( sanitize_text_field( wp_unslash( $_GET['sl'] ) ) ) : '';
	if ( ! empty( $param ) && in_array( $param, $cities_list, true ) ) {
		return $param;
	}
	if ( ! empty( $cookie ) && in_array( $cookie, $cities_list, true ) ) {
		return sanitize_text_field( wp_unslash( $_COOKIE['sormat_user_location'] ) );
	}


	return null;
}

function getUserTime() {
	return '<script type="text/javascript">
var x = new Date()
document.write(x.getHours());
</script>';
}

function change_user_location( string $new_location ) {
	$cities_list = get_geolocation_cities();
	$expire      = 60 * 60 * 24 * 30; // One month
	//	$expire           = 60; // One minute
	$new_location     = mb_strtolower( sanitize_text_field( wp_unslash( $new_location ) ) );
	$current_location = ! empty( $_COOKIE['sormat_user_location'] ) ? $_COOKIE['sormat_user_location'] : '';
	if ( $current_location !== $new_location ) {
		if ( in_array( $new_location, $cities_list, true ) ) {
			update_field( 'user_region', $new_location, 'user_' . get_current_user_id() );
			setcookie( 'sormat_user_location', $new_location, time() + $expire, '/' );
		}
	}
}

if ( ! empty( $_GET['sl'] ) ) {
	change_user_location( $_GET['sl'] );
}

add_action( 'added_post_meta', 'sormat_order_timeout', 10, 4 );
add_action( 'updated_post_meta', 'sormat_order_timeout', 10, 4 );
function sormat_order_timeout( $meta_id, $post_id, $meta_key, $meta_value ) {
	if ( 'manager_callback' == $meta_key ) {
		if ( '1' === $meta_value ) {
			update_post_meta( $post_id, 'sormat_order_timeout', time() );
		} else {
			delete_post_meta( $post_id, 'sormat_order_timeout' );
		}
	}
}

add_action( 'sormat_cron_hook', 'sormat_cron_order_statuses' );

add_filter( 'cron_schedules', 'sormat_cron_interval' );

function sormat_cron_interval( $timeout_list ) {
	$timeout_list['5_minutes'] = [
		'interval' => 300,
		'display'  => 'Каждые 5 минуты' // отображаемое имя
	];
	return $timeout_list;
}

function check_expired_times( $order, $value = 'hour', $time = null ) {
	$work_days = [ 'Monday', 'Tuesday', 'Wednesday', 'Thursday' ];
	$timezone  = new DateTimeZone( 'Europe/Moscow' );
	$monday    = new DateTime( 'next monday 12:00', $timezone );

	$now           = new DateTime( 'now', $timezone );
	$order_date    = new DateTime( $order->order_date, $timezone );
	$order_timeout = (int)get_post_meta( $order->ID, 'sormatl_order_timeout', true );
	$order_timeout = new DateTime( $order_timeout, $timezone );

	if ( $now->format( 'G' ) < 15 && in_array( $now->format( 'l' ), $work_days, true ) ) {
		if ( 'hour' === $value ) {
			return $order_date->modify( '+1 hour' );
		} elseif ( 'day' === $value && $order_timeout ) {
			return $order_timeout->modify( '+1 day' );
		} elseif ( 'ten_days' === $value && $order_timeout ) {
			return $order_timeout->modify( '+10 days' );
		}
	}

	if ( $now->format( 'G' ) > 15 && in_array( $now->format( 'l', $work_days, true ) ) ) {
		if ( 'hour' === $value ) {
			return $order_date->modify( '+1 day 12:00' );
		} elseif ( 'day' === $value ) {
			$order_timeout->modify( '+1 day' );
		} elseif ( 'ten_days' === $value ) {
			$order_timeout->modify( '+10 days' );
		}
	}

	if ( 'Friday' === $now->format( 'l' ) && $now->format( 'G' ) < 15 ) {
		if ( 'hour' === $value ) {
			return $order_date->modify( '+1 hour' );
		} elseif ( 'day' === $value ) {
			return $monday;
		} elseif ( 'ten_days' === $value ) {
			return $order_timeout->modify( '+10 days' );
		}
	}

	if ( 'Friday' === $now->format( 'l' ) && $now->format( 'G' ) > 15 ) {
		if ( 'hour' === $value ) {
			return $monday;
		} elseif ( 'day' === $value ) {
			return $order_timeout->modify( '+1 days' );
		} elseif ( 'ten_days' === $value ) {
			return $order_timeout->modify( '+10 days' );
		}
	}

	return false;
}


function sormat_cron_order_statuses() {
	$query  = new WC_Order_Query(
		[
			'limit'   => -1,
			'status'  => [ 'wc-manager-directed', 'wc-pending', 'wc-directed' ],
			'orderby' => 'date',
			'order'   => 'DESC',
			'return'  => 'ids',
		]
	);
	$orders = $query->get_orders();

	if ( ! empty( $orders ) ) {
		foreach ( $orders as $order_id ) {
			$current_time = (int)time();
			$callback     = get_field( 'manager_callback', $order_id );
			$hour         = 60 * 60;
			$day          = $hour * 24 * 3;
			$ten_days     = $day * 10;
			$order        = new WC_Order( $order_id );
			$timezone     = new DateTimeZone( 'Europe/Moscow' );

			$now        = new DateTime( 'now', $timezone );
			$order_date = new DateTime( $order->order_date, $timezone );

			$diff = $order_date->diff( $now );

			if ( 'pending' === $order->status && (int)$diff->format( '%h' ) > 0 ) {
				$order->update_status( 'expired-operator' );
				return;
			}

			if ( 'manager-directed' === $order->status && (int)$diff->format( '%h' ) > 1 ) {
				$order->update_status( 'expired' );
				return;
			}

			if ( true === $callback ) {
				$order_timeout    = (int)get_post_meta( $order_id, 'sormat_order_timeout', true );
				$time_spend       = $current_time - $order_timeout;
				$product_shipment = get_field( 'product_shipment', $order_id );
				$invoicing        = get_field( 'invoicing', $order_id );

				if ( $invoicing === 'cancel' || 'cancel' === $product_shipment ) {
					$order->update_status( 'expired' );
					return;
				}

				if ( empty( $product_shipment ) && 'success' === $invoicing && $time_spend >= $ten_days ) {
					$order->update_status( 'expired' );
					return;
				}

				if ( empty( $invoicing ) && $time_spend >= 60 ) {
					$order->update_status( 'expired' );
					return;
				}

			}
		}
	}
}

// add_action( 'init', 'sormat_cron_order_statuses' );

wp_schedule_event( time(), '5_minutes', 'sormat_cron_hook' );


function get_main_image_url( $post ) {
	$images         = explode( ',', get_field( 'image_order', $post ) );
	$product_images = sort_images_by_type( $images, $post->ID );
	$main_img       = get_post( $product_images['featured-image'][0] );

	return correct_img_url( wp_get_attachment_image_url( $main_img->ID, 'medium_large' ) );
}

add_action( 'admin_init', 'remove_profile_menu' );

// Removal function
function remove_profile_menu() {
	$current_user = wp_get_current_user();
	if ( in_array( 'shop_manager', $current_user->roles, true ) ) {
		remove_menu_page( 'profile.php' );
	}
}

function get_geolocation_info() {
	return get_terms(
		[
			'taxonomy'   => 'regions',
			'hide_empty' => true,
			'parent'     => 0,
		]
	);
}

function get_geolocation_terms() {
	$template = '';
	$regions  = get_terms(
		[
			'taxonomy'   => 'regions',
			'hide_empty' => true,
			'parent'     => (int)$_POST['term_id'],
		]
	);

	if ( empty( $regions ) ) {
		$cities = new WP_Query(
			[
				'post_type'      => 'geolocation',
				'posts_per_page' => -1,
				'post_status'    => 'publish',
				'tax_query'      => [
					[
						'taxonomy' => 'regions',
						'field'    => 'id',
						'terms'    => (int)$_POST['term_id'],
					],
				],
			]
		);

		ob_start();

		include get_template_directory() . '/template-parts/geolocation/post.php';

		$template = ob_get_contents();

		ob_end_clean();

	} else {
		ob_start();

		include get_template_directory() . '/template-parts/geolocation/term.php';

		$template = ob_get_contents();

		ob_end_clean();
	}

	wp_send_json( $template );
}

add_action( 'wp_ajax_get_geolocation_terms', 'get_geolocation_terms' );
add_action( 'wp_ajax_nopriv_get_geolocation_terms', 'get_geolocation_terms' );
<?php

add_action( 'init', 'suitablebasematerials_taxonomy' );
function suitablebasematerials_taxonomy(){

	register_taxonomy( 'suitablebasematerials', [ 'product', 'productgroup' ], [
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Suitable base materials',
			'singular_name'     => 'Suitable base materials',
			'search_items'      => 'Search Suitable base materials',
			'all_items'         => 'All Suitable base materials',
			'view_item '        => 'View Suitable base materials',
			'parent_item'       => 'Parent Suitable base materials',
			'parent_item_colon' => 'Parent Suitable base materials:',
			'edit_item'         => 'Edit Suitable base materials',
			'update_item'       => 'Update Suitable base materials',
			'add_new_item'      => 'Add New Suitable base materials',
			'new_item_name'     => 'New Suitable base materials Name',
			'menu_name'         => 'Suitable base materials',
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => true,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}
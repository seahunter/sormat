<?php

add_action('init', 'publishingchannels_taxonomy');
function publishingchannels_taxonomy()
{

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy('publishingchannels', [ 'product'], [
        'label' => '', // определяется параметром $labels->name
        'labels' => [
            'name' => 'Publishing channels',
            'singular_name' => 'Publishing channels',
            'search_items' => 'Search Publishing channels',
            'all_items' => 'All Publishing channels',
            'view_item ' => 'View Publishing channels',
            'parent_item' => 'Parent Publishing channels',
            'parent_item_colon' => 'Parent Publishing channels:',
            'edit_item' => 'Edit Publishing channels',
            'update_item' => 'Update Publishing channels',
            'add_new_item' => 'Add New Publishing channels',
            'new_item_name' => 'New Publishing channels Name',
            'menu_name' => 'Publishing channels',
        ],
        'description' => '', // описание таксономии
        'public' => true,
        // 'publicly_queryable'    => null, // равен аргументу public
        // 'show_in_nav_menus'     => true, // равен аргументу public
        // 'show_ui'               => true, // равен аргументу public
        // 'show_in_menu'          => true, // равен аргументу show_ui
        // 'show_tagcloud'         => true, // равен аргументу show_ui
        // 'show_in_quick_edit'    => null, // равен аргументу show_ui
        'hierarchical' => true,

        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'meta_box_cb' => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
        'show_admin_column' => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_in_rest' => null, // добавить в REST API
        'rest_base' => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ]);
}
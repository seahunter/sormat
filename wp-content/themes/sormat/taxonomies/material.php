<?php

add_action( 'init', 'material_taxonomy' );
function material_taxonomy(){

	// список параметров: wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy( 'material', [ 'product', 'productgroup' ], [
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Материал изделия / Покрытие',
			'singular_name'     => 'Материал изделия / Покрытие',
			'search_items'      => 'Search Material',
			'all_items'         => 'All Material',
			'view_item '        => 'View Material',
			'parent_item'       => 'Parent Material',
			'parent_item_colon' => 'Parent Material:',
			'edit_item'         => 'Edit Material',
			'update_item'       => 'Update Material',
			'add_new_item'      => 'Add New Material',
			'new_item_name'     => 'New Material Name',
			'menu_name'         => 'Материал изделия / Покрытие',
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => true,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}
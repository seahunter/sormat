<?php

add_action( 'init', 'approvalscertificates_taxonomy' );
function approvalscertificates_taxonomy(){

	// список параметров: wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy( 'approvalscertificates', [ 'product', 'productgroup' ], [
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Одобрения / Сертификаты',
			'singular_name'     => 'Одобрения / Сертификаты',
			'search_items'      => 'Search Certificates',
			'all_items'         => 'All Certificates',
			'view_item '        => 'View Certificates',
			'parent_item'       => 'Parent Certificates',
			'parent_item_colon' => 'Parent Certificates:',
			'edit_item'         => 'Edit Certificates',
			'update_item'       => 'Update Certificates',
			'add_new_item'      => 'Add New Certificates',
			'new_item_name'     => 'New Certificates Name',
			'menu_name'         => 'Одобрения / Сертификаты',
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => true,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}
<?php

add_action( 'init', 'approvedbasematerials_taxonomy' );
function approvedbasematerials_taxonomy(){

	// список параметров: wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy( 'approvedbasematerials', [ 'product', 'productgroup' ], [
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Материал основания',
			'singular_name'     => 'Материал основания',
			'search_items'      => 'Поиск Материала основания',
			'all_items'         => 'Материал основания',
			'view_item '        => 'Просмотр Материала основания',
			'parent_item'       => 'Родительский Материал основания',
			'parent_item_colon' => 'Родительский Материал основания:',
			'edit_item'         => 'Редактировать Материал основания',
			'update_item'       => 'Обновить Материал основания',
			'add_new_item'      => 'Добавить новый Материал основания',
			'new_item_name'     => 'Новый Материал основания',
			'menu_name'         => 'Материал основания',
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => true,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}
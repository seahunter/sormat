<?php

add_action( 'init', 'regions_taxonomy' );
function regions_taxonomy(){

	// список параметров: wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy( 'regions', [ 'geolocation' ], [
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Регион',
			'singular_name'     => 'Регион',
			'search_items'      => 'Search Регион',
			'all_items'         => 'All Регион',
			'view_item '        => 'View Регион',
			'parent_item'       => 'Parent Регион',
			'parent_item_colon' => 'Parent Регион:',
			'edit_item'         => 'Edit Регион',
			'update_item'       => 'Update Регион',
			'add_new_item'      => 'Add New Регион',
			'new_item_name'     => 'New Регион Name',
			'menu_name'         => 'Регион',
		],
		'description'           => '', // описание таксономии
		'public'                => true,
		// 'publicly_queryable'    => null, // равен аргументу public
		// 'show_in_nav_menus'     => true, // равен аргументу public
		// 'show_ui'               => true, // равен аргументу public
		// 'show_in_menu'          => true, // равен аргументу show_ui
		// 'show_tagcloud'         => true, // равен аргументу show_ui
		// 'show_in_quick_edit'    => null, // равен аргументу show_ui
		'hierarchical'          => true,

		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
		'show_admin_column'     => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		// '_builtin'              => false,
		//'update_count_callback' => '_update_post_term_count',
	] );
}
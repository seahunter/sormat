<?php

add_action('init', 'media_category_taxonomy');
function media_category_taxonomy()
{

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy('media-category', ['attachment'], [
        'label' => '', // определяется параметром $labels->name
        'labels' => [
            'name' => 'Media category',
            'singular_name' => 'Media category',
            'search_items' => 'Search Media category',
            'all_items' => 'All Media category',
            'view_item ' => 'View Media category',
            'parent_item' => 'Parent Media category',
            'parent_item_colon' => 'Parent Media category:',
            'edit_item' => 'Edit Media category',
            'update_item' => 'Update Media category',
            'add_new_item' => 'Add New Media category',
            'new_item_name' => 'New Media category Name',
            'menu_name' => 'Media category',
        ],
        'description' => '', // описание таксономии
        'public' => true,
        // 'publicly_queryable'    => null, // равен аргументу public
        // 'show_in_nav_menus'     => true, // равен аргументу public
        // 'show_ui'               => true, // равен аргументу public
        // 'show_in_menu'          => true, // равен аргументу show_ui
        // 'show_tagcloud'         => true, // равен аргументу show_ui
        // 'show_in_quick_edit'    => null, // равен аргументу show_ui
        'hierarchical' => true,

        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'meta_box_cb' => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
        'show_admin_column' => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_in_rest' => null, // добавить в REST API
        'rest_base' => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ]);
}
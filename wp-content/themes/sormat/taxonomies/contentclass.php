<?php

add_action('init', 'contentclasses_taxonomy');
function contentclasses_taxonomy()
{

    // список параметров: wp-kama.ru/function/get_taxonomy_labels
    register_taxonomy('contentclass', ['article'], [
        'label' => '', // определяется параметром $labels->name
        'labels' => [
            'name' => 'Класс контента',
            'singular_name' => 'Класс контента',
            'search_items' => 'Search Класс контента',
            'all_items' => 'All Класс контента',
            'view_item ' => 'View Класс контента',
            'parent_item' => 'Parent Класс контента',
            'parent_item_colon' => 'Parent Класс контента:',
            'edit_item' => 'Edit Класс контента',
            'update_item' => 'Update Класс контента',
            'add_new_item' => 'Add New Класс контента',
            'new_item_name' => 'New Класс контента Name',
            'menu_name' => 'Класс контента',
        ],
        'description' => '', // описание таксономии
        'public' => true,
        // 'publicly_queryable'    => null, // равен аргументу public
        // 'show_in_nav_menus'     => true, // равен аргументу public
        // 'show_ui'               => true, // равен аргументу public
        // 'show_in_menu'          => true, // равен аргументу show_ui
        // 'show_tagcloud'         => true, // равен аргументу show_ui
        // 'show_in_quick_edit'    => null, // равен аргументу show_ui
        'hierarchical' => true,

        'rewrite' => true,
        //'query_var'             => $taxonomy, // название параметра запроса
        'capabilities' => array(),
        'meta_box_cb' => null, // html метабокса. callback: `post_categories_meta_box` или `post_tags_meta_box`. false — метабокс отключен.
        'show_admin_column' => false, // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии 3.5)
        'show_in_rest' => null, // добавить в REST API
        'rest_base' => null, // $taxonomy
        // '_builtin'              => false,
        //'update_count_callback' => '_update_post_term_count',
    ]);
}
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sormat
 */

?>
    <!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>
    </head>

<body <?php body_class( 'lang-ru' ); ?>>
<?php $cities_list = get_geolocation_info() ?>

    <!-- The Modal -->
    <div id="userCity" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <div id="geolocation-content">
                <p>Выберите страну:</p>
				<?php if ( ! empty( $cities_list ) ) : ?>
                    <ul class="cities-list">
						<?php foreach ( $cities_list as $city ) : ?>
                            <li><a href="#" data-id="<?php echo $city->term_id ?>"><?php echo $city->name ?></a></li>
						<?php endforeach; ?>
                    </ul>
				<?php endif ?>
            </div>
        </div>

    </div>
    <div id="priceModalInfo" class="modal">
        <div class="modal-content">
            <span class="close">&times;</span>
            <p><strong>УВАЖАЕМЫЕ КЛИЕНТЫ. МЫ ПРИНИМАЕМ ЗАКАЗ НА СУММУ ОТ 10 000 РУБЛЕЙ.</strong></p>
            <p>Дополнительные скидки обсуждаются индивидуально после размещения заказа на сайте. Наш менеджер
                свяжется с Вами после размещения заказа.</p>
            <br>
            <p>РАДЫ СОТРУДНИЧАТЬ С ВАМИ!</p>
        </div>
    </div>
    <header id="navigation" class="no-print">
        <div id="upper-wrapper" class="" style="margin-top: 0px;">

			<?php $user_location = get_user_location(); ?>
			<?php if ( ! empty( $user_location ) ) : ?>
                <div id="cookie-text-holder" style="display: block;">
                    <p>Ваш город: <a id="changeUserCity"><strong><?php echo $user_location ?></strong></a></p>
                </div>
			<?php endif ?>
            <div id="tools-nav">
                <div class="tools-nav-box language">
                    <section class="shadow-negation">
                        <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>">Кабинет</a>
                    </section>
                </div>
                <div class="tools-nav-box share">
                    <section class="shadow-negation">
                        <div class="mini-cart-content"><?php echo woocommerce_mini_cart() ?></div>
                        <div class="clear"></div>
                    </section>
                </div>
                <div class="tools-nav-box search">

                    <section class="shadow-negation">
                        <form id="secondary-search-form" method="get" action="https://ru.sormat.com/">
                            <input type="text" id="secondary-search" class="text-input left" name="s"
                                   placeholder="Поиск...">
                            <div class="search-icon-hitarea">
                                <button type="submit" class="search-icon right"></button>
                            </div>
                        </form>
                    </section>
                    <div class="clear"></div>
                </div>

            </div>
            <div class="clear"></div>
        </div>

        <div id="lower-wrapper">
            <div id="main-nav-holder">

                <div id="logo-holder">
                    <a href="<?php echo get_home_url() ?>"><img
                                src="https://ru.sormat.com/wp-content/themes/sormat_main/images/sormat_logo@2x.png"
                                width="183" height="35" alt="Sormat" title="Sormat"></a>
                </div>

                <!--Navigation walker -->
				<?php wp_nav_menu(
					[ 'container_class' => 'menu-header',
					  'menu_id'         => 'menu-main-navigation',
					]
				) ?>
                <!--Navigation walker ends -->

                <div class="clear"></div>
                <div id="menu-button"></div>
            </div>

            <div id="secondary-nav-holder">

            </div>

        </div>
        <div class="clear"></div>
    </header>
<?php if ( ! is_front_page() ) : ?>
    <div id="upper-shadow"></div>
<?php endif ?>
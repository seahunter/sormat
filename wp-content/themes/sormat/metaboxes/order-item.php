<td class="quantity item_cost custom-cost">
    <?php $sormat_price = wc_get_order_item_meta($item_id, '_sormat_price'); ?>
    <div class="view">
        <?php echo wc_price($sormat_price) ?>
    </div>
    <div class="edit" style="display: none;">
        <?php if (null !== $_product) : ?>
            <input
                    type="number"
                    autocomplete="off" name="sormat_price[<?php echo absint($item_id); ?>]" placeholder="0"
                    value="<?php echo $sormat_price ?>"
                    size="1" class="quantity"/>
        <?php endif ?>
    </div>
</td>
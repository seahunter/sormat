<?php

function get_posts_by_post_type(string $post_type)
{
    $result = wp_cache_get($post_type . '_posts');
    if (empty($result)) {
        global $wpdb;
        $query = "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type = '%s' AND post_status ='publish'";
        $result = $wpdb->get_results($wpdb->prepare($query, $post_type));
        wp_cache_set($post_type . '_posts', $result);
    }

    return $result;
}

add_action('add_meta_boxes', 'product_metabox');
function product_metabox()
{
    $screens = ['product'];
    add_meta_box('product_select_metabox', 'Сертификаты/Одобрения', 'product_select_metabox_callback', $screens);
}

// HTML код блока
function product_select_metabox_callback($post, $meta)
{
    $screens = $meta['args'];

    // Используем nonce для верификации
    wp_nonce_field(plugin_basename(__FILE__), 'sormat_nonce');

    // значение поля
    $current_certificates = get_post_meta($post->ID, 'product_certificates', false);
    $current_approve = get_post_meta($post->ID, 'product_approvals', false);

    set_query_var('certificates', get_posts_by_post_type('certificate'));
    set_query_var('approves', get_posts_by_post_type('approval'));
    set_query_var('current_certificates', $current_certificates);
    set_query_var('current_approve', $current_approve);

    get_template_part('metaboxes/product-metabox', 'template');
}

## Сохраняем данные, когда пост сохраняется
add_action('save_post', 'update_product_metabox');
function update_product_metabox($post_id)
{
    // Убедимся что поле установлено.
    if (!isset($_POST['product_metabox']))
        return;

    // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
    if (!wp_verify_nonce($_POST['sormat_nonce'], plugin_basename(__FILE__)))
        return;

    // если это автосохранение ничего не делаем
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;

    // проверяем права юзера
    if (!current_user_can('edit_post', $post_id))
        return;

    // Все ОК. Теперь, нужно найти и сохранить данные
    // Очищаем значение поля input.
    $certificates = $_POST['product_certificates'];
    $approval = $_POST['product_approvals'];

    // Обновляем данные в базе данных.
    if (!empty($certificates)) {
        foreach ($certificates as $certificate) {
            update_post_meta($post_id, 'product_certificates', sanitize_text_field($certificate));
        }
    }
    if (!empty($approval)) {
        foreach ($approval as $approve) {
            update_post_meta($post_id, 'product_approvals', sanitize_text_field($approve));
        }
    }
}

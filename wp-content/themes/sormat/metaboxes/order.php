<?php

add_action( 'add_meta_boxes', 'sormat_order_metabox' );

function sormat_order_metabox() {
	add_meta_box( 'sormat_order_manager_box', __( 'Письмо менеджеру', 'woocommerce' ), 'sormat_order_manager_email_template', 'shop_order', 'side', 'core' );
}

function sormat_order_manager_email_template() {
	global $post;
	echo '<p style="border-bottom:solid 1px #eee;padding-bottom:13px;">
            <input type="text" style="width: 250px;" name="sormat_order_manager_email" data-order-id="' . $post->ID . '" placeholder="Введите email менеджера"></p>
            <button id="sormat_manager_email" class="button button-primary">Отправить</button>';

}

add_action( 'wp_ajax_sormat_order_send_email', 'sormat_order_send_email' );

function sormat_order_send_email() {
	if ( ! empty( $_POST['order_id'] ) ) {
		$order    = new WC_Order( (int)$_POST['order_id'] );
		$mailer   = WC()->mailer();
		$template = wc_get_template_html( 'emails/plain/admin-new-order.php', [ 'order' => $order ] );
		$result = $mailer->send( sanitize_email( $_POST['address'] ), 'Уведомление о заказа на сайте', $template );
		if( true === $result ) {
			$order->update_status('manager-directed');
			wp_send_json_success();
		} else {
			wp_send_json_error();
		}
	}
	wp_die();
}
<?php
if (empty($current_certificates)) {
    $current_certificates = [];
}

if (empty($current_approve)) {
    $current_approve = [];
}

?>
<p>
    <label>Сертификаты</label><br>
    <?php if (!empty($certificates)) : ?>
        <select class="sormat-select2 form-control" name="product-sertificates[]" multiple="multiple">
            <?php foreach ($certificates as $certificate) : ?>
                <option value="<?php echo $certificate->ID ?>" <?php echo in_array($certificate->ID, $current_certificates, true) ? 'selected' : '' ?>><?php echo $certificate->post_title ?></option>
            <?php endforeach; ?>
        </select>
    <?php endif ?>
</p>
<p>
    <label>Одобрения</label><br>
    <?php if (!empty($approves)) : ?>
        <select class="sormat-select2 form-control" name="product_approvals[]" multiple="multiple">
            <?php foreach ($approves as $approve) : ?>
                <option value="<?php echo $approve->ID ?>" <?php echo in_array($approve->ID, $current_approve, true) ? 'selected' : '' ?>><?php echo $approve->post_title ?></option>
            <?php endforeach; ?>
        </select>
    <?php endif ?>
</p>

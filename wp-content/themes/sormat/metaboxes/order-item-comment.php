<td>
    <?php $sormat_item_comment = wc_get_order_item_meta($item_id, 'sormat_item_comment') ?>
    <div class="view">
        <?php echo $sormat_item_comment ?>
    </div>
    <div class="edit" style="display: none;">
        <textarea name="sormat_item_comment[<?php echo absint($item_id); ?>]" id="" cols="30"
                  rows="10"><?php echo $sormat_item_comment ?></textarea>
    </div>
</td>
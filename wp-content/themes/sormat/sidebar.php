<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sormat
 */
?>
<?php $object = get_queried_object() ?>
<?php if (is_single() && ('productgroup' === $object->post_type || 'product' === $object->post_type)) : ?>
    <div class="row related-products">
        <h2>Related</h2>
        <!--To-do loop/search for related products-->
        <?php $relative_id = 'productgroup' === $object->post_type ? $object->ID : get_field('product_group') ?>
        <?php $product_type_id = get_field('product_type', $relative_id) ?>
        <?php
        $relative_product_args = [
            'post_type' => 'productgroup',
            'post_status' => 'publish',
            'post__not_in' => [$object->ID],
            'posts_per_page' => 6,
            'no_found_rows' => true,
            'meta_query' => [
                [
                    'key' => 'product_type',
                    'value' => $product_type_id
                ]
            ]
        ];
        $relative_product = new WP_Query($relative_product_args);
        if ($relative_product->have_posts()) : ?>
            <div class="one-columns">
                <h3>Сопутствующие товары <!--Аналогичные изделия--></h3>
                <!--To-do loop/search for similar products-->
                <div class="blue-ruler"></div>

                <div class="similar-products-wrapper">

                    <?php while ($relative_product->have_posts()) : $relative_product->the_post(); ?>
                        <div class="related-products-holder">
                            <div class="picture-wrapper">
                                <a class="product-image-link" href="<?php echo get_permalink() ?>"><img
                                            src="<?php echo get_main_image_url( get_post() ) ?>"
                                            alt=""></a>
                            </div>
                            <div class="text-wrapper">
                                <p>
                                    <?php the_title() ?> </p>
                            </div>
                        </div>
                    <?php endwhile;
                    wp_reset_postdata(); ?>

                </div>

            </div>
        <?php endif ?>

        <div class="clear"></div>
    </div>
<?php endif ?>
<?php 
/**
 * Template Name: Обучающих
 */
get_header(); ?>

  <?php while( have_posts() ) : ?>
    <section id="training-article" class="main-content-holder">

      <?php get_sidebar('breadcrumbs'); ?>
      
      <div class="row">
        <div class="one-column news-text left first">
        </div>

        <div class="two-columns news-texts right">
          <h1>Обучающих</h1>
              </div>
        <div class="clear"></div>
      </div>

      
          <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/g.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/g.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/g-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/g-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/g-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/g-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-502" class="post-502 article type-article status-publish has-post-thumbnail hentry contentclass-knowledge-base-article contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d0%b2-%d1%87%d0%b5%d0%bc-%d1%80%d0%b0%d0%b7%d0%bd%d0%b8%d1%86%d0%b0-%d0%bc%d0%b5%d0%b6%d0%b4%d1%83-%d0%b8%d1%81%d0%bf%d0%be%d0%bb%d1%8c%d0%b7%d0%be%d0%b2%d0%b0%d0%bd%d0%b8%d0%b5%d0%bc-%d1%80%d0%b0/"><h2>В чем разница между использованием распорных анкеров и бетонных винтов?</h2></a>

            <p>Оба подходят для средних и тяжелых креплений к бетону, полнотелому глиняному кирпичу и природному камню. Длинная резьба клинового (распорного) анкера (S-KA, S-KAK, S-KAH) подходит для т. н. крепления на расстоянии, при котором фиксируемый элемент отделен от основания. Когда речь идет о …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/sormat.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/sormat.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/sormat-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/sormat-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/sormat-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/sormat-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-466" class="post-466 article type-article status-publish has-post-thumbnail hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d0%bf%d0%be%d0%b4%d1%80%d0%be%d0%b1%d0%bd%d0%be-%d0%be-%d1%81%d1%82%d0%b5%d0%bd%d0%be%d0%b2%d1%8b%d1%85-%d0%bc%d0%b0%d1%82%d0%b5%d1%80%d0%b8%d0%b0%d0%bb%d0%b0%d1%85/"><h2>Подробно о стеновых материалах</h2></a>

            <p>Строительный материал монтажного основания, например стены, потолка или пола, является самым главным фактором, влияющим на выбор того или иного вида крепежа.&nbsp;Часто материал покрыт слоем краски, обоев, штукатурки или акустическим слоем. При этом несущее или подходящее для крепления основание скрыто под поверхностью. …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
              </div>

        <div class="two-columns news-texts right">
                <article id="post-500" class="post-500 article type-article status-publish hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d0%b0%d0%ba%d0%b0%d0%b4%d0%b5%d0%bc%d0%b8%d1%8f-sormat-%d0%b3%d0%be%d1%82%d0%be%d0%b2%d0%b8%d1%82-%d1%81%d0%bf%d0%b5%d1%86%d0%b8%d0%b0%d0%bb%d0%b8%d1%81%d1%82%d0%be%d0%b2-%d0%bc%d0%be%d0%bd%d1%82/"><h2>Академия Sormat готовит специалистов монтажа</h2></a>

            <p>С самого начала своей работы компания Sormat являлась лидером в области обучения специалистов по крепежу. Академия Sormat была основана в 2006 году для того, чтобы наши партнеры могли бы предоставлять своим клиентам наилучшее квалифицированное обслуживание в области крепежных решений. Уже сотни …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
              </div>

        <div class="two-columns news-texts right">
                <article id="post-498" class="post-498 article type-article status-publish hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-11-%d0%b7%d0%b0%d1%82%d1%8f%d0%b6%d0%ba%d0%b0-%d0%ba%d1%80%d0%b5%d0%bf%d0%b5%d0%b6%d0%b0-%d1%82%d1%80%d0%b5%d0%b1%d1%83%d0%b5%d1%82-%d0%be%d1%81%d0%be%d0%b1%d0%be%d0%b3/"><h2>Часть 11: Затяжка крепежа требует особого навыка</h2></a>

            <p>Крепления затягиваются в зависимости от типа, либо вручную, либо с помощью механической отвертки, гаечного ключа или шестигранного ключа или специального крепежного инструмента. Правильный момент затяжки Tinst и верный инструментарий упрощают монтаж и избавляют от ненужной работы, слишком сильной или недостаточно сильной …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
              </div>

        <div class="two-columns news-texts right">
                <article id="post-496" class="post-496 article type-article status-publish hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-10-%d0%b3%d0%bb%d1%83%d0%b1%d0%b8%d0%bd%d0%b0-%d1%83%d1%81%d1%82%d0%b0%d0%bd%d0%be%d0%b2%d0%ba%d0%b8-%d0%ba%d1%80%d0%b5%d0%bf%d0%bb%d0%b5%d0%bd%d0%b8%d0%b9/"><h2>Часть 10: Глубина установки креплений</h2></a>

            <p>Основное правило гласит: чем глубже устанавливается анкер, тем больше задействуется прочность основания на сжатие и тягу, т. е. тем прочнее будет крепление. Всегда соблюдайте указанную в руководстве по монтажу максимальную толщину tfix и глубину установки hnom фиксируемой детали. Глубине установки крепежа …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Kiertoporaus1-270x270.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Kiertoporaus1-270x270.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Kiertoporaus1-270x270-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Kiertoporaus1-270x270-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Kiertoporaus1-270x270-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Kiertoporaus1-270x270-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-492" class="post-492 article type-article status-publish has-post-thumbnail hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-9-%d0%b4%d0%bb%d1%8f-%d0%be%d0%b1%d0%b5%d1%81%d0%bf%d0%b5%d1%87%d0%b5%d0%bd%d0%b8%d1%8f-%d0%bd%d0%b0%d0%b4%d0%b5%d0%b6%d0%bd%d0%be%d1%81%d1%82%d0%b8-%d0%ba%d1%80%d0%b5/"><h2>Часть 9: Для обеспечения надежности крепления необходимо просверлить отверстие нужного размера!</h2></a>

            <p>СПОСОБ СВЕРЛЕНИЯ ОПРЕДЛЕЯЕТСЯ СТРОИТЕЛЬНЫМ МАТЕРИАЛОМ, КОТОРЫЙ ВЫСТУПАЕТ В РОЛИ МОНТАЖНОГО ОСНОВАНИЯ. Без просверливания Крепеж устанавливается путем нажатия или вкручивания его непосредственно в пористый строительный материал. Сверление под резьбу Безударное, основанное на вращательных движениях обычной отвертки сверление материалов с низкой прочностью на …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Toimintaperiaatteet1-270x270.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Toimintaperiaatteet1-270x270.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Toimintaperiaatteet1-270x270-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Toimintaperiaatteet1-270x270-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Toimintaperiaatteet1-270x270-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Toimintaperiaatteet1-270x270-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-490" class="post-490 article type-article status-publish has-post-thumbnail hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-8-%d0%bf%d1%80%d0%b8%d0%bd%d1%86%d0%b8%d0%bf%d1%8b-%d1%80%d0%b0%d0%b1%d0%be%d1%82%d1%8b-%d1%80%d0%b0%d0%b7%d0%bb%d0%b8%d1%87%d0%bd%d1%8b%d1%85-%d0%b2%d0%b8%d0%b4%d0%be/"><h2>Часть 8: Принципы работы различных видов крепежа</h2></a>

                  </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Asennustapoja1-270x270.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Asennustapoja1-270x270.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Asennustapoja1-270x270-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Asennustapoja1-270x270-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Asennustapoja1-270x270-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Asennustapoja1-270x270-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-487" class="post-487 article type-article status-publish has-post-thumbnail hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-7-%d1%81%d0%bf%d0%be%d1%81%d0%be%d0%b1%d1%8b-%d1%83%d1%81%d1%82%d0%b0%d0%bd%d0%be%d0%b2%d0%ba%d0%b8-%d0%ba%d1%80%d0%b5%d0%bf%d0%bb%d0%b5%d0%bd%d0%b8%d0%b9/"><h2>Часть 7: Способы установки креплений</h2></a>

            <p>1. Предварительный монтаж Предварительный монтаж является самым распространенным способом, например при использовании нейлоновых дюбелей и при осуществлении креплений с легкой нагрузкой. 2. Сквозной монтаж Сквозной монтаж – наиболее удобный способ крепления, когда необходимо установить несколько элементов или когда для установки крепления …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Voimat1-270x270.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Voimat1-270x270.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Voimat1-270x270-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Voimat1-270x270-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Voimat1-270x270-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Voimat1-270x270-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-484" class="post-484 article type-article status-publish has-post-thumbnail hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-6-%d1%80%d0%b0%d1%81%d1%82%d1%8f%d0%bd%d1%83%d1%82%d0%b0%d1%8f-%d0%b8%d0%bb%d0%b8-%d1%81%d0%b6%d0%b0%d1%82%d0%b0%d1%8f-%d0%b7%d0%be%d0%bd%d0%b0-%d0%b1%d0%b5%d1%82%d0%be/"><h2>Часть 6: Растянутая или сжатая зона бетона?</h2></a>

            <p>Бетон используется в строительстве как в армированном виде, так и без арматуры. Армированный бетон часто называют железобетоном, а стальные прутья, используемые для армирования – арматурой. Бетон хорошо выдерживает сжатие, но легко раскалывается при растягивании. Сталь, в свою очередь, хорошо выдерживает растяжение. …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

        <div class="row">
        <div class="one-column image-column news-text left first">
          <img width="270" height="270" src="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Etäisyydet1-270x270.png" class="attachment-listing@2x size-listing@2x wp-post-image" alt="" srcset="https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Etäisyydet1-270x270.png 270w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Etäisyydet1-270x270-135x135.png 135w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Etäisyydet1-270x270-140x140.png 140w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Etäisyydet1-270x270-75x75.png 75w, https://ru.sormat.com/wp-content/uploads/sites/5/2017/10/Etäisyydet1-270x270-150x150.png 150w" sizes="(max-width: 270px) 100vw, 270px">    </div>

        <div class="two-columns news-texts right">
                <article id="post-481" class="post-481 article type-article status-publish has-post-thumbnail hentry contentclass-training-article">

            <a href="https://ru.sormat.com/article/%d1%87%d0%b0%d1%81%d1%82%d1%8c-5-%d0%b1%d1%83%d0%b4%d1%8c%d1%82%d0%b5-%d0%b2%d0%bd%d0%b8%d0%bc%d0%b0%d1%82%d0%b5%d0%bb%d1%8c%d0%bd%d1%8b-%d0%bf%d1%80%d0%b8-%d0%bc%d0%be%d0%bd%d1%82%d0%b0%d0%b6%d0%b5/"><h2>Часть 5: Будьте внимательны при монтаже вблизи кромки основания</h2></a>

            <p>Действие большинства типов креплений основана на трении, вызываемым расширением силовой части крепежа, которое происходит вследствие закручивания крепежа. Если крепеж находится слишком близко от края основания или другого крепежа, давление от расширения может спровоцировать разрушение основания или поломку крепежа.&nbsp;Расстояние до кромки&nbsp;означает расстояние, …</p>
          </article>
        </div>
        <div class="clear"></div>
      </div>

          <div class="pagination"><span aria-current="page" class="page-numbers current">1</span>
    <a class="page-numbers" href="https://ru.sormat.com/training/page/2/">2</a>
    <a class="next page-numbers" href="https://ru.sormat.com/training/page/2/"><span class="right-pagination"></span></a></div>    <div class="clear"></div>
    </section>    
  <?php endwhile ?>

<?php get_footer();
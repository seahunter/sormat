<?php if( have_posts() ) : ?>
    <div class="search-result-container">
        <?php while( have_posts() ) : the_post() ?>
            <?php get_template_part( 'template-parts/content', 'search' ) ?>
        <?php endwhile ?>
        <div class="clear"></div>
    </div>
<?php endif ?>
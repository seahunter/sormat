<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package sormat
 */

get_header();
?>

    <section id="news" class="main-content-holder">

        <div id="breadcrumb">
            <p><a href="https://ru.sormat.com/">Home</a> <span class="separator">&gt;</span> <span class="current-item">Новости</span>
            </p></div>

        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('template-parts/term', 'item'); ?>
            <?php endwhile; ?>
        <?php endif ?>

        <?php the_posts_pagination(); ?>
        <div class="clear"></div>
    </section>

<?php
get_footer();

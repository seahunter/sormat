<?php 
/**
 * Template Name: База знаний
 */
get_header() ?>

<?php while( have_posts() ) : the_post() ?>
<section id="knowledge-base" class="main-content-holder single-page">
	<?php get_sidebar('breadcrumbs'); ?>
			<h1>Информационная база</h1>

		<div class="row">
			<article class="single-texts">
				

				<form id="knowledge-search-form" action="<?php echo get_home_url() ?>">
					<div class="icon-holder left">
						<span class="search-icon"></span>
					</div>
					<input type="text" id="knowledge-search" name="knowledge-search" placeholder="Что вы ищете?">
					<button name="search-submit" class="main search-submit" type="submit">Отправить</button>
				</form>
			</article>
		</div>

	<div class="row">
		<?php 
		$base_args = array(
			'post_type' => array('post', 'training'),
			'posts_per_page' => -1,
			'post_status' => 'publish',
			's' => isset( $_GET['s'] ) ? esc_html( $_GET['s'] ) : '',
		);
		$knowledge_base = new WP_Query( $base_args );
	?>
	<?php if( $knowledge_base->have_posts() ) : ?>
		<div class="search-result">
      <div class="search-result-container">
				<?php while( $knowledge_base->have_posts() ) : $knowledge_base->the_post(); ?>
					<?php get_template_part( 'template-parts/knowledge', 'preview' ); ?>
				<?php endwhile ?>            
        <div class="clear"></div>
      </div>
    </div>
	<?php endif ?>
  </div>
	<?php 
		$partners = new WP_Query( array(
			'post_type' => 'post',
			'posts_per_page' => 3,
			'post_status' => 'publish',
			'cat' => 6
		) )
	?>
	<?php if( $partners->have_posts() ) : ?>
	<?php
		$key = 0;
		$wrapper_class = '';
	?>
	<article id="articles-wrapper">
		<?php
			while( $partners->have_posts() ) : $partners->the_post();
				if( $key == 0 ) {
					$wrapper_class = 'first';
				}
				set_query_var('wrapper_class', $wrapper_class);
				get_template_part( 'template-parts/article', 'preview' );
				$key++;
			endwhile; wp_reset_postdata();
		?>		
		<div class="clear"></div>
	</article>
	<?php endif ?>
		
</section>
<?php endwhile ?>

<?php get_footer();
<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sormat
 */

get_header();
?>
    <section id="training-article" class="main-content-holder">

        <?php get_sidebar('breadcrumbs'); ?>
        <div class="two-columns first left training-article-texts">
            <article id="post-<?php echo get_the_id() ?>" <?php post_class() ?>>
                <?php while (have_posts()) : the_post(); ?>
                    <?php the_title('<h1>', '</h1') ?>
                    <?php the_content() ?>
                <?php endwhile ?>
            </article>
        </div>

        <div class="smaller one-column right">


            <article id="learn-more-on">
                <h3>Узнайте больше...</h3>
                <div class="blue-ruler"></div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/keva"><img style="max-width:69px;max-height:69px;"
                                                                           src="https://ru.sormat.com/wp-content/uploads/KEVA-125x21.png"
                                                                           alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/keva"><h3>KEVA </h3></a>
                        Заточенные стальные шпильки высокого качества для использования с химическими…
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/ith-300-ve"><img style="max-width:69px;max-height:69px;"
                                                                                 src="https://ru.sormat.com/wp-content/uploads/ITH-300-Ve-125x45.png"
                                                                                 alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/ith-300-ve"><h3>ITH 300 Ve </h3></a>
                        Высокоэффективная инжекционная масса на основе винилэстера для применения в…
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/ith-410-wi"><img style="max-width:69px;max-height:69px;"
                                                                                 src="https://ru.sormat.com/wp-content/uploads/ITH-410-Wi-1-125x46.png"
                                                                                 alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/ith-410-wi"><h3>ITH 410 Wi </h3></a>
                        Инжекционная масса на основе винилэстера для использования в холодных…
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/ipu-150300"><img style="max-width:69px;max-height:69px;"
                                                                                 src="https://ru.sormat.com/wp-content/uploads/IPU-150-300_web1-125x81.png"
                                                                                 alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/ipu-150300"><h3>IPU 150/300 </h3></a>
                        Сэкономьте время, деньги и силы с оригинальными пистолетами Sormat…
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/ipum"><img style="max-width:69px;max-height:69px;"
                                                                           src="https://ru.sormat.com/wp-content/uploads/IPUM-280_web-125x37.png"
                                                                           alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/ipum"><h3>IPUM </h3></a>
                        Эффективный насос для очистки отверстий от буровой муки
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/metal-brush"><img
                                    style="max-width:69px;max-height:69px;"
                                    src="https://ru.sormat.com/wp-content/uploads/METAL-BRUSH-125x17.png" alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/metal-brush"><h3>MB </h3></a>
                        Удобный базовый инструмент для очистки отверстий от буровой муки
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/ith-585-epoxe"><img
                                    style="max-width:69px;max-height:69px;"
                                    src="https://ru.sormat.com/wp-content/uploads/ITH-585-EPOXe-A-125x41.png"
                                    alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/ith-585-epoxe"><h3>ITH 585 EPOXe </h3></a>
                        ETA Option 1 – одобренная чистая эпоксидная сила r
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="learn-more-holder">

                    <div class="learn-more-picture left">
                        <a href="https://ru.sormat.com/products/ith-300-pe"><img style="max-width:69px;max-height:69px;"
                                                                                 src="https://ru.sormat.com/wp-content/uploads/ITH-300-Pe-125x40.png"
                                                                                 alt=""></a>
                    </div>
                    <div class="learn-more-text left">
                        <a href="https://ru.sormat.com/products/ith-300-pe"><h3>ITH 300 Pe </h3></a>
                        Экономичная инжекционная масса общего назначения на основе полиэстера
                    </div>
                    <div class="clear"></div>

                </div>


                <div class="clear"></div>
            </article>


            <article id="table-of-contents" style="display: none;">
                <h3>Содержание</h3>
                <div class="blue-ruler"></div>
                <ul class="post-list">

                </ul>

                <div class="clear"></div>
            </article>
        </div>

        <div class="clear"></div>
    </section>
<?php
get_footer();

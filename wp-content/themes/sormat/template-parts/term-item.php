<div class="row">
    <div class="one-column image-column news-text left first">
        <?php the_post_thumbnail('medium_large'); ?>
    </div>
    <div class="two-columns news-texts right">
        <article id="post-<?php the_ID(); ?>" c<?php post_class(); ?>>

            <h6><?php the_time('F d, Y'); ?></h6>
            <a href="<?php the_permalink(); ?>">
                <h2><?php the_title() ?></h2></a>

            <?php the_excerpt(); ?>
        </article>
    </div>
    <div class="clear"></div>
</div>
<div class="search-result-holder">
  <a href="<?php the_permalink() ?>" class="greybox" style="display:block;"><?php the_post_thumbnail() ?></a>
  <p><a href="<?php the_permalink() ?>"><?php the_title() ?></a></p>
</div>
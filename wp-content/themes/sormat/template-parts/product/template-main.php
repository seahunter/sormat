<div class="row general-product-info">
    <?php get_template_part('template-parts/product/content'); ?>
    <?php if (isset($product_id)) : ?>
        <?php get_template_part('template-parts/product/product', 'description'); ?>
    <?php else : ?>
        <?php get_template_part('template-parts/product/tech', 'chars'); ?>
    <?php endif; ?>
    <div class="clear"></div>
</div>
<div class="row additional-information">
    <h2>Дополнительная информация </h2>
    <div class="one-column first" style="display: none;">

        <h3>Новости</h3>
        <div class="blue-ruler"></div>

        <div class="additional-information-holder first">


        </div>

    </div>

    <div class="one-column" style="display: none;">
        <h3>Статьи</h3>
        <div class="blue-ruler"></div>
        <div class="additional-information-holder first">


        </div>

    </div>

    <?php
    $links_args = [];
    if (!empty($product_type)) {
        $links_args = get_attached_media('application/pdf', $product_type);
    }
    ?>
    <?php if (!empty($links_args)) : ?>
        <div class="one-column downloads first">
            <h3>Скачивание</h3>
            <div class="blue-ruler"></div>
            <div class="additional-information-holder first">

                <?php foreach ($links_args as $link) : ?>
                    <div class="additional-information-text">
                        <span class="read-more-icon"></span>
                        <p><a href="<?php echo $link->guid ?>"
                              target="_blank"><?php echo $link->post_title ?></a><a
                                    href="<?php echo $link->guid ?>"
                                    target="_blank"><span class="pdf"><img
                                            src="https://ru.sormat.com/wp-content/themes/sormat_main/images/pdf@2x.png"
                                            width="17" height="10" alt=""></span></a></p>
                    </div>
                <?php endforeach; ?>

            </div>

        </div>

        <div class="clear"></div>
    <?php endif ?>
</div>
<?php get_template_part('template-parts/product/application'); ?>
<div class="row application">
    <div class="one-column first applications">
        <h3>Применение</h3>
        <div class="blue-ruler"></div>
        <?php the_field('applications_ru'); ?>
    </div>
    <?php $approved = wp_get_post_terms(get_the_ID(), 'approvedbasematerials', ['fields' => 'ids']) ?>
    <?php $suitablebasematerials = wp_get_post_terms(get_the_ID(), 'suitablebasematerials', ['fields' => 'ids']) ?>
    <div class="one-column base-materials">
        <?php if (!empty($approved) || !empty($suitablebasematerials)) : ?>
            <h3>Материал основания</h3>
            <div class="blue-ruler"></div>
        <?php endif ?>
        <?php if (!empty($approved)) : ?>
            <h6>Одобрено для</h6>
            <ul>
                <?php foreach ($approved as $item) : ?>
                    <li><?php echo get_translated_term_name($item) ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif ?>
        <?php if (!empty($suitablebasematerials)) : ?>
            <h6>Также подходит для</h6>
            <ul>
                <?php foreach ($suitablebasematerials as $item) : ?>
                    <li><?php echo get_translated_term_name($item) ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
    <?php
    $data = [];
    if (isset($product_id)) {
        $current_approve = get_post_meta($product_id, 'product_approvals', false) ?: [];
        $current_certificates = get_post_meta($product_id, 'product_certificates', false) ?: [];
        $data = array_merge($current_approve, $current_certificates);
    }
    ?>
    <?php if (!empty($data)) : ?>
        <div class="one-column approval">

            <h3>Одобрения / Сертификаты</h3>

            <div class="blue-ruler"></div>

            <?php foreach ($data as $key => $item) : ?>
                <div class="approval-holder <?php echo 0 === $key || $key % 2 === 0 ? 'first' : '' ?>">
                    <?php
                    $item_post = get_post($item);
                    $images = get_attached_media('image/png', $item_post->post_parent);
                    ?>
                    <?php if (!empty($images)) : ?>
                        <?php foreach ($images as $attach) : ?>
                            <a href="#" class="approval-link" data-identifier="<?php echo $attach->ID ?>"
                               data-lang="ru">

                                <div class="greybox first">
                                    <img src="<?php echo correct_img_url(wp_get_attachment_image_url($attach->ID, 'medium')) ?>"
                                         alt="">
                                </div>

                                <p><?php echo $attach->post_title ?></p>
                            </a>
                        <?php endforeach; ?>
                    <?php endif ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif ?>

    <div class="clear"></div>
</div>
<?php $materials = wp_get_post_terms(get_the_id(), ['material'], ['fields' => 'ids']) ?>
<?php $conditions = wp_get_post_terms(get_the_id(), ['conditions'], ['fields' => 'ids']) ?>
<?php if (!empty($materials) || !empty($conditions)) : ?>
    <div class="two-columns technical-features right">
        <h3 class="left">Технические характеристики</h3>
        <div class="clear"></div>
        <div class="blue-ruler"></div>
        <div class="table">
            <?php if (!empty($materials)) : ?>
                <?php $material_str = '' ?>
                <div class="table-row">
                    <div class="data-left">Материал</div>
                    <div class="data-right">
                        <?php foreach ($materials as $material) : ?>
                            <?php $material_str .= get_translated_term_name($material) ?>
                        <?php endforeach; ?>
                        <?php echo $material_str ?>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php endif; ?>
            <?php if (!empty($conditions)) : ?>
                <?php $conditions_str = '' ?>
                <div class="table-row">
                    <div class="data-left">Подходящие условия</div>
                    <?php foreach ($conditions as $condition) : ?>
                        <?php $conditions_str .= get_translated_term_name($condition) ?>
                    <?php endforeach; ?>
                    <div class="data-right"> <?php echo $conditions_str ?></div>
                    <div class="clear"></div>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php endif ?>
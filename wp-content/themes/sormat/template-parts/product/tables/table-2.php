<?php if (!empty($fixing_details_by_stud_solid_installation_data)) : ?>
    <div class="row performance-data">
        <h2>Детали монтажа для полнотелых материалов</h2>
        <table id="performance-data-table">
            <thead>
            <tr>
                <th>Размер шпильки<span class="sort-icon"></span></th>
                <th>Размер гайки под ключ<span class="sort-icon"></span></th>
                <th>Отверстие в прикрепляемом материале (d<sub>f</sub>)<span class="sort-icon"></span></th>
                <th>Диаметр отверстия (d<sub>0</sub>)<span class="sort-icon"></span></th>
                <th>мин.глубина отверстия (h<sub>1</sub>)<span class="sort-icon"></span></th>
                <th>Глубина анкеровки (h<sub>nom</sub>)<span class="sort-icon"></span></th>
                <th>Теоретический расход смолы (vol)<span class="sort-icon"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($fixing_details_by_stud_solid_installation_data as $key => $item) : ?>
                <?php wp_parse_str($item, $colums); ?>
                <tr>
                    <td><?php echo $colums['size'] ?></td>
                    <td><?php echo $colums['sw'] ?>&nbsp;mm</td>
                    <td><?php echo $colums['df'] ?>&nbsp;mm</td>
                    <td><?php echo $colums['drill_hole_diameter'] ?>&nbsp;mm</td>
                    <td><?php echo $colums['min_hole_depth'] ?>&nbsp;mm</td>
                    <td><?php echo $colums['nominal_setting_depth'] ?>&nbsp;mm</td>
                    <td><?php echo $colums['theoretical_resin_consumption'] ?>&nbsp;ml</td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif ?>
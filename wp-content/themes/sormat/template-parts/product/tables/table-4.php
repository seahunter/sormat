<?php if (!empty($fixing_details_by_stud_solid_loads)) : ?>
    <div class="row performance-data performance_solid_base_material_loads">
        <h2>Рабочие характеристики для полнотелых материалов</h2>
        <table id="performance-data-table">
            <thead>
            <tr>
                <th>Размер шпильки<span class="sort-icon"></span></th>

                <th>Класс прочности<span class="sort-icon"></span></th>

                <th>Материал основания<span class="sort-icon"></span></th>

                <th>Глубина анкеровки (h<sub>nom</sub>)<span class="sort-icon"></span></th>

                <th>мин. толщина материала основания (h<sub>min</sub>)<span class="sort-icon"></span></th>

                <th>Момент затяжки (T<sub>inst</sub>)<span class="sort-icon"></span></th>

                <th>Тип нагрузки<span class="sort-icon"></span></th>

                <th class="no-sort">Направление нагрузки</th>

                <th>Значение нагрузки<span class="sort-icon"></span></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($fixing_details_by_stud_solid_loads as $key => $item) : ?>
                <?php wp_parse_str($item, $colums); ?>
                <?php $type = explode(' ', $colums['type']) ?>
                <tr data-performance-base="non-cracked-concrete-c20-25">
                    <td><?php echo $colums['size'] ?></td>

                    <td><?php echo $colums['steel_class'] ?></td>

                    <td><?php echo $colums['base'] ?></td>

                    <td><?php echo $colums['nominal_setting_depth'] ?>&nbsp;mm</td>

                    <td><?php echo $colums['min_base_material_thickness'] ? $colums['min_base_material_thickness'] . '&nbsp;mm' : '' ?></td>

                    <td><?php echo $colums['installation_torque'] ? $colums['installation_torque'] . '&nbsp;Nm' : '' ?></td>

                    <?php if (isset($type[1]) && 'NRec' === $type[1]) : ?>
                        <td>N<sub>Rec</sub></td>
                        <td>
                            <img src="https://ru.sormat.com/wp-content/themes/sormat_main/images/load-tension.png"
                                 width="39"
                                 height="20" alt="tension" title="tension">

                        </td>
                    <?php else : ?>
                        <td>V<sub>Rec</sub></td>
                        <td>
                            <img src="https://ru.sormat.com/wp-content/themes/sormat_main/images/load-shear.png"
                                 width="39"
                                 height="20" alt="shear" title="shear">

                        </td>
                    <?php endif ?>


                    <td><?php echo $colums['value'] ?>&nbsp;kN</td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
<?php endif ?>
<?php if (!empty($permissible_load)) : ?>
    <?php
    $bases = [];
    foreach ($permissible_load as $value) {
        wp_parse_str($value, $result);
        if (!in_array($result['base'], $bases)) {
            $bases[] = $result['base'];
        }
    }
    ?>
    <div class="row performance-data">
        <h2>Рабочие характеристики</h2>

        <table id="performance-data-table">
            <thead>
            <tr>
                <th>Материал основания<span class="sort-icon"></span>
                    <div class="performance-data-selector-wrapper">
                        <select name="product-performance-permissible-loads" class="performance-data-selector" id="performance-data-selector">
                            <option value="">все</option>
                            <?php foreach ($bases as $base) : ?>
                                <option value="<?php echo str_replace(' ', '-', mb_strtolower($base)) ?>"><?php echo $base ?></option>
                            <?php endforeach; ?>
                        </select>

                </th>

                <th>Тип нагрузки<span class="sort-icon"></span></th>
                <th>Глубина анкеровки (h<sub>nom</sub>)<span class="sort-icon"></span></th>
                <th class="no-sort">Направление нагрузки</th>
                <th>Значение нагрузки<span class="sort-icon"></span></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($permissible_load as $key => $item) : ?>
                <?php wp_parse_str($item, $colums); ?>
                <?php $type = explode(' ', $colums['type']) ?>
                <tr data-performance-base="<?php echo str_replace(' ', '-', mb_strtolower($colums['base'])) ?>">
                    <td><?php echo $colums['base'] ?></td>

                    <?php if (isset($type[1]) && 'NRec' === $type[1]) : ?>
                        <td>N<sub>Rec</sub></td>
                    <?php else : ?>
                        <td>V<sub>Rec</sub></td>
                    <?php endif ?>
                    <td><?php echo $colums['nominal_setting_depth'] ?>&nbsp;mm</td>

                    <td>
                        <?php if (isset($type[1]) && 'NRec' === $type[1]) : ?>
                            <img src="https://ru.sormat.com/wp-content/themes/sormat_main/images/load-tension.png"
                                 width="39"
                                 height="20" alt="Растягивающая сила" title="Растягивающая сила">
                        <?php else : ?>
                            <img src="https://ru.sormat.com/wp-content/themes/sormat_main/images/load-shear.png"
                                 width="39"
                                 height="20" alt="срез" title="срез">
                        <?php endif ?>
                    </td>


                    <td><?php echo $colums['value'] ?>&nbsp;kN</td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif ?>
<?php if (!empty($installation_images)) : ?>
    <div class="row installation">
        <h2>Монтаж</h2>
        <div class="blue-ruler"></div>
        <?php foreach ($installation_images as $image) : ?>
            <?php $url = correct_img_url(wp_get_attachment_image_url($image, 'medium-large')) ?>
            <img class="show-on-pdf-view" width="135"
                 src="<?php echo $url ?>"/>
            <div class="installation-holder">
                <div class="picture-wrapper">
                    <a class="product-image-link"
                       href="<?php echo $url ?>"
                       data-lightbox="installation-frame" data-title=""><img width="135"
                                                                             src="<?php echo $url ?>"
                                                                             alt=""></a>
                </div>
                <div class="text-wrapper">
                    <p>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>

        <div class="clear"></div>
    </div>
<?php endif ?>
<?php if (!empty($chemical_fixings_set)) : ?>
    <div class="row performance-data">
        <h2>Время схватывания и полного отверждения</h2>
        <table id="performance-data-table">
            <thead>
            <tr>
                <th>Температура материала основания<span class="sort-icon"></span></th>
                <th class="align-right" style="width:10%">Время схватывания<span class="sort-icon"></span></th>
                <th class="align-right">Время твердения<span class="sort-icon"></span></th>
                <th style="width:55%;"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($chemical_fixings_set as $key => $item) : ?>
                <?php wp_parse_str( $item, $colums ); ?>
                <tr>
                    <td><?php echo $colums['base_material_temperature'] ?> °C</td>
                    <td class="align-right">
                        <?php echo $colums['gel_time'] ?>&nbsp;min
                    </td>
                    <td class="align-right">
                        <?php echo $colums['curing_time'] ?>&nbsp;min
                    </td>
                    <td>&nbsp;</td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif ?>
<?php if (!empty($fixing_details_by_stud_hollow_loads)) : ?>
    <div class="row performance-data performance_hollow_base_material_loads">
        <h2>Рабочие характеристики для пустотелых материалов</h2>

        <table id="performance-data-table">
            <thead>
            <tr>
                <th>Размер шпильки<span class="sort-icon"></span></th>
                <th>Класс прочности<span class="sort-icon"></span></th>
                <th>Материал основания<span class="sort-icon"></span></th>
                <th>Глубина анкеровки (h<sub>nom</sub>)<span class="sort-icon"></span></th>
                <th class="hidden">мин. толщина материала основания (h<sub>min</sub>)<span class="sort-icon"></span>
                </th>
                <th class="hidden">Момент затяжки (T<sub>inst</sub>)<span class="sort-icon"></span></th>
                <th>Тип нагрузки<span class="sort-icon"></span></th>
                <th class="no-sort">Направление нагрузки</th>
                <th>Значение нагрузки<span class="sort-icon"></span></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($fixing_details_by_stud_hollow_loads as $key => $item) : ?>
                <?php wp_parse_str($item, $colums); ?>
                <?php $type = explode(' ', $colums['type']) ?>
                <tr data-performance-base="hollow-light-expanded-clay-aggregate-fb-%e2%89%a5-27-mn-m2">
                    <td><?php echo $colums['size'] ?></td>
                    <td><?php echo $colums['steel_class'] ?></td>
                    <td><?php echo $colums['base'] ?></td>
                    <td><?php echo $colums['nominal_setting_depth'] ?>&nbsp;mm</td>
                    <td class="hidden"></td>
                    <td class="hidden"></td>
                    <?php if (isset($type[1]) && 'NRec' === $type[1]) : ?>
                        <td>N<sub>Rec</sub></td>
                        <td>
                            <img src="https://ru.sormat.com/wp-content/themes/sormat_main/images/load-tension.png"
                                 width="39"
                                 height="20" alt="tension" title="tension">

                        </td>
                    <?php else : ?>
                        <td>V<sub>Rec</sub></td>
                        <td>
                            <img src="https://ru.sormat.com/wp-content/themes/sormat_main/images/load-shear.png"
                                 width="39"
                                 height="20" alt="shear" title="shear">

                        </td>
                    <?php endif ?>

                    <td><?php echo $colums['value'] ?>&nbsp;kN</td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif ?>
<?php $materials = wp_get_post_terms(get_the_id(), ['material'], ['fields' => 'names']) ?>
<div class="two-columns product-overview right">
    <h3 class="left">Описание продукции</h3>
    <div class="clear"></div>
    <div class="blue-ruler"></div>
    <div class="table">
        <?php if (!empty($snro)) : ?>
            <div class="table-row">
                <div class="data-left">Другие артикулы</div>
                <div class="data-right">SNRO <?php echo $snro ?> / <?php echo $lvi ?? '' ?></div>
                <div class="clear"></div>
            </div>
        <?php endif ?>
        <?php if (!empty($materials)) : ?>
            <div class="table-row">
                <div class="data-left">Материал</div>
                <div class="data-right"> <?php echo implode(', ', $materials) ?></div>
                <div class="clear"></div>
            </div>
        <?php endif; ?>

        <?php if (!empty($product_id)) : ?>
            <?php $package = get_package_info($product_id) ?>
            <?php if (!empty($package)) : ?>
                <div class="table-row">
                    <div class="data-left">Упаковка</div>

                    <div class="data-right">
                        <?php echo $package ?>
                    </div>
                    <div class="clear"></div>
                </div>
            <?php endif ?>
        <?php endif ?>
        <?php if (!empty($weight)) : ?>
            <div class="table-row">
                <div class="data-left">Вес</div>
                <div class="data-right"><?php echo str_replace(',', '.', $weight) * 1000 ?> kg / 1000</div>
                <div class="clear"></div>
            </div>
        <?php endif ?>
    </div>
</div>
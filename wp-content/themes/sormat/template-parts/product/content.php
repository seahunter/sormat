<?php
$image_path = 'https://www.sormat.com/wp-content/uploads/';
$images     = explode( ',', get_field( 'image_order' ) );
global $product_images;
global $parent_group;
$product_images = sort_images_by_type( $images, get_the_id() );
$carousel       = [];
if ( ! empty( $product_images['product-image'] ) ) {
	$carousel = array_merge( $carousel, $product_images );
}
if ( $parent_group === true && ! empty( $product_images['measurement-drawing'] ) ) {
	$carousel = array_merge( $carousel, $product_images['measurement-drawing'] );
}
global $woocommerce;
global $product;
if ( ! is_object( $product ) ) $product = wc_get_product( $product_id );
?>
<div class="one-column first product-images">

	<?php if ( ! empty( $product_images['featured-image'] ) ) : ?>
		<?php $main_img = get_post( $product_images['featured-image'][0] ) ?>
        <div class="product-image">
            <a class="product-image-link"
               href="<?php echo correct_img_url( wp_get_attachment_image_url( $main_img->ID, 'medium_large' ) ) ?>"
               data-lightbox="product-images"
               data-title="<?php echo $main_img->post_title ?>">
				<?php $img_src = wp_get_attachment_image_src( $main_img->ID, 'medium' ) ?>
                <img
                        src="<?php echo correct_img_url( $img_src[0] ) ?>"
                        alt=""
                        width="<?php echo $img_src[1] ?>" height="<?php echo $img_src[2] ?>"></a>
        </div>
        <div class="clear"></div>
		<?php if ( ! empty( $carousel ) ) : ?>
            <div class="thumbnail-wrapper">

				<?php foreach ( $carousel as $key => $image ) : ?>
					<?php
					$attachment_sizes = get_post_meta( $image, '_wp_attachment_metadata', true );
					?>
					<?php if ( ! empty( $attachment_sizes ) ) : ?>
						<?php
						$large     = $attachment_sizes['sizes']['large']['file'];
						$thumbnail = $attachment_sizes['sizes']['thumbnail']['file'];
						if ( ! empty( $large ) ) : ?>
                            <div class="thumbnail-holder <?php echo 0 === $key ? 'first' : '' ?>">
                                <a class="product-image-link"
                                   href="<?php echo $image_path . $large ?>"
                                   title=""
                                   data-title="Скачайте в высоком разрешении"><img
                                            src="<?php echo $image_path . $thumbnail ?>"
                                            alt="">
                                </a>
                            </div>
						<?php endif; ?>
					<?php endif ?>
				<?php endforeach; ?>

                <div class="clear"></div>
            </div>
		<?php endif; ?>
	<?php endif ?>
	<?php if ( is_object( $product ) ) : ?>
        <div class="price-row">
            <p>
				<?php echo wc_price( $product->get_price() ) ?>
            </p>
        </div>
        <div class="clear"></div>
        <div class="sormat-add-to-card">
			<?php
				wc_get_template( 'single-product/add-to-cart/simple.php' );
			?>
        </div>
	<?php endif ?>
</div>
<div class="two-columns technical-features-description right">
    <div class="sales-description">
        <p><?php echo get_field( 'sales_description_ru' ) ?></p>
		<?php the_field( 'product_description_long_ru' ) ?>
    </div>
</div>

<?php $relative_product = get_relative_products(get_the_ID()) ?>

<?php if (!empty($relative_product)) : ?>
    <?php $tabel = build_product_list_table($relative_product) ?>
    <?php $columns = !empty($tabel['columns']) ? $tabel['columns'] : [] ?>
    <div class="row products-table-holder">
        <h2><?php echo get_the_title() ?> Вид товара</h2>
        <table id="products-table" class="tablesorter tablesorter-default" role="grid">
            <thead>
            <tr class="tablesorter-headerRow" role="row">
                <th class="no-sort tablesorter-header tablesorter-headerUnSorted" data-column="0" tabindex="0"
                    scope="col" role="columnheader" aria-disabled="false" aria-controls="products-table"
                    unselectable="on" aria-sort="none"
                    aria-label=": No sort applied, activate to apply an ascending sort" style="user-select: none;">
                    <div class="tablesorter-header-inner"></div>
                </th>

                <?php if (!empty($columns)) : ?>
                    <?php $i = 1; ?>
                    <?php foreach ($columns as $key => $column_name) : ?>
                        <th data-column="<?php echo $i?>" class="tablesorter-header tablesorter-headerUnSorted"
                            tabindex="0"
                            scope="col"
                            role="columnheader" aria-disabled="false" aria-controls="products-table" unselectable="on"
                            aria-sort="none"
                            aria-label="<?php echo $column_name ?>: No sort applied, activate to apply an ascending sort"
                            style="user-select: none;">
                            <div class="tablesorter-header-inner"><?php echo $column_name ?><span
                                        class="sort-icon"></span></div>
                        </th>
                        <?php $i+= 1; ?>
                    <?php endforeach; ?>
                <?php endif ?>
                <th class="no-sort tablesorter-header tablesorter-headerUnSorted" data-column="7" tabindex="0"
                    scope="col" role="columnheader" aria-disabled="false" aria-controls="products-table"
                    unselectable="on" aria-sort="none"
                    aria-label=": No sort applied, activate to apply an ascending sort" style="user-select: none;">
                    <div class="tablesorter-header-inner"></div>
                </th>
            </tr>
            </thead>
            <?php $products_list = !empty ($tabel['posts']) ? $tabel['posts'] : [] ?>
            <?php $columns_slugs = !empty($columns) ? array_keys($columns) : [] ?>
            <?php if (!empty($products_list)) : ?>
                <tbody aria-live="polite" aria-relevant="all">
                <?php foreach ($products_list as $product_id => $product_item) : ?>
                    <tr data-href="<?php echo get_the_permalink($product_id); ?>">
                        <td>
                            <a class="product-image-link" href="<?php echo get_the_permalink($product_id); ?>">
                                <img width="46" src="https://ru.sormat.com/wp-content/uploads/S-KA-125x26.png"
                                     alt=""></a>
                        </td>
                        <?php foreach ($product_item as $key => $field) : ?>
                            <?php if (in_array($key, $columns_slugs)) : ?>
                                <td><?php echo $field ?><?php echo in_array($key, ['total_length', 'nominal_size', 'drill_hole_diameter', 'fixture_thickness']) ? ' mm' : '' ?></td>
                            <?php endif ?>
                        <?php endforeach; ?>
                        <td><a href="<?php echo get_the_permalink($product_id); ?>">
                                <div class="read-more-icon"></div>
                            </a></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            <?php endif ?>
        </table>
    </div>
<?php endif ?>
<?php
global $product_images;
$meta_data = get_post_meta(get_the_id());
$columns = build_setup_columns($meta_data);
?>
<?php if (!empty($columns)) : ?>
    <div class="row technical-data">
        <h2>Технические данные</h2>
        <?php for ($i = 0; $i < 2; $i++) : ?>
            <div class="one-column <?php echo 0 === $i ? 'first' : '' ?>">
                <?php $rows = $columns[$i] ?>
                <?php if (!empty($rows)) : ?>
                    <h3>параметры установки</h3>
                    <div class="blue-ruler"></div>
                    <div class="table">
                        <?php foreach ($rows as $key => $value) : ?>
                            <?php if (!empty($value)) : ?>
                                <div class="table-row">
                                    <div class="data-left"><?php echo $key ?></div>
                                    <div class="data-right"><?php echo $value ?></div>
                                    <div class="clear"></div>
                                </div>
                            <?php endif ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>
                <?php if (0 === $i && !empty($product_images['measurement-drawing'])) : ?>
                    <?php foreach ($product_images['measurement-drawing'] as $product_image) : ?>
                        <div class="technical-picture">
                            <?php $img_medium = correct_img_url(wp_get_attachment_image_url($product_image, 'medium')) ?>
                            <?php $img_medium_large = correct_img_url(wp_get_attachment_image_url($product_image, 'medium-large')) ?>
                            <img class="show-on-pdf-view"
                                 src="<?php echo $img_medium ?>"
                                 alt="">
                            <a class="product-image-link"
                               href="<?php echo $img_medium_large ?>"
                            ><img
                                        src="<?php echo $img_medium ?>"
                                        alt=""></a>
                        </div>
                    <?php endforeach; ?>
                <?php endif ?>
            </div>
        <?php endfor; ?>
        <?php if (!empty($columns[2])) : ?>
            <div class="one-column">
                <h3>Данные о головке</h3>
                <div class="blue-ruler"></div>
                <?php $head_rows = $columns[2] ?>
                <div class="table">
                    <?php foreach ($head_rows as $key => $value) : ?>
                        <?php if (!empty($value)) : ?>
                            <div class="table-row">
                                <div class="data-left"><?php echo $key ?></div>
                                <div class="data-right"><?php echo $value ?></div>
                                <div class="clear"></div>
                            </div>
                        <?php endif ?>
                    <?php endforeach; ?>

                </div>

            </div>
        <?php endif ?>
        <div class="clear"></div>
        <?php
        if (!empty($meta_data['permissible_load'])) {
            set_query_var('permissible_load', $meta_data['permissible_load']);
            get_template_part('template-parts/product/tables/work-chars');
        }

        if (!empty($product_images['installation-frame'])) {
            set_query_var('installation_images', $product_images['installation-frame']);
            get_template_part('template-parts/product/tables/installation');
        }
        ?>
    </div>
<?php endif ?>
<?php
if (!empty($meta_data['chemical_fixings_set'])) {
    set_query_var('chemical_fixings_set', $meta_data['chemical_fixings_set']);
    get_template_part('template-parts/product/tables/table', '1');
}

if (!empty($meta_data['fixing_details_by_stud_solid_installation_data'])) {
    set_query_var('fixing_details_by_stud_solid_installation_data', $meta_data['fixing_details_by_stud_solid_installation_data']);
    get_template_part('template-parts/product/tables/table', '2');
}

if (!empty($meta_data['fixing_details_by_stud_hollow_installation_data'])) {
    set_query_var('fixing_details_by_stud_hollow_installation_data', $meta_data['fixing_details_by_stud_hollow_installation_data']);
    get_template_part('template-parts/product/tables/table', '3');
}

if (!empty($meta_data['fixing_details_by_stud_solid_loads'])) {
    set_query_var('fixing_details_by_stud_solid_loads', $meta_data['fixing_details_by_stud_solid_loads']);
    get_template_part('template-parts/product/tables/table', '4');
}

if (!empty($meta_data['fixing_details_by_stud_hollow_loads'])) {
    set_query_var('fixing_details_by_stud_hollow_loads', $meta_data['fixing_details_by_stud_hollow_loads']);
    get_template_part('template-parts/product/tables/table', '5');
}

?>

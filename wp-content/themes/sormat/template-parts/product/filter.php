<?php
$images_slug = [
    'approvedbasematerials' => 'basematerial',
    'conditions' => 'condition',
    'approvalscertificates' => 'approval',
];
?>
<section id="product-selector-holder" class="open" data-notice="См. результаты ниже">

    <a href="#" class="scrolldown-notice-popup">
        <span class="arrow"><span class="chevron"></span></span>
        <div class="scrolldown-notice-searching">
            <span class="scrolldown-text-searching">Searching...</span> &nbsp;
            <span class="scrolldown-indicator"></span>
        </div>
        <span class="scrolldown-text-searched">См. результаты ниже</span>
    </a>

    <header id="product-selector-header-text">
        <h3 class="left">Найдите нужную продукцию</h3>
        <a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>" class="see-all right">Вся продукция &gt;</a>
        <div class="clear"></div>
    </header>
    <?php $taxonomies = ['approvedbasematerials', 'conditions', 'approvalscertificates', 'material'] ?>
    <div id="product-selector-header">
        <div id="selector-productcategories" class="product-selector-box first-selector active">Товарная категория</div>
        <?php if (!empty($taxonomies)) : ?>
            <?php $i = 0; ?>
            <?php foreach ($taxonomies as $taxonomy): ?>
                <?php $tax_obj = get_taxonomy($taxonomy); ?>
                <div id="selector-<?php echo str_replace('-', '', $taxonomy) ?>"
                     class="product-selector-box"><?php echo $tax_obj->label ?></div>
                <?php if (1 == $i) break; ?>
                <?php $i++ ?>
            <?php endforeach ?>
        <?php endif ?>
        <div class="clear"></div>
    </div>

    <div id="product-selector-content">
        <?php
        //            $product_cats = get_field('categories_list', 'option');
        $product_cats = get_terms(
            [
                'taxonomy' => 'product_cat',
                'hide_empty' => true,
                'orderby' => 'term_order',
            ]
        );
        ?>
        <?php if (!empty($product_cats)) : ?>
            <div id="content-productcategories" class="product-selector-content selected">
                <?php foreach ($product_cats as $product_cat) : ?>
                    <?php
                        $product_cat_option = get_option( 'taxonomy_' . $product_cat->term_id );
                    ?>
                    <div class="product-selector-content-box product-category-selector type-product_cat value-<?php echo $product_cat->slug ?>"
                         data-type="product_cat" data-value="<?php echo $product_cat->slug ?>"
                         data-name="<?php echo $product_cat_option['term_name_ru'] ?>"><img
                                src="<?php echo 'https://sormat.com/wp-content/themes/sormat_main/images/category/' . $product_cat->slug . '.jpg' ?>"
                                alt=""><span
                                class="title hyphenate"><?php echo $product_cat_option['term_name_ru'] ?></span><a href="#"
                                                                                                                class="question"
                                                                                                                data-title="<?php echo $product_cat_option['term_name_ru'] ?>"
                                                                                                                data-description="<?php echo $product_cat->description ?>"></a>
                        <div class="check"></div>
                    </div>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>
        <?php endif ?>

        <?php if (!empty($taxonomies)) : ?>
            <?php foreach ($taxonomies as $taxonomy) : ?>
                <?php $terms = get_terms(['taxonomy' => $taxonomy, 'hide_empty' => true]) ?>
                <div id="content-<?php echo str_replace('-', '', $taxonomy) ?>"
                     class="product-selector-content">
                    <?php if (!empty($terms)) : ?>
                        <?php foreach ($terms as $term): ?>
                            <?php
                                $product_terms_option  = get_option( 'taxonomy_' . $term->term_id );
                            ?>
                            <div class="product-selector-content-box product-category-selector type-<?php echo $term->slug ?> value-<?php echo $term->slug ?>"
                                 data-type="<?php echo $term->slug ?>"
                                 data-value="<?php echo $term->slug ?>" data-name="<?php echo $product_terms_option['term_name_ru'] ?>">
                                <?php $img_slug = $images_slug[$taxonomy] ?? null ?>
                                <?php if (!empty($img_slug)) : ?>
                                    <img
                                            src="<?php echo 'https://sormat.com/wp-content/themes/sormat_main/images/' . $img_slug . '/' . $term->slug . '.png' ?>"
                                            alt="">
                                <?php endif; ?>
                                <span
                                        class="title hyphenate"><?php echo $product_terms_option['term_name_ru'] ?></span><a href="#"
                                                                                                   class="question"
                                                                                                   data-title="<?php echo $product_terms_option['term_name_ru'] ?>"
                                                                                                   data-description="<?php echo $term->description ?>"></a>
                                <div class="check"></div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                    <div class="clear"></div>
                </div>
            <?php endforeach; ?>
        <?php endif ?>
    </div>
    <div id="product-selector-footer">
        <?php if (!empty($taxonomies)) : ?>
            <?php $i = 0; ?>
            <?php foreach ($taxonomies as $taxonomy): ?>
                <?php $tax_obj = get_taxonomy($taxonomy) ?>
                <?php if ($i > 1) : ?>
                    <div id="selector-<?php echo str_replace('-', '', $taxonomy) ?>"
                         class="product-selector-box"><?php echo $tax_obj->label ?></div>
                <?php endif ?>
                <?php $i++ ?>
            <?php endforeach ?>
        <?php endif ?>
        <div class="clear"></div>
        <div class="chosen-scope">
            <div class="scope-left">
            </div>
            <div class="scope-right">
                <a href="#">Очистить все</a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>
<p>Выберите город</p>
<ul class="cities-list">
	<?php
	if ( $cities->have_posts() ) :
		while ( $cities->have_posts() ) : $cities->the_post(); ?>
            <li>
                <a href="<?php echo esc_url( get_home_url() . '?sl=' . get_the_title() ) ?>"><?php echo get_the_title() ?></a>
            </li>
		<?php
		endwhile;
	endif
	?>
    <li>
        <a href="<?php echo esc_url( get_home_url() . '?sl=другое' ) ?>">Другое</a>
    </li>
</ul>
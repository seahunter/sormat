<?php
if (!empty($product_types)) { ?>
    <h3>Результаты поиска <?php echo count($product_types) ?></h3>
    <div class="blue-ruler"></div>
    <div class="search-result-container">
        <?php foreach ($product_types as $key => $type) { ?>
            <div class="search-result-group">
                <div class="search-result-group-title"><h3><?php echo $key ?></h3></div>
                <div class="blue-ruler"></div>
                <?php foreach ($type as $product) { ?>
                    <?php $image_id = get_sormat_image($product->ID); ?>
                    <?php
                        if( 0 === $image_id ) {
                            $image_url = correct_img_url(get_the_post_thumbnail_url( $product->ID, 'medium' ));
                        } else {
                            $image_url = correct_img_url(wp_get_attachment_image_url( $image_id, 'medium' ));
                        }
                    ?>
                    <div class="search-result-holder">
                        <a href="<?php echo get_permalink( $product ) ?>" class="greybox"
                           style="display:block;"><img
                                    src="<?php echo $image_url ?>"
                                    alt="" width="125" height="31"></a>
                        <p><a href="<?php echo get_permalink( $product ) ?>"><?php echo get_the_title( $product ) ?></a></p>
                        <p><?php echo get_field('sales_description_ru', $product) ?></p>
                    </div>
                <?php } ?>
            </div>
            <?php
        } ?>
    </div>
<?php }

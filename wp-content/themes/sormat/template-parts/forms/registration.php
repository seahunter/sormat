<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
    <label for="account_first_name">Имя&nbsp;<span class="required">*</span></label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name"
           id="account_first_name" autocomplete="given-name" value="">
</p>
<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
    <label for="account_last_name">Фамилия&nbsp;<span class="required">*</span></label>
    <input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name"
           id="account_last_name" autocomplete="family-name" value="">
</p>
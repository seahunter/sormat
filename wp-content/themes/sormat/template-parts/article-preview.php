<div class="article-holder <?php echo isset($wrapper_class) ? $wrapper_class : '' ?>">
    <div class="article-news-picture">
        <a href="<?php the_permalink() ?>">
            <?php the_post_thumbnail('medium') ?>
    </div>

    <div class="article-news-text">
        <span class="read-more-icon"></span>
        <p><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></p>
    </div>
</div>
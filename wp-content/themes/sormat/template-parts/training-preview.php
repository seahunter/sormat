<div class="row">
  <div class="one-column image-column news-text left first">
    <?php the_post_thumbnail('medium') ?>
  </div>
  <div class="two-columns news-texts right">
      <article id="post-<?php the_id() ?>" <?php post_class() ?>>

      <a href="<?php esc_url( the_permalink() ) ?>"><h2><?php the_title() ?></h2></a>
      <?php the_excerpt() ?>
    </article>
  </div>
  <div class="clear"></div>
</div>
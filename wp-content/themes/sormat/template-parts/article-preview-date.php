<section class="recent-news-holder first">
  <div class="recent-news-text">
    <h6><?php echo mysql2date( 'F d, Y', get_the_time('d-m-Y H:i:s') ) ?></h6>
    <span class="read-more-icon"></span>
    <p><a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></p>
  </div>
</section>
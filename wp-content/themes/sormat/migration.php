<?php
require_once("../../../wp-load.php");

global $wpdb;

$query = "SELECT ID FROM {$wpdb->posts} WHERE post_status = 'publish' AND post_type = 'product'";
$result = $wpdb->get_col($query);


if (!empty($result)) {
    foreach ($result as $item) {
        update_post_meta($item, '_price', 0);
    }
}

echo "Complete";
(function ($) {
// gmaps variables
    var geocoder;
    var gmap = null;
    var visible_markers = [];
    var kbsearchFull = null;

    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

// Function.prototype.bind polyfill
    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5
                // internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }

            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () {
                },
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP && oThis
                        ? this
                        : oThis,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
        };
    }

    /* Helper for testing local storage support */
    function supportsLocalStorage() {
        try {
            return 'localStorage' in window && window['localStorage'] !== null;
        } catch (e) {
            return false;
        }
    }

    /* Helper for setting a cookie */
    function setCookie(name, value, days) {
        var cookie = name + "=" + value;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + days * 24 * 3600000);
            cookie += "; expires=" + date.toUTCString();
        }
        document.cookie = cookie + "; path=/";
    }

    /* Helper for getting a cookie */
    function getCookie(name) {
        var pattern = new RegExp(name + '=([^;]+)', 'i'),
            match = document.cookie.match(pattern);

        return match && match[1] || null;
    }

    /* Helper for setting state to local storage and cookie */
    function setState(name, value, days) {
        days = days || 360;

        setCookie(name, value, days);
        if (supportsLocalStorage()) {
            try {
                localStorage[name] = value;
            } catch (e) {
            }
        }

        return value;
    }

    /* Helper for getting state from local storage or cookie */
    function getState(name) {
        var state;
        if (supportsLocalStorage()) {
            state = localStorage[name];
        }
        return setState('user-selected', state || getCookie(name));
    }

    function preg_quote(str) {
        return (str + '').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
    }

    function searchHighlight(str, keyword) {
        keyword = preg_quote(keyword);
        keyword = keyword.replace(/\s/g, '|');

        return str.replace(new RegExp(keyword, 'gi'), '<span>$&</span>');
    }

    function sormatIsMobile() {
        return (/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera);
    }

    var lastScrollPosition, documentHeight, windowHeight, footerHeight, footerLowerPart;

    $(document).ready(function () {
        $('a[href*="#"]:not([href="#"])').click(function (e) {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 400);
                    return false;
                }
            }
            return true;
        });
        // Site wide functions
        initFooterAccordion();
        initSearches();
        initChat();

        // Page related functions
        initDialogs();
        initCarousels();
        initProductSelector();
        initResellerMap();

        // init content position after everything else is loaded
        initContentPosition();
        initHeaderFooterScroll();
        initMobileMenu();
        getDimensionsForFooter();

        // Sortable product tables, theme article breather images
        initEnhancements();
        initTableOfContents();

        initPrizePage();

        $('#ask-anything-form').submit(function (e) {
            e.preventDefault();
            var $form = $(this)
            var data = $form.serialize()
            $.ajax({
                type: $form.context.method,
                url: $form.context.action,
                data: data,
                success: function (response) {
                    alert(response)
                }
            });
        })

        $("#floating-back-to-top").click(function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 400);
            return false;
        });

        $('#upper-part').on('click', function (e) {
            if ($('footer').hasClass('closed')) {
                showFooter();
            } else {
                closeFooter();
                $('footer #footer-content .accordion-holder').removeClass('active');
                $('footer #footer-content .open-close-accordion').removeClass('clicked');
            }
        });
    });


    /* Prize page */
    function initPrizePage() {

        // Hide content on confirmation message.
        $(document).bind("gform_post_render", function (event, form_id, current_page) {

            if (form_id === 1 && $(".validation_error").length === 0) {
                $("article").children().not("h1").hide();
            }

            // if ( $(".gform_confirmation_message").length > 0 ) {
            // 	//$(".gforms_confirmation_message").prevAll().not("h1").hide()
            // }
        });


        if ($("#prize-page").length > 0) {

            // Check answers.
            $("#prize-page #check-answers").on("click", function (e) {
                e.preventDefault();
                $("#prize-page .answer").removeClass("hidden");

                $("#prize-page .disabled input").attr("disabled", false);
                $("#prize-page .gform_footer .button").attr("disabled", false);
                $("#prize-page #check-answers").hide();
            });

            // Disable the contact fields.
            $("#prize-page .disabled input").attr("disabled", true);
            $("#prize-page .gform_footer .button").attr("disabled", true);
        }

    }

    /* Handle chat */

    function initChat() {
        $('.chat-form').submit(handleChat);
    }

    function handleChat(e) {
        e.preventDefault();

        userlikeData.user.name = this.elements['chat-email'].value;
        userlikeData.user.email = this.elements['chat-email'].value;
        //userlikeData.custom.question = this.elements['chat-question'].value;
        userlikeUpdateAPI();

        userlikeStartChat();
    }

    /* Handle header, footer and scrolling */
    function initHeaderFooterScroll() {
        /* Handle scrolling */
        lastScrollPosition = $(window).scrollTop();
        footerScroll();
        $(window).scroll(handleScroll);
        $(window).resize(handleResize);

        $('.tools-nav-box.share, .tools-nav-box.language').click(function (event) {
            event.stopPropagation();

            //$(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $(this).children('div').stop().fadeOut(150);
                $(this).removeClass('active').find('span').removeClass('active');
                $(this).next('div').css('border-color', '#e4772a');
            } else {

                // Attach close to document
                if (!sormatIsMobile()) {
                    $(document).on('click', closeMenus);
                }

                $('.tools-nav-box.share, .tools-nav-box.language').removeClass('active');
                $('.tools-nav-box.share, .tools-nav-box.language').find('span').removeClass('active');
                $('.tools-nav-box.share, .tools-nav-box.language').children('div').hide();

                $(this).children('div').stop().fadeIn(150);
                $(this).addClass('active').find('span').addClass('active');
                $(this).next('div').css('border-color', '#efefef');
            }
            //$(this).find('.tools-nav-language-wrapper').toggle();
        });

        /* Dropdown menu */
        $('li.menu-item-has-children').click(function (event) {
            event.stopPropagation();
            lastScrollPosition = 0;
            /*footerScroll();*/
            // Attach close to document
            if (!sormatIsMobile()) {
                $(document).on('click', closeMenus);
            }
            $('#main-nav-holder li.menu-item-has-children').not(this).children('a').removeClass('active');
            $('#main-nav-holder li.menu-item-has-children').not(this).children('.sub-menu').hide();
            $(this).children('a').toggleClass('active');
            $(this).children('.sub-menu').toggle();
            $('#menu-super-service li.menu-item-has-children').children('.sub-menu').show();
        });

        $('select').customSelect();
    }

    function closeMenus() {
        $('.tools-nav-box.share, .tools-nav-box.language').removeClass('active');
        $('.tools-nav-box.share, .tools-nav-box.language').find('span').removeClass('active');
        $('.tools-nav-box.share, .tools-nav-box.language').children('div').hide();
        $('.tools-nav-box.share, .tools-nav-box.language').css('border-color', '#e4772a');

        $('#main-nav-holder li.menu-item-has-children').children('a').removeClass('active');
        $('#main-nav-holder li.menu-item-has-children').children('.sub-menu').hide();

        $(document).off('click');
    }

    /*
    function iosFix() {
      $('body').append($('<div></div>').addClass('iosfix'));
      setTimeout(function() {
        $('.iosfix').remove();
      }, 500);
    }
    */
    function handleScroll() {
        footerScroll();
        backToTopVisibility();
        stickyNav();
        hideScrolldownNotice();
    }

    function handleResize() {
        setContentPaddingTop();
        stickyNavAfterResize();
        footerScroll();
        resizeOverlayHolder();
        hideScrolldownNotice();
    }

// Check scrolling direction. Returns 1=down, 0=still or -1=up.
    function getScrollDirection() {
        var scrollMax = $(document).height() - windowHeight,
            currentScrollPosition = Math.min(scrollMax, Math.max(0, $(window).scrollTop())),
            delta = currentScrollPosition - lastScrollPosition,
            direction = 0;

        if (delta > 0) {
            direction = 1;
        } else if (delta < 0) {
            direction = -1;
        }

        lastScrollPosition = currentScrollPosition;

        return direction;
    }

    function stickyNav() {
        var scroll = $(window).scrollTop();
        var previousScroll = lastScrollPosition;
        var offset = $('#lower-wrapper').offset().top;
        var headerY = 0;

        if (!$('#upper-wrapper').hasClass('animated')) {

            // At top
            if (scroll <= 0) {
                //iosFix();
                $('#upper-wrapper').addClass('animated').animate({
                    marginTop: 0
                }, afterNavAnimation);

                // Not at top
            } else if (lastScrollPosition <= 0 || parseInt($('#upper-wrapper').css('marginTop'), 10) >= 0) {
                //iosFix();
                headerY = -$('#upper-wrapper').height();
                if ($('#menu-main-navigation').offset().top != $('#logo-holder').offset().top) {
                    headerY -= $('#logo-holder').height();
                }

                $('#upper-wrapper').addClass('animated').animate({
                    marginTop: headerY
                }, afterNavAnimation);

            }
        }

        lastScrollPosition = scroll;
    }

    function stickyNavAfterResize() {

        if ($(window).scrollTop() <= 0) {
            $('#upper-wrapper').addClass('animated').animate({
                marginTop: 0
            }, afterNavAnimation);
        }

    }

    function afterNavAnimation() {
        $('#upper-wrapper').removeClass('animated');
    }

    function getDimensionsForFooter() {
        footerHeight = $('#footer-content-holder').height();
        footerLowerPart = $('footer #lower-part').innerHeight();
        documentHeight = $(document).innerHeight();
        windowHeight = $(window).innerHeight();
    }

    function footerScroll() {
        getDimensionsForFooter();

        if (windowHeight + $(window).scrollTop() - 2 < documentHeight - footerHeight) {
            $('footer').css('position', 'fixed');
            $('footer').css('left', '0');
            $('footer').css('bottom', '-' + footerHeight + 'px');
            $('body').css('padding-bottom', footerHeight + footerLowerPart + 'px');
            $('footer').removeClass('open').addClass('closed');
            $('footer #footer-content .accordion-holder').removeClass('active');
            $('footer #footer-content .open-close-accordion').removeClass('clicked');
        } else {
            $('footer').css('position', 'relative');
            $('footer').css('left', '0');
            $('footer').css('bottom', '0');
            $('body').css('padding-bottom', '0');
            $('footer').removeClass('closed').addClass('open');
        }
    }

    function showFooter() {
        $('html, body').animate({
            scrollTop: documentHeight
        }, 500);
        $('footer').removeClass('closed').addClass('open');
    }

    function closeFooter() {
        $('html, body').animate({
            scrollTop: documentHeight - windowHeight - footerHeight
        }, 500);
        $('footer').removeClass('open').addClass('closed');
    }

    function backToTopVisibility() {
        if ($(window).scrollTop() > 100) {
            $("#floating-back-to-top").addClass("visible");
        } else {
            $("#floating-back-to-top").removeClass("visible");
        }
    }

    /* Searches */
    var mainTimeout, secondaryTimeout;

    /**
     * Initialize searches.
     */
    function initSearches() {

        // Initialize search for the top menu and frontpage search.
        //initAutosearch( $("#secondary-search"), siteSearchHandler  );
        initAutosearch($("#main-search"), frontSearchHandler);

        // Initialize search for the search page.
        if ($("body").hasClass("search")) {

            initAutosearch($("#knowledge-search"), searchHandler);

            // Highlight only keyword, not whole word
            var keyword = $("#knowledge-search").val();

            $("#search-result-items").find("em").each(function () {
                $(this).html(searchHighlight($(this).text(), keyword));
            });

        } else {
            initAutosearch($("#knowledge-search"), knowledgeSearchHandler);
        }

        var secondaryTimeout;

        /*
        $(".tools-nav-box.search").on( "click", function() {
            secondarySearchOpen();
            $("#secondary-search").focus();
        });

        $("#secondary-search").blur( secondarySearchClose );
        */

        $("#main-search").focus(mainSearchOpen);
        $("#main-search").blur(mainSearchClose);

        $("#knowledge-search").focus(knowledgeSearchOpen);
        $("#knowledge-search").blur(knowledgeSearchClose);

        $("#reseller-search").focus(resellerSearchOpen);
        $("#reseller-search").blur(resellerSearchClose);

    }


    function secondarySearchOpen() {
        if (secondaryTimeout) {
            clearTimeout(secondaryTimeout);
            secondaryTimeout = null;
        }

        //$(this).closest('div').find('#tools-nav-search-results-wrapper').stop().fadeIn(150);
        $('.tools-nav-box.search').addClass('active').find('.search-icon').addClass('active');
        $('.tools-nav-box.search').next('.tools-nav-box').addClass('no-border');
    }

    function secondarySearchClose() {
        secondaryTimeout = setTimeout(function () {
            $('.tools-nav-box.search').removeClass('active').find('.search-icon').removeClass('active');
            $('.tools-nav-box.search').next('.tools-nav-box').removeClass('no-border');
            $('#tools-nav-search-results-wrapper').stop().fadeOut(150);
        }, 200);
    }

    function mainSearchOpen() {
        if (mainTimeout) {
            clearTimeout(mainTimeout);
            mainTimeout = null;
        }

        $('#main-search-wrapper').css('background', '#dcdcdc');
        $('#main-search-wrapper .search-icon').addClass('active');
        //$('#search-result-container').show();
    }

    function mainSearchClose() {
        mainTimeout = setTimeout(function () {
            $('#main-search-wrapper').css('background', '#efefef');
            $('#main-search-wrapper .search-icon').removeClass('active');
            $('#search-result-container').hide();
        }, 200);
    }

    function knowledgeSearchOpen() {
        $('#knowledge-search-form .search-icon').addClass('active');
    }

    function knowledgeSearchClose() {
        $('#knowledge-search-form .search-icon').removeClass('active');
    }

    function resellerSearchOpen() {
        $('#reseller-search-form .search-icon').addClass('active');
    }

    function resellerSearchClose() {
        $('#reseller-search-form .search-icon').removeClass('active');
    }


    /**
     * Initialize site searches.
     *
     * @param object   input   Search input field.
     * @param callback handler Search submit handler function.
     **/
    function initAutosearch(input, handler) {
        if (!input.length) {
            return;
        }

        var delay,
            form = input.closest("form");

        input.on("keyup", function () {
            if (delay) {
                clearTimeout(delay);
            }

            delay = setTimeout(handler, 500);
        });

        form.on("submit", handler);

    }

    /*
    function siteSearchHandler (e)
    {
        if (e) { e.preventDefault(); }

        var input = $('#secondary-search'),
            keyword = input.val();

        input.focus();

        if (keyword.length) {
            $('.tools-nav-box.search')
                .append('<img class="ajax-loader" src="' + themeURL + '/images/ajax-loader.gif" alt="" style="position:absolute;z-index:100;right:50px;top:2px;" />');

            $.get($('#secondary-search-form').attr('action') + '?s=' + keyword + '&context=site&autosearch=1', function (data) {
                $('.tools-nav-box.search .ajax-loader').remove();

                $('#tools-nav-search-results-wrapper').html(data).stop().fadeIn(150);

                // Highlight only keyword, not whole word
                $('#tools-nav-search-results-wrapper').find('em').each(function () {
                    $(this).html(searchHighlight($(this).text(), keyword));
                });
            });
        }
    }
    */

    function frontSearchHandler(e) {
        if (e) {
            e.preventDefault();
        }

        var input = $('#main-search'),
            keyword = input.val();

        input.focus();

        if (keyword.length) {
            $('#main-search-holder .center-search-elements')
                .append('<img class="ajax-loader" src="' + themeURL + '/images/ajax-loader.gif" alt="" style="position:absolute;left:' + (input.position().left + input.outerWidth() - 36) + 'px;top:14px;" />');

            $.get($('#main-search-form').attr('action') + '?s=' + keyword + '&context=front&autosearch=1', function (data) {

                $('#main-search-holder .ajax-loader').remove();

                $('#search-result-container').html(data).stop().fadeIn(150);

                // Highlight only keyword, not whole word
                $('#search-result-container').find('em').each(function () {
                    $(this).html(searchHighlight($(this).text(), keyword));
                });

            });
        }
    }

    /**
     * Handles the knowledge base search.
     *
     * @param Object e Event.
     */
    function knowledgeSearchHandler(e) {
        if (e) {
            e.preventDefault();
        }

        var keyword = $('#knowledge-search').val();

        // Stores the original full list of articles to reset to when there is
        // no user search (i.e. user remove everything from the search box).
        if (kbsearchFull === null) {
            kbsearchFull = $('<div>').append($('.search-result-container').clone()).html();
        }

        // Perform actual search.
        if (keyword.length) {
            $('.search-result-container').html('<img class="ajax-loader" src="' + themeURL + '/images/ajax-loader.gif" alt="" style="margin-left:30px" />');

            $.get($('#knowledge-search-form').attr('action') + '?s=' + keyword + '&context=kb&autosearch=1', knowledgeSearchResultHandler);
        } else {
            // Restores the full list of articles.
            knowledgeSearchResultHandler(kbsearchFull);
        }
    }

    /**
     * Sets the knowledge base search results.
     *
     * @param  string data Search results HTML.
     */
    function knowledgeSearchResultHandler(data) {
        $('.search-result').html(data);
    }


    /**
     * Default search handler.
     *
     * @param object e Event.
     **/
    function searchHandler(e) {

        if (e) {
            e.preventDefault();
        }

        var keyword = $("#knowledge-search").val();

        if (keyword.length) {

            $("#search-result-items").stop().fadeOut(150);

            $(".search-keyword").text(keyword);

            // Fetch search results and add them to the page.
            $.get($("#knowledge-search-form").attr("action") + "?s=" + keyword + "&autosearch=1", function (data) {
                $("#search-result-items").html(data).stop().fadeIn(150);

                // Highlight only keyword, not whole word
                $('#search-result-items').find('em').each(function () {
                    $(this).html(searchHighlight($(this).text(), keyword));
                });

            });
        }

    }


    /**
     * Initialise dialogs overlays.
     * e.g. product selector question mark dialog.
     **/
    function initDialogs() {

        // Product and product group images.
        $('.thumbnail-wrapper').each(function () {
            $(this).magnificPopup({
                delegate: '.product-image-link',
                type: 'image',
                tLoading: '',
                gallery: {
                    enabled: true,
                    tCounter: ''
                }
            });
        });

        // Email link share
        $(".share-link-email").on("click", function (e) {

            e.stopPropagation();
            e.preventDefault();

            openOverlayHolder("email-share");

        });

        //$('.question').on('click', openOverlayHolder);
        $("a.question").click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            $('#question-holder').html('<h3>' + $(this).data('title') + '</h3><p>' + $(this).data('description') + '</p>');

            openOverlayHolder();
        });

        $("a.notes").click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            $('#question-holder').html('<h3>' + $(this).data('title') + '</h3><p>' + $(this).data('description') + '</p>');

            openOverlayHolder();
        });

        $("a.approval-link").click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            $('#question-holder').load(ajaxurl, {
                action: 'approval_dialog',
                postid: $(this).data('identifier'),
                lang: $(this).data('lang')
            }, openOverlayHolder);
        });

        $("a.certificate-link").click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            $('#question-holder').load(ajaxurl, {
                action: 'certificate_dialog',
                postid: $(this).data('identifier'),
                lang: $(this).data('lang')
            }, openOverlayHolder);
        });


        if (!getState('cookies')) {
            showCookieNotifier();
        }

        $('#overlay,#close-question').on('click', closeOverlayHolder);
        $('#cookie-text-holder .close-icon-cross, #cookie-text-pop-up .close-popup').on('click', closeCookieNotifier);
    }


    /**
     * Open overlay holder.
     *
     * @param string element Overlay identifier, if no identifier is provived the
     *                       question-dialog is used.
     */
    function openOverlayHolder(identifier) {

        if (identifier === "email-share") {
            $element = $("#email-share-dialog-wrapper");

            var messageText = $("#email-share-dialog-wrapper #email-share-message").data("default").trim();

            $("#email-share-dialog-wrapper input[type=text]").val("")
            $("#email-share-dialog-wrapper #email-share-message").val(messageText + " " + document.URL);

            var maxCharacters = 500;
            $("#email-share-dialog-wrapper .remaining-message-characters span").text(maxCharacters - messageText.length);


            $("#email-share-dialog-wrapper .feedback .success").hide();
            $("#email-share-dialog-wrapper .feedback .error").hide();
            $("#email-share-dialog-wrapper form").show();

        } else {
            $element = $("#question-dialog-wrapper");
        }

        $element.css("top", ($(window).height() - $element.height()) / 2 + $(window).scrollTop() + "px");
        $element.css("left", ($(window).width() - $element.width()) / 2 + "px");
        $element.fadeIn(300);

        $("#overlay").show();
        $("#overlay").css("width", $(window).width());
        $("#overlay").css("height", $(window).height() + 100);
        $("#overlay").animate({"opacity": 0.7});

    }


    /**
     *
     */
    function resizeOverlayHolder() {

        $("#question-dialog-wrapper").css("left", ($(window).width() - $("#question-dialog-wrapper").width()) / 2 + "px");
        $("#email-share-dialog-wrapper").css("left", ($(window).width() - $("#email-share-dialog-wrapper").width()) / 2 + "px");

        $("#overlay").css("width", $(window).width());
        $("#overlay").css("height", $(window).height() + 100);

    }

    /**
     * Close the overlay holder
     */
    function closeOverlayHolder() {

        $('#question-dialog-wrapper').fadeOut(300);
        $('#email-share-dialog-wrapper').fadeOut(300);

        $('#overlay').animate({'opacity': 0.0}, function () {
            $('#overlay').hide();
        });
    }

    function showCookieNotifier() {
        $('#cookie-text-holder').show();

        /* for cookie popup window to appear below defined resolution
        if ($(window).width < 768) {
            $('#cookie-text-pop-up').show();
        }
         */
    }

    function closeCookieNotifier() {
        $('#cookie-text-holder, #cookie-text-pop-up').fadeOut(setContentPaddingTop);

        setState('cookies', 1);
    }


    /**
     * Email Link Share: Submit.
     **/
    $("#email-share-dialog-wrapper form").submit(function (e) {

        e.preventDefault();

        var $wrapper = $("#email-share-dialog-wrapper");

        if ($("#email-share-from-email", $wrapper).val().length != 0) {
            return false;
        }


        var data = {
            "action": "email-share-link",
            "url": document.URL,
            "page-title": document.title,
            "email-share-from-name": $("form #email-share-from-name", $wrapper).val(),
            "email-share-from-email": $("form #email-share-from-email", $wrapper).val(),
            "email-share-to-email": $("form #email-share-to-email", $wrapper).val(),
            "email-share-message": $("form #email-share-message", $wrapper).val()
        };

        var response = $.post(ajaxurl, data);

        response.done(function (data) {

            if (data.success === true) {

                $("form", $wrapper).hide();
                $(".success", $wrapper).show();
            } else {
                $("form", $wrapper).hide();
                $(".error", $wrapper).show();
            }

        });

        response.error(function (data) {
            $("form", $wrapper).hide();
            $(".error", $wrapper).show();
        });

    });


    /**
     * Email Link Share: Continue button on feedback closes the overlay.
     */
    $("#email-share-dialog-wrapper .continue").on("click", function (e) {
        e.preventDefault();
        $("#close-question").trigger("click");
    });


    /**
     * Email Link Share: Message character counter.
     */
    $("#email-share-dialog-wrapper form #email-share-message").on("keyup keydown", function (e) {

        var maxCharacters = 500;

        var message = $(this).val();

        // do not include the link in the count
        var urlCleanMessage = message.replace(document.URL, '');

        var remainingCharacters = maxCharacters - urlCleanMessage.length;

        $("#email-share-dialog-wrapper .remaining-message-characters span").text(remainingCharacters);

        if (remainingCharacters < 0) {
            this.setCustomValidity($(this).data("errortxt"));
        } else {
            this.setCustomValidity("");
        }

        if ($(this).val().indexOf(document.URL) === -1) {
            this.setCustomValidity($(this).data("errorlink"));
            $(this).val($(this).val() + ' ' + document.URL);

        } else {
            this.setCustomValidity("");

        }

    });


    /**
     * Try again button brings back the form with the previous input.
     **/
    $("#email-share-dialog-wrapper .retry").on("click", function (e) {
        e.preventDefault();

        var $wrapper = $("#email-share-dialog-wrapper");

        $(".success", $wrapper).hide();
        $(".error", $wrapper).hide();
        $("form", $wrapper).show();
    });


    /* Owl carousels, like front page carousel */

    function initCarousels() {
        $('.owl-carousel').owlCarousel({
            items: 1,
            singleItem: true,
            autoPlay: 5000,
            stopOnHover: true,
            responsive: true
        });
    }


    /* Product selector */

    var ignoreHashChange = false;

    /**
     * Initializes the Product Selector.
     */
    function initProductSelector() {

        if (!$('#product-selector-holder').length) {
            return;
        }

        // Display scrolldown notice.
        $('.scrolldown-notice-popup').click(function (e) {
            e.preventDefault();
            $('html,body').animate({scrollTop: $('.search-result').offset().top}, 400);
        });

        // Handle preselected items
        var selected = $('.product-selector-content .selected');
        if (selected.length) {
            selected.each(function () {
                addScope($(this), true);
            });
        }

        // Keep track of hash queries
        $(window).on('hashchange', handleHashChange);
        handleHashChange();

        $(".product-selector-box").click(handleClickedSelector);

        if ($('body').hasClass('home')) {
            $(".product-category-selector").click(transitionToProducts);
        } else {
            $(".product-category-selector").click(handleClickedItem);
        }

        $("#product-selector-holder .scope-left").on('click', 'a', removeScope);
        $("#product-selector-holder .scope-right a").click(clearScope);
    }


    /**
     * Handles the Product Selector search.
     **/
    function productgroupSearchHandler(e) {

        if (e) {
            e.preventDefault();
        }

        $('.scrolldown-text-searched').hide();


        if (searchHeaderIsHidden()) {
            $('.scrolldown-notice-popup').css('left', ($(window).width() / 2 - $('.scrolldown-notice-popup').width() / 2)).addClass('visible').find('.scrolldown-indicator').html('<img class="ajax-loader" src="' + themeURL + '/img/ajax-loader.gif" alt="" style="vertical-align: middle;" />');
            $('.scrolldown-notice-popup').removeClass('searched');
            $('.scrolldown-notice-searched').hide();
            $('.scrolldown-notice-searching').show();
        }

        $('.search-result').show();
        $('.search-result-container').prepend('<img class="ajax-loader" src="' + themeURL + '/images/ajax-loader.gif" alt="" style="margin-left:30px" />');

        var facets = $('#product-selector-holder .scope-left a').map(mapProductSelections).get().join('&'),
            action = $('#productgroup-search-form').attr('action');

        // $.get(action + '?s=' + encodeURIComponent('f:') + '&' + facets + '&context=group&autosearch=1', productgroupSearchResultHandler);

        $.ajax({
            url: action,
            type: 'POST',
            data: facets,
        })
            .done(function (response) {
                productgroupSearchResultHandler(response.data)
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });


        // Generate url, group selections
        var urls = {};

        $('.product-selector-content .selected').each(function () {

            var item = $(this),
                type = item.data('type'),
                mapped = type + '[]=' + item.data('value');

            if (!urls[type]) {
                urls[type] = [];
            }

            urls[type].push(item.data('value'));

        });

        // Generate url, join
        var url = $.map(urls, function (value, key) {
            return key + '=' + value.join(',');
        }).join('&');

        // Update URL
        ignoreHashChange = true;
        if (history.pushState) {
            history.replaceState(null, null, '#' + url);
        } else {
            location.hash = url;
        }

    }

    function handleHashChange() {
        if (!ignoreHashChange) {
            if (location.hash.length > 1) {
                var hash = location.hash.substr(1),
                    sections = hash.split('&');

                if (sections.length) {

                    clearScope(null, true);

                    $.each(sections, function (index, value) {
                        var tmp = value.split('='),
                            type = tmp[0],
                            items = tmp[1].split(',');

                        $.each(items, function (index, value) {
                            var item = $('.type-' + type + '.value-' + value);
                            addScope(item, true);
                        });
                    });

                    productgroupSearchHandler();

                }
            } else {
                clearScope();
            }
        }

        ignoreHashChange = false;
    }

    function mapProductSelections(index) {
        var item = $(this).data('item'),
            type = item.data('type'),
            value = item.data('value'),
            mapped = type + '[]=' + value;

        return mapped;
    }

    var firstSearchNotice = true;

    function productgroupSearchResultHandler(data) {
        $('.search-result').html(data);

        // If product group title is wider than the container,
        // add an invisible item next to it so that the title
        // doesn't overflow and thus be cut-off.
        $('.search-result-group').each(function (i, obj) {
            var titleContainerWidth = $(this).find('.search-result-group-title').width();
            var titleWidth = $(this).find('.search-result-group-title h3').width();

            if (titleWidth > titleContainerWidth) {
                $(this).append('<div class="search-result-holder"></div>');
            }
        });

//    if(firstSearchNotice) {
        showHideScrolldownNotice();
//    }
    }

    function showHideScrolldownNotice() {
        if (searchHeaderIsHidden()) {
            firstSearchNotice = false;
            $('.scrolldown-notice-searching').hide();
            $('.scrolldown-text-searched').show();
            $('.scrolldown-notice-popup').addClass('searched');
        } else {
            $('.scrolldown-notice-popup').removeClass('visible searched');
        }
//    hideScrolldownNotice();
    }

    function hideScrolldownNotice() {
        //center popup while visible
        $('.scrolldown-notice-popup.visible').css('left', ($(window).width() / 2 - $('.scrolldown-notice-popup').width() / 2));

        if ($('.scrolldown-notice-popup').hasClass('searched') || !searchHeaderIsHidden()) {
            $('.scrolldown-notice-popup').removeClass('visible searched');
        }
        /*  if($('.scrolldown-notice-popup').scrollTop() >= $('.row.search-result').scrollTop()) {
            $('.scrolldown-notice-popup').removeClass('visible searched');
            console.log($('.scrolldown-notice-popup').scrollTop(), $('.row.search-result').scrollTop());
          }*/
    }

    function searchHeaderIsHidden() {
        if ($('#product-selector-holder').length > 0) {
            var resultOffset = $('#product-selector-holder').offset().top + $('#product-selector-holder').height() + 180,
                windowHeight = $(window).height();
            if (windowHeight < resultOffset && windowHeight + $(window).scrollTop() < resultOffset + 60) {
                return true;
            } else {
                return false;
            }
        }
    }

    function addScope(item, nosearch) {
        item.addClass('selected');

        var scope = $('<a href="#">' + item.data('name') + '</a>');

        scope.data('item', item);
        item.data('scope', scope);

        $("#product-selector-holder .scope-left").append(scope);

        $('.chosen-scope').slideDown();

        // Send search request
        if (!nosearch) {
            productgroupSearchHandler();
        }
    }

    function removeScope(e, clear, dontclose) {
        if (e) e.preventDefault();

        var clickedElement = $(this),
            item = clickedElement.data('item');

        if (item) {
            item.removeClass('selected');
        }
        clickedElement.remove();

        // If all selections are removed, hide scope
        if (!dontclose && !$(".chosen-scope .scope-left a").length) {
            $('.chosen-scope').slideUp();

        }

        /* Obsolete
        // If all product category selections are removed, reset selector
        if (!$('#content-productcategories .selected').length) {
            var firstselector = $(".product-selector-box:first");

            //$(".product-selector-box").addClass('inactive');
            //firstselector.removeClass('inactive');

            if (!firstselector.hasClass('active')) {
                firstselector.click();
            }

        // Otherwise search, if not clearing
        } else
        */

        if (clear || !$('.product-selector-content .selected').length) {

            var title = $('.search-result > h3');
            title.text(title.text().replace(/\(\d+\)/, ''));
            $('.search-result-container').empty();
            location.hash = '';

        } else {

            // Send search request
            productgroupSearchHandler();

        }
    }

    function clearScope(e, dontclose) {
        if (e) e.preventDefault();

        $(".chosen-scope .scope-left a").each(function () {
            removeScope.apply(this, [null, true, dontclose]);
        });
    }

    function handleClickedSelector(e) {
        if (e) e.preventDefault();

        var clickedElement = $(this);

        if ($("#product-selector-holder").hasClass('open')) {

            var isActive = clickedElement.hasClass('active');
            if (!clickedElement.hasClass('inactive')) {
                $(".product-selector-box").removeClass('active');

                if (!isActive) {
                    clickedElement.addClass('active');
                }

                $("#product-selector-content").slideUp("slow", function () {

                    var group = clickedElement.attr('id').split('-')[1];
                    console.log(group)
                    $(this).find('.product-selector-content').removeClass('selected').hide();
                    $(this).find('#content-' + group).addClass('selected').show();

                    // Init owl carousel
                    /*
                    $('#content-' + group).owlCarousel({
                        pagination: false,
                        responsive: true,
                        scrollPerPage: true,
                        items: 6,
                        itemsDesktop: false,
                        itemsDesktopSmall: [979,5],
                        itemsTablet: [767,5],
                        itemsTablet: [710,3],
                        itemsTabletSmall: false,
                        itemsMobile: [320,3]
                    });
                     */
                    if (!isActive) {
                        $("#product-selector-content").slideDown("slow");
                    }

                });
            }

        }
    }

    function handleClickedItem(e) {
        e.preventDefault();

        var item = $(this),
            type = item.data('type'),
            value = item.data('value'),
            name = item.data('name');

        if (item.hasClass('selected')) {
            var scope = item.data('scope');
            if (scope) {
                removeScope.apply(scope);
            }
        } else {
            addScope(item);
        }

        if ($('#content-productcategories .selected').length) {
            //$(".product-selector-box").removeClass('inactive');
        } else {
            //$(".product-selector-box").addClass('inactive');
            //$(".product-selector-box:first").removeClass('inactive');
        }
    }

    function transitionToProducts(e) {
        e.preventDefault();

        var item = $(this),
            type = item.data('type'),
            value = item.data('value'),
            group = item.closest('.product-selector-content').attr('id').split('-')[1],
            url = $('#product-selector-header-text a').attr('href') + '?cat=' + encodeURIComponent(group) + '#' + encodeURIComponent(type) + '=' + encodeURIComponent(value);

        location.href = url;
    }


    /**
     * Resellers Map (uses Google Maps Geocoding and Leafleft with marker clustering).
     **/
    var ResellersMap = {};


    /**
     * Initialize resellers map.
     */
    function initResellerMap() {

        if ($("#map-resellers").length > 0) {
            ResellersMap.loadMap();
        }

    }


    /**
     * Load the reseller locator map.
     **/
    ResellersMap.loadMap = function () {

        geocoder = new google.maps.Geocoder(); // Load geocoder - required to convert search input into a coordinate.

        ResellersMap.RADIUS = 50000;

        // Load map base.
        ResellersMap.map = new L.Map("map-resellers", {
            center: new L.LatLng(53.9618, 58.4277),
            zoom: 3,
            attributionControl: false
        });

        // Load layers (Open Street Map, Google Maps: Terrain and Roadmap) - Temporary.
        var osm = new L.TileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png");
        var ggl = new L.Google("ROADMAP");
        var ggl2 = new L.Google("TERRAIN");

        ResellersMap.map.addLayer(ggl);

        /*ResellersMap.map.addControl( new L.Control.Layers( {
            "Open Street Map": osm,
            "Google Roadmap":  ggl,
            "Google Terrain":  ggl2
            }, {} ));*/

        // Submit the search form and update the map
        $("#reseller-search-form").submit(ResellersMap.queryRetailers).submit();

    };


    /**
     * Load the reseller location markers to the map.
     *
     * @param array markersToLoad List of resellers markers.
     */
    ResellersMap.loadMarkers = function (markersToLoad) {

        console.log(markersToLoad);
        if (markersToLoad.length === 0) {
            // return false;
            markersToLoad = markers;
        }

        ResellersMap.loadedMarkers = markersToLoad;

        ResellersMap.iconDefault = L.icon({
            iconUrl: themeURL + "/images/marker-default.png",
            iconRetinaUrl: themeURL + "/images/marker-default.png",
            iconSize: [32, 52],
            iconAnchor: [16, 52],
            popupAnchor: [0, -46],
            // shadowUrl: 'my-icon-shadow.png',
            // shadowRetinaUrl: 'my-icon-shadow@2x.png',
            // shadowSize: [68, 95],
            // shadowAnchor: [22, 94]
        });


        var LeafletMarkers = [];

        ResellersMap.LeafletClusteredMarkers = L.markerClusterGroup({
            showCoverageOnHover: false,
            spiderfyOnMaxZoom: false,
            zoomToBoundsOnClick: true,
            iconCreateFunction: function (cluster) {
                var clusterMarkers = cluster.getAllChildMarkers();
                // console.log( clusterMarkers.length );
                var n = 0;

                // for ( var i = 0; i < clusterMarkers.length; i++ ) {
                // 	console.log( clusterMarkers[ i ] );
                // 	n += clusterMarkers[ i ].number;
                // 	// console.log(n);
                // }

                return L.divIcon({html: clusterMarkers.length, className: 'cluster-icon', iconSize: L.point(42, 42)});
            }
        });

        var markersBounds = [];

        ResellersMap.loadedMarkers.forEach(function (m) {

            if (!m.lat || !m.lng) {
                return true;
            }

            markersBounds[markersBounds.length] = [m.lat, m.lng];

            tmpMarker = L.marker([m.lat, m.lng], {icon: ResellersMap.iconDefault});

            LeafletMarkers[LeafletMarkers.length] = tmpMarker;

            m.markerObject = tmpMarker;

            ResellersMap.LeafletClusteredMarkers.addLayer(tmpMarker);

        });

        var markersLayer = ResellersMap.map.addLayer(ResellersMap.LeafletClusteredMarkers);

        var bounds = L.latLngBounds(markersBounds);

        ResellersMap.map.fitBounds(bounds);


        // Display reseller data when the marker clicked.
        ResellersMap.LeafletClusteredMarkers.on("click", function (e) {

            var matchMakerData = ResellersMap.getResellerInfo(e, true);

            $(".map-left").removeClass("full-width");

            $(".map-right").animate({width: "show"}, 350);

            ResellersMap.map.invalidateSize();

            ResellersMap.map.setView(e.latlng, 14);

        });

    };


    /**
     * Display the reseller search results.
     *
     * @param string resultsHTML Search results HTML.
     */
    ResellersMap.displaySearchResults = function (resultsHTML) {

        $("#reseller-page .search-result").show();

        $(".search-result-container").html(resultsHTML);

        $(".search-result-container .search-result-holder").click(ResellersMap.searchResultClickHandler);

    };


    /**
     * Handle the selection of a reseller search result.
     *
     * object e Click event.
     */
    ResellersMap.searchResultClickHandler = function (e) {

        e.preventDefault();

        var selectedMarker = ResellersMap.loadedMarkers[$(this).index()];

        ResellersMap.getResellerInfo(selectedMarker);

        $(".map-left").removeClass("full-width");

        $(".map-right").animate({width: "show"}, 350);

        ResellersMap.map.invalidateSize();

        ResellersMap.map.setView(selectedMarker.markerObject.getLatLng(), 14);

        return false;

    }


    /**
     * Get Reseller info.
     *
     * @param object marker Clicker marker event information.
     * @return object       Object with clicked reseller information.
     **/
    ResellersMap.getResellerInfo = function (marker, mapMarker) {

        if (!marker) {
            return false;
        }

        var resultObj = {};

        if (mapMarker === true) {

            // Look for clicked marker.
            markers.forEach(function (m) {

                if (!m.lat && !m.lng) {
                    return true;
                } // skip marker if it does not have proper coordinates.

                var ll = L.latLng(m.lat, m.lng);

                // Create the data object if there is a match and add the data to the HTML.
                if (ll.equals(marker.latlng)) {
                    marker = m;
                }

            });

        }

        resultObj.type = decodeURIComponent(marker.type) || "";
        resultObj.title = decodeURIComponent(marker.title) || "";
        resultObj.address1 = decodeURIComponent(marker.address1) || "";
        resultObj.address2 = decodeURIComponent(marker.address2) || "";
        resultObj.zip = decodeURIComponent(marker.zip) || "";
        resultObj.city = decodeURIComponent(marker.city) || "";
        resultObj.country = decodeURIComponent(marker.country) || "";
        resultObj.phone = decodeURIComponent(marker.phone) || "";
        resultObj.url = decodeURIComponent(marker.url) || "";

        $(".reseller_type").text(resultObj.type);
        $(".reseller_name").text(resultObj.title);
        $(".reseller_address_1").text(resultObj.address1);
        $(".reseller_address_2").text(resultObj.address2);
        $(".reseller_address_3").text(resultObj.zip + '  ' + resultObj.city);
        $(".reseller_country").text(resultObj.country);
        $(".reseller_phone").text(resultObj.phone);
        $(".reseller_url").text(resultObj.url);
        $(".reseller_url").attr('href', resultObj.url);

        return resultObj;

    };


    /**
     * Query the retailers based on the user search.
     **/
    ResellersMap.queryRetailers = function (e) {

        e.preventDefault();

        // Get value from user search.
        var search = $("#reseller-search").val();

        // Loaded all the markers when there is no search keyword.
        if (!search.length) {
            ResellersMap.loadMarkers(markers);
            return false;
        }

        // Get the markers within a radius (i.e. ResellersMap.RADIUS) of the location of the search point.
        geocoder.geocode({address: search, language: 'en'}, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {

                // Get country name
                queriedCountry = null;
                queriedCountryHits = 0;

                for (var n = 0; n < results[0].address_components.length; n++) {

                    if ($.inArray('country', results[0].address_components[n].types) > -1) {
                        queriedCountry = results[0].address_components[n].long_name.toLowerCase();
                    }

                }

                // Convert some inaccurate results to get better results.
                var convertResults = {
                    '61.924,25.748': new google.maps.LatLng(60.17332440000001, 24.941024800000037), // Finland -> Helsinki
                    '61.524,105.319': new google.maps.LatLng(55.755826, 37.6173)                     // Russia -> Moscow
                };

                var approxLatLng = results[0].geometry.location.lat().toFixed(3) + ',' + results[0].geometry.location.lng().toFixed(3);

                if (convertResults[approxLatLng]) {
                    results[0].geometry.location = convertResults[approxLatLng];
                }

                var searchedPosition = new L.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

                // Debug code: add circle to show the radius area.
                // var filterCircle = L.circle( searchedPosition, ResellersMap.RADIUS, {
                // 	opacity: 1,
                // 	weight: 1,
                // 	fillOpacity: 0.4
                // }).addTo( ResellersMap.map );

                var markersWithinRadius = [];
                var markersWithinRadiusHTML = '';

                markers.forEach(function (m) {

                    if (!m.lat || !m.lng) {
                        return true;
                    }

                    var ll = new L.LatLng(m.lat, m.lng);

                    if (ll.distanceTo(searchedPosition) <= ResellersMap.RADIUS) {

                        markersWithinRadius.push(m);

                        // Generate result HTML.
                        markersWithinRadiusHTML += '<a href="#" class="search-result-holder">'
                            + '		<div class="picture-wrapper">'
                            + '			<img src="' + mapImagesURL + m.map + '" />'
                            + '			<img src="' + themeURL + '/images/marker-default.png" class="marker-icon" />'
                            + '		</div>'
                            + '		<div class="text-wrapper">'
                            + '			<p><span>' + decodeURIComponent(m.title) + '</span> ' + decodeURIComponent(m.city) + '</p>'
                            + '		</div>'
                            + '	</a>';

                    }

                });

                if (markersWithinRadiusHTML != '') {
                    ResellersMap.displaySearchResults(markersWithinRadiusHTML);
                }

                ResellersMap.loadMarkers(markersWithinRadius);

            } else {
                ResellersMap.loadMarkers(markers);
            }

        });

        return false;

    };


    function loadGoogleMap(containerID) {
        var mapmarker;
        var autocomplete;
        var geocoderPosition;
        var firstRun = true;
        var initialLocation;
        var browserSupportFlag = new Boolean();
        var zoom = 3;
        var myLatlng;
        myLatlng = new google.maps.LatLng(55.270582, 15.474808);

        var myOptions = {
            zoom: zoom,
            center: myLatlng,
            disableDefaultUI: true,
            zoomControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        gmap = new google.maps.Map(document.getElementById(containerID), myOptions);
        geocoder = new google.maps.Geocoder();
        google.maps.event.trigger(gmap, "resize");
        //position: new google.maps.LatLng(value.lat,value.lng),
        if (markers.length > 0) {
            $.each(markers, function (index, value) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(value.lat, value.lng),
                    title: '-',
                    shadow: '',
                    visible: true,
                    icon: themeURL + '/images/sormat-pin.png'
                });

                marker.setMap(gmap);
                google.maps.event.addListener(marker, 'click', markerClickHandler);
                value.gmapsMarkerObject = marker;
            });
            //markers[0].gmapsMarkerObject.setIcon(themeURL+'/images/sormat-pin-selected.png');
        }
        //gmap.checkResize();
    }

    function runGeocoder() {
    }

// DEPRECATED
    function markerClickHandler() {
        var resellerData = getResellerInfo($(this)[0]);
    }

// DEPRECATED!
    function getResellerInfo(marker) {
        // look for clicked marker
        for (var i = 0; i < markers.length; i++) {
            markers[i].gmapsMarkerObject.setIcon(themeURL + '/images/sormat-pin.png');
        }
        var resultObj = {};
        if (marker) {
            for (var i = 0; i < markers.length; i++) {
                if (markers[i].gmapsMarkerObject.getPosition().equals(marker.getPosition())) {
                    resultObj.type = decodeURIComponent(markers[i].type) || "";
                    resultObj.title = decodeURIComponent(markers[i].title) || "";
                    resultObj.address1 = decodeURIComponent(markers[i].address1) || "";
                    resultObj.address2 = decodeURIComponent(markers[i].address2) || "";
                    resultObj.zip = decodeURIComponent(markers[i].zip) || "";
                    resultObj.city = decodeURIComponent(markers[i].city) || "";
                    resultObj.country = decodeURIComponent(markers[i].country) || "";
                    resultObj.phone = decodeURIComponent(markers[i].phone) || "";
                    resultObj.url = decodeURIComponent(markers[i].url) || "";

                    $(".reseller_type").text(resultObj.type);
                    $(".reseller_name").text(resultObj.title);
                    $(".reseller_address_1").text(resultObj.address1);
                    $(".reseller_address_2").text(resultObj.address2);
                    $(".reseller_address_3").text(resultObj.zip + '  ' + resultObj.city);
                    $(".reseller_country").text(resultObj.country);
                    $(".reseller_phone").text(resultObj.phone);
                    $(".reseller_url").text(resultObj.url);
                    $(".reseller_url").attr('href', resultObj.url);
                    markers[i].gmapsMarkerObject.setIcon(themeURL + '/images/sormat-pin-selected.png');
                    $('.map-right').show();
                    break;
                }
            }
        }
        return resultObj;
    }

// DEPRECATED
    function searchResultClickHandler(e) {
        e.preventDefault();
        var resellerData = getResellerInfo(markers[$(this).index()].gmapsMarkerObject);
        gmap.setZoom(14);
        gmap.setCenter(markers[$(this).index()].gmapsMarkerObject.getPosition());
        return false;
    }

    var queriedCountry = null,
        queriedCountryHits = 0;


// DEPRECATED
    function queryRetailers(e) {
        e.preventDefault();

        var search = $("#reseller-search").val();
        if (!search.length) {
            return false;
        }

        geocoder.geocode({address: search, language: 'en'}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {

                // Get country name
                queriedCountry = null;
                queriedCountryHits = 0;
                for (var n = 0; n < results[0].address_components.length; n++) {
                    if ($.inArray('country', results[0].address_components[n].types) > -1) {
                        queriedCountry = results[0].address_components[n].long_name.toLowerCase();
                    }
                }

                // convert some inaccurate results to get better results
                var convertResults = {
                    '61.924,25.748': new google.maps.LatLng(60.17332440000001, 24.941024800000037), // finland -> helsinki
                    '61.524,105.319': new google.maps.LatLng(55.755826, 37.6173) // russia -> moscow
                };
                var approxLatLng = results[0].geometry.location.lat().toFixed(3) + ',' + results[0].geometry.location.lng().toFixed(3);
                if (convertResults[approxLatLng]) {
                    results[0].geometry.location = convertResults[approxLatLng];
                }

                // hide all markers and calculate distances between point and geocode result
                $.each(markers, function (index, value) {
                    if (value.country.toLowerCase() == queriedCountry) {
                        queriedCountryHits++;
                    }
                    value.distance = getDistance({
                        "lat": value.lat,
                        "lng": value.lng
                    }, {"lat": results[0].geometry.location.lat(), "lng": results[0].geometry.location.lng()});
                    value.gmapsMarkerObject.setIcon(themeURL + '/images/sormat-pin.png');
                    //value.gmapsMarkerObject.setMap(null);
                });

                // sort markers by distance
                markers.sort(compareDistance);

                // show closest on map and results
                $(".search-result-container").html();
                var resultsHtml = "";
                var closestMarkerBounds = new google.maps.LatLngBounds();
                for (var i = 0; i < Math.min(markers.length, 18, Math.max(queriedCountryHits, 6)); i++) {
                    //if (markers[i].distance < 150000) { // 150 km
                    closestMarkerBounds.extend(markers[i].gmapsMarkerObject.position);
                    resultsHtml += '<a href="#" class="search-result-holder">'
                        + '<div class="picture-wrapper">'
                        + '<img src="' + markers[i].map + '" />'
                        + '<img src="' + themeURL + '/images/sormat-pin.png" class="marker-icon" />'
                        + '</div>'
                        + '<div class="text-wrapper">'
                        + '<p><span>' + decodeURIComponent(markers[i].title) + '</span> ' + decodeURIComponent(markers[i].city) + '</p>'
                        + '</div>'
                        + '</a>';
                    //}
                }

                if (closestMarkerBounds.isEmpty()) {
                    gmap.setCenter(results[0].geometry.location);
                    gmap.setZoom(11);
                    $(".reseller_type").text("");
                    $(".reseller_name").text("");
                    $(".reseller_address_1").text("");
                    $(".reseller_address_2").text("");
                    $(".reseller_address_3").text("");
                    $(".reseller_country").text("");
                    $(".reseller_phone").text("");
                    $(".reseller_url").text("");
                    $(".reseller_url").attr('href', '#');
                    $(".search-result-container").html("<p>No results.</p>");
                    $('.map-right').hide();
                } else {
                    $("#reseller-page .search-result").show();
                    $(".search-result-container").html(resultsHtml);
                    $(".search-result-container .search-result-holder").click(searchResultClickHandler);
                    var nearestMarker = getResellerInfo(markers[0].gmapsMarkerObject);
                    closestMarkerBounds.extend(results[0].geometry.location);
                    gmap.fitBounds(closestMarkerBounds);
                    $('.map-right').show();
                }

            } else {
                //alert('Geocode was not successful for the following reason: ' + status);
            }
        });
        return false;
    }


    var rad = function (x) {
        return x * Math.PI / 180;
    };

    var getDistance = function (p1, p2) {
        var R = 6378137; // Earth’s mean radius in meter
        var dLat = rad(p2.lat - p1.lat);
        var dLong = rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d; // returns the distance in meter
    };

    function compareDistance(a, b) {
        if (a.country.toLowerCase() == queriedCountry && b.country.toLowerCase() != queriedCountry) {
            return -1;
        } else if (a.country.toLowerCase() != queriedCountry && b.country.toLowerCase() == queriedCountry) {
            return 1;
        }

        if (a.distance < b.distance) {
            return -1;
        } else if (a.distance > b.distance) {
            return 1;
        }
        return 0;
    }

    function initFooterAccordion() {
        $('#footer-content h3').click(function () {
            $(this).children('.open-close-accordion').toggleClass('clicked').closest('.column').find('.accordion-holder').toggleClass('active');
        });
    }

    /* Javascript styling, like sortable tables and theme article breather images */

    /*Mobile navigation*/
    function initContentPosition() {
        setContentPaddingTop();
    }

    function setContentPaddingTop() {
        var paddingTop = $('#upper-wrapper').height() + $('#lower-wrapper').height();
        $('#carousel-wrapper, #upper-shadow, .single-featuredimage').css('padding-top', paddingTop);
    }

    function initMobileMenu() {
        if ($('#menu-main-navigation li.current-menu-item').length == 0) {
            $('#menu-main-navigation li.menu-item').eq(0).addClass('current-menu-item');
        }
        $('#menu-button').click(openMenu);
        if ($('#menu-button').is(':visible')) {
            $('#menu-main-navigation > li.current-menu-item, #menu-main-navigation > li.current-menu-parent').unbind('click');

            $('.menu-header').on('click', '#menu-main-navigation > li.current-menu-item, #menu-main-navigation > li.current-menu-parent', openMenu);
        }
    }

    function openMenu(e) {
        if (e) e.preventDefault();

        $currentItem = $('#menu-main-navigation > li.current-menu-item, #menu-main-navigation > li.current-menu-parent');
        $currentItem.addClass('current-menu-item');
        $('#menu-main-navigation').prepend($currentItem);
        $('#menu-main-navigation li').not('.current-menu-item').toggle();
        $currentItem.find('.sub-menu').toggle();
    }


    function initEnhancements() {
        /*News hover functions*/
        $('.article-news-text p, .recent-news-text p').hover(
            function () {
                $(this).siblings('.read-more-icon').addClass('active');
            },
            function () {
                $(this).siblings('.read-more-icon').removeClass('active');
            }
        );

        /* Hide empty blocks */
        var first, empty;

        first = true;
        empty = true;
        $('.technical-data .one-column').each(function () {
            if (!$(this).find('.table-row').length) {
                $(this).hide();
            } else {
                empty = false;
                if (first) {
                    first = false;
                    $(this).addClass('first');
                }
            }
        });
        if (empty) {
            $('.technical-data').hide();
        }

        first = true;
        empty = true;
        $('.additional-information .one-column').each(function () {
            if (!$(this).find('.additional-information-text').length) {
                $(this).hide();
            } else {
                empty = false;
                if (first) {
                    first = false;
                    $(this).addClass('first');
                }
            }
        });
        if (empty) {
            $('.additional-information').hide();
        }


        /* Hide empty columns */
        var table, headings, last;

        /* Hide empty columns from product table */
        table = $('#products-table');
        headings = table.find('thead th');
        last = headings.length - 1;

        headings.each(function (index) {

            // Never hide first and last column
            if (index == 0 || index == last) {
                return;
            }

            hideColumn(table, $(this), index);
        });

        /* Hide empty columns from load table */
        $('.performance-data').each(function () {
            table = $(this).find('table');
            headings = table.find('thead th');
            last = headings.length - 1;

            headings.each(function (index) {
                hideColumn(table, $(this), index);
            });

            if (headings.length == headings.filter('.hidden').length) {
                $(this).hide();
            }
        });

        /* Make product table sortable */
        $('#products-table').tablesorter();
        //$('.performance-data table').tablesorter();

        //calculateBreatherImage();
    }

    function hideColumn(table, heading, index) {

        // Get cells in column
        var cells = table.find('tr td:nth-child(' + (index + 1) + ')');

        // Filter empty cells
        var nonempty = cells.filter(function () {
            return $.trim($(this).html()).length;
        });

        // Hide heading and cells, if all are empty
        if (!nonempty.length) {
            heading.addClass('hidden');
            cells.addClass('hidden');
        }
    }

    function calculateBreatherImage() {
        var windowWidth = $(window).width();
        var containerMargin = $(window).width() - $('.main-content-holder').width();
        $('.size-header').width(1267);
        $('.size-header').css('left', -containerMargin / 2);
    }

    function initTableOfContents() {

        var toc = '';

        $('body.single-trainingarticle article.trainingarticle h3').each(function () {
            var title = $(this).text();
            var slug = convertToSlug(title);

            $(this).append('<a name="' + slug + '" class="article-anchor"></a>');

            toc += '<li class="list-link">';
            toc += '<a href="#' + slug + '">';
            toc += title;
            toc += '</a></li>';
        });

        if (toc) {
            $('#table-of-contents .post-list').html(toc);
            $('#table-of-contents').show();
        }

    }

    function convertToSlug(text) {
        return text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
    }

    (function () {
        // Clickable table rows
        $('#products-table').on('click', 'tbody tr', function () {
            var href = $(this).data('href');

            if (href) {
                window.location = href;
            }
        });
    })();

    /**
     * Performance data table selector.
     *
     * @return {[type]} [description]
     */
    (function () {
        var $perfInput = $('#performance-data-selector');
        var $perfTable = $perfInput.closest('table');

        $perfInput.on('change', function () {
            var value = $(this).val();

            if ('' === value) {
                $perfTable.find('tbody tr').show();
            } else {
                $perfTable.find('tbody tr').hide();
                $perfTable.find('tbody tr[data-performance-base="' + value + '"]').show();
            }
        });
    })();

    $('.sormat-ajax').each(function (index, el) {
        $(this).on('submit', function (e) {
            e.preventDefault()
            let $this = $(this)[0];
            $.ajax({
                'url': $this.action,
                'method': $this.method,
                'data': $(this).serialize(),
            })
                .done(function (response) {
                    alert(response.data)
                })
        })
    })

    function initModal(modalName, buttonName, showModal = false, callback = null) {
        var modal = document.getElementById(modalName);
        if (modal !== null) {
            var btn = document.getElementById(buttonName);
            var span = modal.getElementsByClassName('close')[0];

            if (showModal === true) {
                modal.style.display = "block";
            }

            if (btn !== null) {
                btn.onclick = function () {
                    modal.style.display = "block";
                }
            }

            if (span !== null) {
                span.onclick = function () {
                    modal.style.display = "none";
                    if (callback !== null) {
                        callback()
                    }
                }
            }

            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        }
    }

    var coookie = getCookie('sormat_user_location') === null;
    var session = sessionStorage.getItem('sormat_price_info') === null;
    initModal('userCity', 'changeUserCity', coookie)
    initModal('priceModalInfo', null, session, function () {
        sessionStorage.setItem('sormat_price_info', true);
    })

    $('#geolocation-content').on('click', 'a', function () {
        $.ajax({
            url: '/wp-admin/admin-ajax.php?action=get_geolocation_terms',
            type: 'POST',
            data: {
                term_id: $(this).data('id')
            },
        })
            .done(function (response) {
                $('#geolocation-content').html(response)
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
    })

})(jQuery);

(function ($) {
    $('.sormat-select2').select2({
        placeholder: "Выберите нужные сертификаты",
    })

    $('#sormat_manager_email').click(function (e) {
        e.preventDefault()
        var $input = $('input[name="sormat_order_manager_email"]')
        $.ajax({
            url: '/wp-admin/admin-ajax.php?action=sormat_order_send_email',
            type: 'post',
            data: {
                address: $input.val(),
                order_id: $input.data('order-id')
            }
        })
            .done(function (response) {
                console.log(response)
            })
            .always(function (response) {
                console.log(response)
            })
    })

})(jQuery);
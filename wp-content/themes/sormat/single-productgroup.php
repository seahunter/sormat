<?php

get_header(); ?>
    <section id="product-group" class="main-content-holder">
        <?php while (have_posts()) : the_post(); ?>
            <div class="product-title-redux">
                <?php
                $product_cat = get_the_terms(get_post(), 'product_cat');
                $product_cat_name = !empty($product_cat) ? $product_cat[0]->name : '';
                ?>
                <h2><?php echo get_the_title() . ' ' . $product_cat_name ?></h2>
            </div>
            <?php
            global $parent_group;
            $parent_group = true;
            get_template_part('template-parts/product/template', 'main');

            get_template_part('template-parts/product/table', 'relative');

            get_sidebar();
        endwhile; ?>
    </section>
<?php
get_footer();
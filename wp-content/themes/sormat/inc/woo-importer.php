<?php

require_once('../../../../wp-load.php');

global $wpdb;

$row = 1;
if (($handle = fopen("list.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        $query = "SELECT ID FROM $wpdb->posts WHERE post_title = %s";
        $result = $wpdb->get_col($wpdb->prepare($query, $data[0]));
        update_post_meta($result[0], '_regular_price', (float)$data[1]);
	    update_post_meta($result[0], '_price', (float)$data[1]);
    }
    fclose($handle);
}

var_dump('COMPLETE');
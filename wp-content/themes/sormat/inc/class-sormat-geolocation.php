<?php

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;

class Sormat_Geolocation
{
    /**
     * @var Reader
     */
    private $reader;
    /**
     * @var string
     */
    private $ip;

    public function __construct(Reader $reader, string $ip)
    {
        $this->reader = $reader;
        $this->ip = $ip;
    }

    public function get_city()
    {
        try {
            return $this->reader->city($this->ip)->city->name;
        } catch (AddressNotFoundException $e) {
            echo $e->getMessage();
        } catch (InvalidDatabaseException $e) {
            echo $e->getMessage();
        }
    }
}
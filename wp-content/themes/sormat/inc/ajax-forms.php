<?php

function create_feedback_post( $post_type, $post_fields = array() ) {
  $data = array(
    'post_title' => isset($_POST['name']) ? wp_strip_all_tags($_POST['name']) : '',
    'post_type' => $post_type,
    'post_content' => isset($_POST['message']) ? wp_strip_all_tags($_POST['message']) : '',
    'post_status' => 'draft'
  );

  $args = wp_parse_args( $post_fields, $data );
  $post_id = wp_insert_post($args, true);
  if( is_wp_error( $post_id ) ) {
    echo $post_id->get_error_message();
  } else {
    return $post_id;
  }
}

function feedback_form() {
	if( ! empty( $_POST ) ) {

    $content = '';
    if( isset( $_POST['contact_subject'] ) ) {
      $content .= '<p>Тема: ' . $_POST['contact_subject'] . '</p>';
    }
    if( isset( $_POST['contact_general-question'] ) ) {
      $content .= '<p>Вопрос: ' . $_POST['contact_general-question'] . '</p>';
    }
    if( isset( $_POST['contact_free_field'] ) ) {
      $content .= '<p>Текст сообщения: ' . $_POST['contact_free_field'] . '</p>';
    }

    $form_post = create_feedback_post( 'feedback', array(
      'post_title' => $_POST['contact_name'] . ' - ' . $_POST['contact_email'],
      'post_content' => $content
    ) );
    if( ! is_wp_error( $form_post ) ) {
      $form_message = 'Сообщение успешно отправлено';
      echo $form_message;
    } else {
      $form_message = 'Произошла ошибка. Повторите позже';
      echo $form_message;
    }
  }
  wp_die();
}

add_action( 'wp_ajax_feedback', 'feedback_form' );
add_action( 'wp_ajax_nopriv_feedback', 'feedback_form' );
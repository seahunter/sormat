<?php

use GeoIp2\Database\Reader;
use MaxMind\Db\Reader\InvalidDatabaseException;

class Geolocation
{
    /**
     * @var string
     */
    private $path;
    /**
     * @var bool|Reader
     */
    public $reader;

    public function __construct()
    {
        $this->path = wp_get_upload_dir()['basedir'] . '/woocommerce_uploads/GeoLite2-City.mmdb';
        $this->reader = $this->get_instance();
    }

    private function get_instance()
    {
        if (file_exists($this->path)) {
            try {
                return new Reader($this->path, ['ru']);
            } catch (InvalidDatabaseException $e) {
                error_log($e->getMessage());
            }
        }

        return false;
    }


}
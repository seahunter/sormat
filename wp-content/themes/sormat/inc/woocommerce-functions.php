<?php

$region = '';
if ( is_user_logged_in() ) {
	$user   = wp_get_current_user();
	$region = get_field( 'region', $user );
}
global $regions;
$GLOBALS['regions'] = $regions;

add_filter( 'woocommerce_states', 'awrr_states_russia' );
function awrr_states_russia( $states ) {
	$states['RU'] = $GLOBALS['regions'];

	return $states;
}

function filter_admin_products_posts( WP_Query $query ) {
	if ( is_admin() && $query->get( 'post_type' ) == 'shop_order' ) {

		$user = wp_get_current_user();

		if ( in_array( 'shop_manager', $user->roles ) && is_main_query() ) {
			$manager_regions = get_field( 'cities', 'user_' . $user->ID );
			$cities          = [];
			foreach ( $manager_regions as $value ) {
				$cities[] = $value['city']->post_title;
			}
			$meta_query = [
				'relation' => 'OR',
				[
					'key'     => 'manager',
					'compare' => 'EXISTS',
				],
				[
					'key'     => 'manager',
					'value'   => $user->ID,
					'compare' => '=',
				],
				[
					'key'     => 'customer_region',
					'value'   => $cities,
					'compare' => 'IN',
				],
			];
			$query->set( 'meta_query', $meta_query );
		}
	}
}

add_action( 'pre_get_posts', 'filter_admin_products_posts', 10, 1 );

function get_manager_id( $region ) {
	$managers = get_users(
		[
			'role'       => 'shop_manager',
			'meta_query' => [
				[
					'key'     => 'region',
					'value'   => $region,
					'compare' => '==',
				],
			],
		]
	);

	if ( $managers ) {
		return $managers[0]->data->ID;
	}

	return false;
}

function update_manager( $order_id ) {

	$order   = new WC_Order( $order_id );
	$state   = $order->get_billing_state();
	$manager = get_manager_id( $state );

	if ( $manager ) {
		update_field( 'manager', $manager, $order_id );
	}

}

add_action( 'woocommerce_update_order', 'update_manager', 20, 1 );

function wooninja_remove_items() {
	$remove = [ 'wc-settings', 'wc-status', 'wc-addons', ];
	foreach ( $remove as $submenu_slug ) {
		if ( ! current_user_can( 'update_core' ) ) {
			remove_submenu_page( 'woocommerce', $submenu_slug );
		}
	}
}

add_action( 'admin_menu', 'wooninja_remove_items', 99, 0 );

function get_pricelist_pdf() {
	$prices = get_field( 'prices', 'option' );
	$file   = get_field( 'default_pricelist', 'option' );
	if ( isset( $_COOKIE['shop_user_city'] ) && isset( $_COOKIE['shop_user_region'] ) ) {
		foreach ( $prices as $price ) {
			if ( mb_strtoupper( $price['region'] ) == mb_strtoupper( $_COOKIE['shop_user_city'] ) || mb_strtoupper( $price['region'] ) == mb_strtoupper( $_COOKIE['shop_user_region'] ) ) {
				$file = $price['price']['url'];
				return $file;
			}
		}
	}

	return $file['url'];
}

function get_user_geo_country() {
	$geolocation = new Geolocation();
	if ( $geolocation ) {
		$sormat_geo = new Sormat_Geolocation( $geolocation->reader, WC_Geolocation::get_ip_address() );
		return $sormat_geo->get_city();
	}

	return 'Ваш город не определён';

}



function find_users_by_location( $request_city ) {

	$users = get_users(
		[
			'role__in' => [ 'shop_manager' ],
		]
	);

	$result = [];

	if ( ! empty( $users ) ) {
		foreach ( $users as $user ) {
			$cities = get_field( 'cities', 'user_' . $user->ID );
			if ( ! empty( $cities ) ) {
				foreach ( $cities as $city ) {
					if ( mb_strtolower($request_city) === mb_strtolower( $city['city']->post_title ) ) {
						$result[] = $user->user_email;
						break;
					}
				}
			}
		}
	}

	return $result;
}


add_filter( 'woocommerce_email_headers', 'sormat_new_order_sub_email', 9999, 3 );

function sormat_new_order_sub_email( $headers, $email_id, $order ) {

	if ( 'new_order' == $email_id ) {
		$city    = mb_strtolower( sanitize_text_field( wp_unslash( $_COOKIE['sormat_geolocation'] ) ) );

		if( ! empty( $city ) ) {
			$users = find_users_by_location( $city );

			foreach ( $users as $user ) {
				$headers .= 'Cc: '. $user .' <'. $user .'>' . "\r\n";
			}
		}
	}
	return $headers;
}

function send_user_via_checkout( $customer_id, $new_customer_data, $password_generated  ) {
	wp_new_user_notification($customer_id, null, 'user');
}

add_action('woocommerce_created_customer', 'send_user_via_checkout', 10, 3 );
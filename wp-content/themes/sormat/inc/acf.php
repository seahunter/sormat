<?php

function register_acf_options_pages() {
	global $regions;
	// Check function exists.
	if ( ! function_exists( 'acf_add_options_page' ) )
		return;

	// register options page.
	$option_page = acf_add_options_page(
		[
			'page_title' => __( 'Theme General Settings' ),
			'menu_title' => __( 'Theme Settings' ),
			'menu_slug'  => 'theme-general-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		]
	);

	acf_add_local_field_group(
		[
			'key'                   => 'group_5ef2742b1755e',
			'title'                 => 'Регион',
			'fields'                => [
				[
					'key'               => 'field_5ef2743490bf7',
					'label'             => 'Выберите регион',
					'name'              => 'region',
					'type'              => 'select',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => [
						'width' => '',
						'class' => '',
						'id'    => '',
					],
					'choices'           => $regions,
					'default_value'     => [],
					'allow_null'        => 1,
					'multiple'          => 0,
					'ui'                => 1,
					'ajax'              => 1,
					'return_format'     => 'label',
					'placeholder'       => '',
				],
			],
			'location'              => [
				[
					[
						'param'    => 'user_role',
						'operator' => '==',
						'value'    => 'shop_manager',
					],
				],
			],
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => true,
			'description'           => '',
		]
	);


}

// Hook into acf initialization.
add_action( 'acf/init', 'register_acf_options_pages' );

function update_order_acf_meta( $value, $post_id, $field ) {
	$order = new WC_Order( $post_id );
	if ( 'manager_callback' === $field['name'] && '1' === $value ) {
		$order->update_status( 'client-call' );
	} elseif ( 'invoicing' === $field['name'] && 'success' === $value ) {
		$order->update_status( 'invoiced' );
	} elseif ( 'invoicing' === $field['name'] && 'cancel' === $value ) {
		$order->update_status( 'client-cancel' );
	} elseif ( 'product_shipment' === $field['name'] && 'success' === $value ) {
		$order->update_status( 'shipped' );
	} elseif ( 'product_shipment' === $field['name'] && 'cancel' === $value ) {
		$order->update_status( 'client-cancel' );
	}

	return $value;
}

add_filter( 'acf/update_value', 'update_order_acf_meta', 10, 3 );


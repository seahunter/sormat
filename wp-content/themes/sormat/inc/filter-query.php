<?php

function product_search_filter( $query ) {
  if( ! is_admin() && $query->is_main_query() && $query->is_search() ) {
    $taxonomies = ['product_cat'];
    $attr_taxonomies = wc_get_attribute_taxonomies();
    if( ! empty( $attr_taxonomies ) ) {
      foreach ($attr_taxonomies as $tax) {
        $taxonomies[] = 'pa_' . $tax->attribute_name;
      }
    }
    $result = array_uintersect($taxonomies, array_keys($_GET), 'strcasecmp');
    $taxquery = [];
    if( ! empty( $result ) ) {
      $query->set('post_type', 'product');
      // foreach ($result as $tx) {
      //   $tax_item = [
      //     'taxonomy' => $tx,
      //     'field' => 'slug',
      //     'terms' => $_GET[$tx]
      //   ];
      //   $taxquery[] = $tax_item;
      // }
    }

    $query->set('tax_query', $taxquery);
  }
}

add_action( 'pre_get_posts', 'product_search_filter' );

function modify_search_vars($search_vars) {
    $taxonomies = ['product_cat'];
    $attr_taxonomies = wc_get_attribute_taxonomies();
    if( ! empty( $attr_taxonomies ) ) {
      foreach ($attr_taxonomies as $tax) {
        $taxonomies[] = 'pa_' . $tax->attribute_name;
      }
    }
    $result = array_uintersect($taxonomies, array_keys($search_vars), 'strcasecmp');
    $taxquery = [];
    if( ! empty( $result ) ) {
      foreach ($result as $tx) {
        $search_vars[$tx] = implode(',', $search_vars[$tx]);
      }
    }

    if( isset( $search_vars['s'] ) ) {
      $search_vars['s'] = '';
    }

    return $search_vars;
}

add_filter('request', 'modify_search_vars', 99);
